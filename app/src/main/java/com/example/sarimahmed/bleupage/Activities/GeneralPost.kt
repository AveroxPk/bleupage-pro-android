package com.example.sarimahmed.bleupage.Activities

import android.accounts.AccountManager
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.graphics.Matrix
import android.graphics.Picture
import android.media.ExifInterface
import android.media.Image
import android.net.Uri
import android.opengl.Visibility
import android.os.*
import android.support.v7.app.AppCompatActivity
import android.provider.MediaStore
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.ContextMenu
import android.view.MenuItem
import android.view.View
import android.widget.*

import com.example.sarimahmed.bleupage.R
import com.example.sarimahmed.bleupage.SessionManager.UserDataManager
import org.jetbrains.anko.find
import org.jetbrains.anko.toast
import android.provider.MediaStore.Files.FileColumns.MEDIA_TYPE_IMAGE
import android.support.v4.content.AsyncTaskLoader
import android.text.Editable
import android.util.Base64
import com.bumptech.glide.Glide
import com.bumptech.glide.GlideBuilder
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.drawable.GlideDrawable
import com.bumptech.glide.request.FutureTarget
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.example.sarimahmed.bleupage.HelperClasses.AlertManager
import com.example.sarimahmed.bleupage.HelperClasses.BuildUrl
import com.example.sarimahmed.bleupage.HelperClasses.JsonKeys
import java.text.SimpleDateFormat
import java.util.*
import com.example.sarimahmed.bleupage.HelperClasses.MarshMallowPermission
import com.example.sarimahmed.bleupage.R.drawable.add_image
import org.jetbrains.anko.alert
import org.jetbrains.anko.doAsync
import java.io.*
import java.net.URI
import java.net.URL
import java.net.URLConnection


class GeneralPost : AppCompatActivity(), View.OnClickListener , CompoundButton.OnCheckedChangeListener{



    private var homeToolbar: Toolbar? = null
    private var button_next: Button? = null
    private var toolbar_title: TextView? = null
    private var schedule_switch: Switch? = null
    private var picker: NumberPicker? = null
    private var ScheduleActivated: Boolean = false
    private lateinit var post_body: EditText
    private lateinit var post_body_url: EditText
    private var add_image: LinearLayout? = null
    private var imageFileuri: Uri? = null
    private var bitmap_object: Bitmap? = null
    private var imageFile: File? = null
    private var RESULT_LOAD_CAMERA = 0
    private var RESULT_LOAD_IMAGE = 1
    private var imageview_add_image: ImageView? = null
    private lateinit var marshMallowPermission: MarshMallowPermission
    private lateinit var image: Image
    private var picture_string: String = ""
    private var link: String = ""
    private var message: String = ""
    private lateinit var imageByteArray: ByteArray
    private var base64: Boolean = false
    private var filename: String? = null
    private var template_url: String = ""
    private var title: String = ""
    private var isActivityActive: Boolean = false
    private var button_remove_image: ImageButton? = null
    private var imagePath: String? = null

    private var bitmap: Bitmap? = null



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_general_post)

        homeToolbar = findViewById(R.id.my_toolbar) as Toolbar
        setSupportActionBar(homeToolbar)
        //supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_user)



        supportActionBar?.setDefaultDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)


        super.setTitle("")

        toolbar_title = findViewById(R.id.toolbar_title_post) as TextView?

        isActivityActive = true

        var intent = intent
        var extras = intent.extras



        try {
            title = extras.getString("TITLE")



                toolbar_title?.text = title


        } catch(e: Exception) {
        } finally {
        }





        button_next = findViewById(R.id.button_next) as Button
        button_next?.setOnClickListener(this)

        schedule_switch = findViewById(R.id.schedule_switch) as Switch?
        schedule_switch?.setOnCheckedChangeListener(this)

        post_body = findViewById(R.id.post_body) as EditText
        post_body_url = findViewById(R.id.post_body_url) as EditText

        add_image = findViewById(R.id.add_image) as LinearLayout?

        add_image?.setOnClickListener(this)

        imageview_add_image = findViewById(R.id.imageview_add_image) as ImageView?

        button_remove_image = findViewById(R.id.button_remove_image) as ImageButton?
        button_remove_image?.setOnClickListener(this)

        if(imageview_add_image?.drawable == null)
        {
            button_remove_image?.visibility = View.GONE
            button_remove_image?.isClickable = false
        }
        marshMallowPermission = MarshMallowPermission(this)





        picker = findViewById(R.id.timePicker_generalpost) as NumberPicker
        picker?.maxValue = 23

        val strs = resources.getStringArray(R.array.array_time_24hr)
        picker?.displayedValues = strs
        picker?.wrapSelectorWheel = true

        picker?.isEnabled = false
        ScheduleActivated = false

        if(BuildUrl.templates_url != "" && BuildUrl.templates_url.isEmpty() == false && BuildUrl.templates_url != " " && BuildUrl.templates_url != null)
        {
            template_url = BuildUrl.templates_url

            //val progressBar =  findViewById(R.id.progress) as ProgressBar

            var load = SetImageFromUrl(BuildUrl.templates_url,imageview_add_image!!)
            load.execute()




        }

        if(title.equals(resources.getString(R.string.string_toolbarTitle_blog_post)))
        {

            post_body?.setText(extras.getString("BLOG_CONTENT"),TextView.BufferType.EDITABLE)

            post_body_url?.setText(extras.getString("BLOG_LINK"),TextView.BufferType.EDITABLE)


        }





    }

    override fun onResume() {

        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (!marshMallowPermission.checkPermissionForCamera()) {
                    marshMallowPermission.requestPermissionForCamera()

                } else if (!marshMallowPermission.checkPermissionForExternalStorage()) {
                    marshMallowPermission.requestPermissionForExternalStorage()
                }
            }
        } catch (e: Exception) {
        } finally {
        }

        if(BuildUrl.templates_url != "" && BuildUrl.templates_url.isEmpty() == false && BuildUrl.templates_url != " " && BuildUrl.templates_url != null)
        {
            template_url = BuildUrl.templates_url



          // var a = SetImage(template_url, imageview_add_image!!)

        Log.d("resumecalled","in on resume template url is not empty ")
         //

            try {
                var load = SetImageFromUrl(template_url,imageview_add_image!!)
                load.execute()
            } catch(e: Exception) {
            } finally {
            }


        }






            //var a = SetImage(template_url,imageview_add_image!!,"tem")




        super.onResume()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.getItemId()

        if (id == android.R.id.home) {
            onBackPressed()
            overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left)
            finish()
            return true
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onBackPressed() {

        BuildUrl.templates_url = ""
        try {
            Glide.get(applicationContext).clearMemory()

        } catch(e: Exception) {


            Log.d("bitmap",""+e.message.toString())
            Log.d("bitmap",""+e.printStackTrace().toString())

        } finally {
            Log.d("bitmap","final block")

        }



        super.onBackPressed()
        finish()
        overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left)

    }


    override fun onClick(v: View?) {
        var id = v?.id
        when(id)
        {
            R.id.button_next ->
            {

                if(post_body?.text.toString().trim().isEmpty() && post_body_url?.text.toString().trim().isEmpty() == true && filename == null && template_url.trim().isEmpty())
                {
                    var a = AlertManager("Post cannot be empty",3000,this@GeneralPost)
                }

                else {


                    //Log.d("postintent",post_body?.text.toString() + post_body_url?.text.toString() + picture_string)

                    if (post_body?.text.toString() == null) {
                        message = ""
                    } else {
                        message = post_body?.text.toString()
                    }
                    if (post_body_url?.text.toString() == null) {
                        link = ""
                    } else {
                        link = post_body_url?.text.toString()
                    }
                    if (picture_string == null) {
                        picture_string = ""
                    }


                    var post_intent = Intent(this, PlatformSelection::class.java)
                    post_intent.putExtra("POST_TYPE","general")
                    post_intent.putExtra("POSTED_ON", "" + picker?.value)
                    post_intent.putExtra("SCHEDULE", ScheduleActivated)
                    post_intent.putExtra("POST_BODY", post_body?.text.toString())
                    post_intent.putExtra("POST_BODY_URL", post_body_url?.text.toString())
                    //post_intent.putExtra("IMAGE_STRING",picture_string)
                    post_intent.putExtra("BASE_64", base64)

                    if (base64 == true) {
                        post_intent.putExtra("IMAGE", filename)

                    } else if (base64 == false) {
                        post_intent.putExtra("TEMPLATE_URL", template_url)

                    }


                    try {

                        startActivity(post_intent)
                    } catch(e: Exception) {
                        Log.d("postintent", "general post page: " + e.message)
                        Log.d("postintent", "general post page: " + e.printStackTrace().toString())
                    } finally {
                        Log.d("postintent", "general post page: " + "post intent final block")

                    }

                    overridePendingTransition(R.anim.enter_horizontal, R.anim.exit_horizontal)
                }

            }
            R.id.button_remove_image ->
            {
                try {
                    imageview_add_image?.setImageBitmap(null)
                    imageview_add_image?.setImageDrawable(null)
                    BuildUrl.templates_url = ""

                    try {
                        /*imageview_add_image?.setImageResource(0);
                        imageview_add_image?.destroyDrawingCache()*/
                        Glide.get(applicationContext).clearMemory()


                    } catch(e: Exception) {
                        Log.d("img",e.message)
                        Log.d("img",e.printStackTrace().toString())

                    } finally {
                    }


                    if(imageview_add_image?.drawable == null)
                    {
                        button_remove_image?.visibility = View.GONE
                        button_remove_image?.isClickable = false
                        base64 = false
                        BuildUrl.templates_url = ""
                        imageview_add_image?.background = resources.getDrawable(R.drawable.add_image)
                        add_image?.isClickable = true
                        add_image?.isEnabled = true

                        filename = null
                    }
                    else
                    {
                        Log.d("drawable","drawable is not null")
                    }

                    Log.d("resumecalled",template_url)
                } catch(e: Exception) {
                } finally {
                }
            }
            R.id.add_image ->
            {
                registerForContextMenu(add_image)
                add_image?.showContextMenu()


            }

        }
    }


    override fun onCheckedChanged(buttonView: CompoundButton?, isChecked: Boolean) {

        if(isChecked)
        {
            picker?.isEnabled = true
            ScheduleActivated = true
        }
        else
        {
            picker?.isEnabled = false
            ScheduleActivated = false
        }
    }

    override fun onCreateContextMenu(menu: ContextMenu?, v: View?, menuInfo: ContextMenu.ContextMenuInfo?) {
        super.onCreateContextMenu(menu, v, menuInfo)
        if(title == resources.getString(R.string.string_toolbarTitle_generalPost)) {

            getMenuInflater().inflate(R.menu.add_image_context_menu, menu)
        }
        else if(title == resources.getString(R.string.string_toolbarTitle_discountPost))
        {
            getMenuInflater().inflate(R.menu.add_discount_image_menu, menu)

        }
    }

    override fun onContextItemSelected(item: MenuItem?): Boolean {

        // val info = item?.getMenuInfo() as AdapterContextMenuInfo

        when (item?.itemId) {
            R.id.context_camera -> {
                openCamera()
                return true
            }
            R.id.context_load_templates ->
            {
                try {
                        var templates_intent = Intent(this, Templates_GeneralPost::class.java)
                        startActivity(templates_intent)
                        overridePendingTransition(R.anim.enter_horizontal, R.anim.exit_horizontal)


                } catch(e: Exception) {
                    Log.d("loadtemp",e.message)
                    Log.d("loadtemp",e.printStackTrace().toString())

                } finally {
                    Log.d("loadtemp","final block")

                }

                return true
            }
            R.id.context_gallery ->
            {
                openGallery()
                return true
            }
            else -> return super.onContextItemSelected(item)
        }
    }

    fun openCamera() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!marshMallowPermission.checkPermissionForCamera()) {
                marshMallowPermission.requestPermissionForCamera()

            }
            else if (!marshMallowPermission.checkPermissionForExternalStorage()) {
                marshMallowPermission.requestPermissionForExternalStorage()
            }
            else {
               /* if (!marshMallowPermission.checkPermissionForExternalStorage()) {
                    marshMallowPermission.requestPermissionForExternalStorage()
                }*/
                    val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                    val mediaStorageDir = File(
                            Environment.getExternalStorageDirectory().toString()
                                    + File.separator
                                    + "abc"
                                    + File.separator
                                    + "abc"
                    )



                    if (!mediaStorageDir.exists()) {
                        mediaStorageDir.mkdirs()
                    }

                    val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss",
                            Locale.getDefault()).format(Date())
                    try {
                        imageFile = File.createTempFile(
                                "IMG_" + timeStamp, /* prefix */
                                ".jpg", /* suffix */
                                mediaStorageDir      /* directory */
                        )



                        takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(imageFile))
                        startActivityForResult(takePictureIntent, RESULT_LOAD_CAMERA)
                    } catch (e: IOException) {
                        Log.d("camera",e.message)
                        Log.d("camera",e.printStackTrace().toString())
                    }
                finally
                {
                    Log.d("camera","final")
                }
            }
        }
        else
        {
            val camera_intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            imageFile = File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "test.jpg")
            imageFileuri = Uri.fromFile(imageFile)
            camera_intent.putExtra(MediaStore.EXTRA_OUTPUT, imageFileuri)
            camera_intent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 1)
            try {
                if (camera_intent.resolveActivity(getPackageManager()) != null) {

                    startActivityForResult(camera_intent, RESULT_LOAD_CAMERA)
                } else {
                    Log.d("Imagelog", "package manager null")
                }
            } catch (e: Exception) {
                e.printStackTrace()
                Log.d("Camera Activity", e.message)

            } finally {
                Log.d("Camera Activity", "final")

            }
        }
    }


    fun openGallery() {

            if (!marshMallowPermission.checkPermissionForExternalStorage()) {
                marshMallowPermission.requestPermissionForExternalStorage()
            }
            else {
                val gallery_intent = Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
                startActivityForResult(gallery_intent, RESULT_LOAD_IMAGE)
            }

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == 0) {
            when (resultCode) {
                Activity.RESULT_OK -> if (imageFile?.exists() == true) {

                    if(bitmap_object != null)
                    {
                        bitmap_object = null

                    }
                    else if(bitmap != null)
                    {
                        bitmap = null
                    }
                    bitmap_object = scaleBitmapDown(BitmapFactory.decodeFile(imageFile?.getAbsolutePath()), 1200)





                    //imageview_add_image?.setImageBitmap(bitmap_object)


                    /*Glide.with(this@GeneralPost)
                            .load(Uri.fromFile(imageFile))
                            .diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true)
                            .into(imageview_add_image)*/
                    filename = "bitmap.png"






                    //imageview_add_image?.setImageBitmap(bitmap_object)
                    //picture_string = getBase64(bitmap_object!!)!!
                    var stream = this.openFileOutput(filename, Context.MODE_PRIVATE)
                    bitmap_object?.compress(Bitmap.CompressFormat.PNG, 100, stream)



                    //Cleanup
                    stream.close()
                    // bitmap_object?.recycle();

                    var load = SetImageFromBitmap(getRotatedBitmap(imageFile?.absolutePath!!), imageview_add_image!!)
                    load.execute()
                    //Log.d("Imagelog", "base 64: "+getBase64(bitmap_object!!))
                    //Toast.makeText(this,"Picture was saved at "+imageFile.getAbsolutePath(),Toast.LENGTH_LONG).show();
                    //Log.d("Imagelog", "Picture was saved at " + imageFile?.getAbsolutePath())
                }
                else
                {
                    Toast.makeText(this, "Error loading Picture", Toast.LENGTH_LONG).show()
                    Log.d("Imagelog", "Error loading Picture")
                }
                Activity.RESULT_CANCELED ->
                    //Toast.makeText(this,"Result canceled",Toast.LENGTH_LONG).show();
                    Log.d("Imagelog", "Result canceled")
            }
        } else if (requestCode == RESULT_LOAD_IMAGE) {
            when (resultCode) {
                Activity.RESULT_OK -> {
                    val selectedImage = data?.data
                    val filePathColumn = arrayOf(MediaStore.Images.Media.DATA)

                    val cursor = contentResolver.query(selectedImage,
                            filePathColumn, null, null, null)
                    cursor!!.moveToFirst()



                    val columnIndex = cursor.getColumnIndex(filePathColumn[0])
                    var picturePath = cursor.getString(columnIndex)
                    cursor.close()


                       // imageByteArray = convertToByteArray(bitmap_object!!)
                if(bitmap_object != null)
                {
                    bitmap_object = null

                }
                else if(bitmap != null)
                {
                    bitmap = null
                }


                    bitmap_object = scaleBitmapDown(BitmapFactory.decodeFile(picturePath),1200)
                    //imageview_add_image?.setImageBitmap(bitmap_object)


                    filename = "bitmap.png"



                    //Write file
             var stream = this.openFileOutput(filename, Context.MODE_PRIVATE);
        bitmap_object?.compress(Bitmap.CompressFormat.PNG, 100, stream);

    //Cleanup
    stream.close();

                    var load = SetImageFromBitmap(getRotatedBitmap(picturePath), imageview_add_image!!)
                    load.execute()
    //bitmap_object?.recycle();


                    //picture_string = getBase64(compressBitmap(bitmap_object!!))


                    //Log.d("Imagelog", "base 64 image load: "+getBase64(bitmap_object!!))



                   // Log.d("Imagelog",imageview_add_image?.toString())

                    //imageview_add_image?.setImageBitmap(compressBitmap(bitmap_object!!))
                    //Toast.makeText(getApplicationContext(),picturePath,Toast.LENGTH_LONG).show();
                }
            }


        }
    }

    fun scaleBitmapDown(bitmap: Bitmap, maxDimension: Int): Bitmap {
        val originalWidth = bitmap.width
        val originalHeight = bitmap.height
        var resizedWidth = maxDimension
        var resizedHeight = maxDimension

        if (originalHeight > originalWidth) {
            resizedHeight = maxDimension
            resizedWidth = (resizedHeight * originalWidth.toFloat() / originalHeight.toFloat()).toInt()
        } else if (originalWidth > originalHeight) {
            resizedWidth = maxDimension
            resizedHeight = (resizedWidth * originalHeight.toFloat() / originalWidth.toFloat()).toInt()
        } else if (originalHeight == originalWidth) {
            resizedHeight = maxDimension
            resizedWidth = maxDimension
        }
        val a = "Width: $resizedWidth\nHeight: $resizedHeight"
        Log.d("imagedimen", a)
        return Bitmap.createScaledBitmap(bitmap, resizedWidth, resizedHeight, false)
    }

    fun getBase64(bitmap: Bitmap): String {
        val outputStream = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, outputStream)

        return Base64.encodeToString(outputStream.toByteArray(), Base64.DEFAULT)
    }

    fun compressBitmap(bitmap: Bitmap): Bitmap
    {
        val bmap = Bitmap.createScaledBitmap(bitmap, 100, 100, true)

        var baos: ByteArrayOutputStream? = ByteArrayOutputStream()
        bmap.compress(Bitmap.CompressFormat.JPEG, 100, baos)


        return bmap
    }


    private fun getImageByteAsString(bitmap: Bitmap): String {

        var bitmapStr: String = ""

        try {

            val bmap = Bitmap.createScaledBitmap(bitmap, 100, 100, true)

            var baos: ByteArrayOutputStream? = ByteArrayOutputStream()
            bmap.compress(Bitmap.CompressFormat.JPEG, 100, baos)
            val b = baos!!.toByteArray()

            // here chance to get Out Of Memory Exception
            bitmapStr = Base64.encodeToString(b, Base64.DEFAULT)

            baos.close()
            baos = null
        } catch (e: IOException) {
            // TODO Auto-generated catch block
            e.printStackTrace()
        }

        return bitmapStr
    }

    fun convertToByteArray(bitmap: Bitmap): ByteArray
    {
        val stream = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream)
        var byteArray = stream.toByteArray()
        return byteArray
    }
    fun convertToBitmap(byteArray: ByteArray): Bitmap {
        return BitmapFactory.decodeByteArray(byteArray, 0, byteArray.size)

    }

    private inner class SetImageFromUrl constructor( templateUrl: String ,view: ImageView ): AsyncTask<Object, Object, Object>()
    {

        private var templateUrl: String? = null
        private var view: ImageView? = null


        init {
            this.templateUrl = templateUrl
            this.view = view

        }


        override fun onPreExecute() {

            Log.d("load","in pre execute")
            Log.d("resumecalled","in pre  execute ")

            try
            {








            }
            catch (e: Exception )
            {
                e.printStackTrace();
            }


            super.onPreExecute()
        }
        override fun doInBackground(vararg params: Object?): Object? {

            try
        {

            Log.d("load","in do in background")

            Log.d("resumecalled","in do in background execute ")



            bitmap = Glide.with(this@GeneralPost)
                    .load(template_url).asBitmap()
                    .diskCacheStrategy(DiskCacheStrategy.NONE)
                    .into(Int.MAX_VALUE, Int.MAX_VALUE).get()



        }
        catch (e: Exception )
        {
            e.printStackTrace();
        }
        return null;
        }

        override fun onPostExecute(result: Object?) {

            Log.d("load","on post execute")

            Log.d("resumecalled","in post execute ")


            try {
                imageview_add_image?.setImageBitmap(bitmap)
            } catch(e: Exception) {
                Log.d("img",e.message)
                Log.d("img",e.printStackTrace().toString())

            } finally {
            }


            imageview_add_image?.background = null
            button_remove_image?.visibility = View.VISIBLE
            button_remove_image?.isClickable = true
            add_image?.isClickable = false
            add_image?.isEnabled = false
            base64 = false

        }

    }

    private inner class SetImageFromBitmap(bitmap: Bitmap,view: ImageView): AsyncTask<Void,Void,Void>()
    {


        private var bitmap: Bitmap? = null
        private var view: ImageView? = null


        init {
            this.bitmap = bitmap
            this.view = view

            try {
                Glide.get(applicationContext).bitmapPool.clearMemory()
            } catch(e: Exception) {
            } finally {
            }

        }
        override fun onPreExecute() {
            try {
                (imageview_add_image?.setImageBitmap(bitmap))
            } catch(e: Exception) {
                Log.d("img",e.message)
                Log.d("img",e.printStackTrace().toString())

            } finally {
            }
            super.onPreExecute()
        }

        override fun doInBackground(vararg params: Void?): Void? {
            try
            {

                Log.d("load","in do in background")

            }
            catch (e: Exception )
            {
                e.printStackTrace();
            }
           return null
        }

        override fun onPostExecute(result: Void?) {
            imageview_add_image?.background = null
            button_remove_image?.visibility = View.VISIBLE
            button_remove_image?.isClickable = true
            add_image?.isClickable = false
            add_image?.isEnabled = false
            base64 = true


            Log.d("post",""+base64)
            Log.d("post",""+bitmap_object.toString())

            super.onPostExecute(result)
        }

    }
/*
    fun getRotatedBitmap(filename: String): Bitmap
    {
                var bounds = BitmapFactory.Options();
        bounds.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(filename, bounds);

        var opts = BitmapFactory.Options();
        var bm = BitmapFactory.decodeFile(filename, opts);
        var exif =ExifInterface(filename);
        var orientString = exif.getAttribute(ExifInterface.TAG_ORIENTATION);
        var orientation: Int

        if(orientString != null)
        {
            orientation = Integer.parseInt(orientString)
        }
        else
        {
            orientation = ExifInterface.ORIENTATION_NORMAL
        }

        var rotationAngle = 0
        if (orientation == ExifInterface.ORIENTATION_ROTATE_90) rotationAngle = 90;
        if (orientation == ExifInterface.ORIENTATION_ROTATE_180) rotationAngle = 180;
        if (orientation == ExifInterface.ORIENTATION_ROTATE_270) rotationAngle = 270;

        var matrix = Matrix();
        matrix.setRotate(rotationAngle as Float,  bm.getWidth() as Float / 2,  bm.getHeight() as Float / 2);
        var rotatedBitmap = Bitmap.createBitmap(bm, 0, 0, bounds.outWidth, bounds.outHeight, matrix, true);

        return rotatedBitmap
    }*/

    fun getRotatedBitmap(imagePath: String): Bitmap
    {

            val f = File(imagePath)
            val exif = ExifInterface(f.getPath())
            val orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL)

            var angle = 0

            if (orientation == ExifInterface.ORIENTATION_ROTATE_90) {
                angle = 90
            } else if (orientation == ExifInterface.ORIENTATION_ROTATE_180) {
                angle = 180
            } else if (orientation == ExifInterface.ORIENTATION_ROTATE_270) {
                angle = 270
            }

            val mat = Matrix()
            mat.postRotate(angle.toFloat())

            val bmp = BitmapFactory.decodeStream(FileInputStream(f), null, null)
            val correctBmp = Bitmap.createBitmap(bmp, 0, 0, bmp.width, bmp.height, mat, true)

        return correctBmp

    }

}
