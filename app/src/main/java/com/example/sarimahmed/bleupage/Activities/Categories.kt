package com.example.sarimahmed.bleupage.Activities

import android.app.IntentService
import android.app.ProgressDialog
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.ListView
import android.widget.TextView
import com.android.volley.AuthFailureError
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Response
import com.android.volley.toolbox.JsonArrayRequest
import com.android.volley.toolbox.JsonObjectRequest
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.example.sarimahmed.bleupage.Adapter.AccountsDataAdapter
import com.example.sarimahmed.bleupage.Application.ApplicationController
import com.example.sarimahmed.bleupage.HelperClasses.AlertManager
import com.example.sarimahmed.bleupage.HelperClasses.BuildUrl
import com.example.sarimahmed.bleupage.HelperClasses.Constants
import com.example.sarimahmed.bleupage.HelperClasses.JsonKeys

import com.example.sarimahmed.bleupage.R
import org.jetbrains.anko.toast
import org.json.JSONArray
import org.json.JSONObject

class Categories : AppCompatActivity(),View.OnClickListener {


    private var homeToolbar: Toolbar? = null
    private var toolbar_title: TextView? = null
    private var button_next: Button? = null
    private var progressDialog: ProgressDialog? = null
    private var listView_categories: ListView? = null
    private var arraylist_categories: ArrayList<String>? = null
    private var textview_categories: TextView? = null
    private var arraylist_selected_cat: ArrayList<String>? = null
    private var status: Int? = null
    private var categoriesFetched: ArrayList<String>? = null
    private var time: String?= null
    private var postOnDays: ArrayList<Int>? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_categories)


        homeToolbar = findViewById(R.id.my_toolbar) as Toolbar
        setSupportActionBar(homeToolbar)
        //supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_user)


        super.setTitle("")



        toolbar_title = findViewById(R.id.toolbar_title_accounts_list) as TextView?



        supportActionBar?.setDefaultDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)


        button_next = findViewById(R.id.button_next) as Button
        button_next?.setOnClickListener(this)


        listView_categories = findViewById(R.id.categories_listview) as ListView?
        listView_categories?.choiceMode = ListView.CHOICE_MODE_MULTIPLE

        arraylist_categories = ArrayList()
        arraylist_selected_cat = ArrayList()
        categoriesFetched = ArrayList()
        postOnDays = ArrayList()



        loadData()



        listView_categories?.setOnItemClickListener { parent, view, position, id ->

            if(listView_categories?.isItemChecked(position) == true)
            {

                arraylist_selected_cat?.add(listView_categories?.getItemAtPosition(position) as String)


            }
            else if(listView_categories?.isItemChecked(position) == false)
            {
                if(arraylist_selected_cat?.contains(listView_categories?.getItemAtPosition(position)) == true)
                {
                    arraylist_selected_cat?.remove(listView_categories?.getItemAtPosition(position))
                }
            }

        }


    }

    override fun onBackPressed() {

        super.onBackPressed()
        overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left)

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.getItemId()

        if (id == android.R.id.home) {
            onBackPressed()
            overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left)
            finish()
            return true
        }

        return super.onOptionsItemSelected(item)
    }

    fun loadData() {

        progressDialog = ProgressDialog.show(this@Categories, "Fetching Data!", "Please wait....", true)
        progressDialog?.setCancelable(false)
        var request = object : JsonArrayRequest(Method.GET, Constants.URL_CATEGORIES, null, Response.Listener<JSONArray>()
        { response ->

            try {
                Log.d("response", ""+response.toString())

                for (i in 0..response.length() - 1) {

                    val category = response.getString(i)

                   arraylist_categories?.add(category)

                }


                val itemsAdapter = ArrayAdapter<String>(this, R.layout.categories_list_item, arraylist_categories)
                listView_categories?.adapter = itemsAdapter


                var intentData = intent
                var extras = intentData.extras
                if(extras.getString("FROM") != "createpost")
                {
                    getAutoPostConfig()
                    Log.d("response","not from createpost")
                }

                progressDialog?.dismiss()
            } catch (e: Exception) {
                progressDialog?.dismiss()

            } finally {
            }







        }, Response.ErrorListener { error ->

            progressDialog?.dismiss()

            var alert = AlertManager("Network Problem!",3000,this)


        }
        )
        {

            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                return Constants.getAuthHeader()

            }
        }

        request.setRetryPolicy(DefaultRetryPolicy(
                25000,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT))
        ApplicationController.instance?.addToRequestQueue(request)

    }

    override fun onClick(v: View?) {
        var id = v?.id

        when(id)
        {
            R.id.button_next ->
            {
                var intentData = intent
                var extras = intentData.extras
                Log.d("autopost",""+extras.getString("FROM"))

                if(extras.getString("FROM") == "createpost")
                {
                    if(arraylist_selected_cat!!.size == 0)
                    {
                        var alert = AlertManager("Select atleast one category",3000,this)
                    }
                    else
                    {


                        var intent = Intent(this@Categories,Schedule::class.java)
                        intent.putExtra("CATEGORIES",arraylist_selected_cat)
                        intent.putExtra("FROM",extras.getString("FROM"))
                        startActivity(intent)
                        overridePendingTransition(R.anim.enter_horizontal, R.anim.exit_horizontal)
                    }
                }
                else
                {
                    if(arraylist_selected_cat!!.size == 0)
                    {
                        var alert = AlertManager("Select atleast one category",3000,this)
                    }
                    else
                    {


                        var intent = Intent(this@Categories,Schedule::class.java)
                        intent.putExtra("CATEGORIES",arraylist_selected_cat)
                        intent.putExtra("FROM",extras.getString("FROM"))
                        intent.putExtra("TIME",time)
                        intent.putExtra("POST_ON_DAYS",postOnDays)
                        intent.putExtra("STATUS",status)
                        intent.putExtra("ID",extras.getInt("ID"))

                        startActivity(intent)
                        overridePendingTransition(R.anim.enter_horizontal, R.anim.exit_horizontal)
                    }
                }

            }
        }

    }

    fun getAutoPostConfig()
    {
       /* progressDialog = ProgressDialog.show(this@Categories, "Fetching Data!", "Please wait....", true)
        progressDialog?.setCancelable(false)*/
        var intentData = intent
        var extras = intentData.extras
        BuildUrl.setFetchAutoPostConfigUrl(extras.getString("FROM"),extras.getInt("ID"))
        Log.d("buildurl", BuildUrl.getFetchAutoPostConfigUrl())

        var request = object : JsonObjectRequest(Method.GET, BuildUrl.getFetchAutoPostConfigUrl(), null, Response.Listener<JSONObject>()
        { response ->

            try {
                //Log.d("response", ""+response.toString())

                    status = response.getInt(JsonKeys.variables.KEY_STATUS)
                    //status = result.getInt(JsonKeys.variables.KEY_STATUS)
                Log.d("response", "status: "+response.toString())




                var autoposting_configuration = response.getJSONObject("autoposting_configuration")

                time = autoposting_configuration.getString(JsonKeys.variables.KEY_TIME)
                Log.d("response", "time: "+time)


                for(i in 0..autoposting_configuration.getJSONArray(JsonKeys.arrays.KEY_CATEGORY).length() - 1)
                {
                    categoriesFetched?.add(autoposting_configuration.getJSONArray(JsonKeys.arrays.KEY_CATEGORY).get(i) as String)
                }
                for(i in 0..categoriesFetched!!.size - 1)
                {
                    Log.d("response", ""+"in for loop")
                    if(arraylist_categories?.contains(categoriesFetched?.get(i))!!)
                    {


                        for(j in 0..arraylist_categories!!.size - 1)
                        {

                            if(listView_categories?.getItemAtPosition(j)?.equals(categoriesFetched?.get(i)) == true)
                            {
                                listView_categories?.setItemChecked(j,true)
                                arraylist_selected_cat?.add(categoriesFetched?.get(i)!!)
                                Log.d("response", ""+arraylist_selected_cat.toString())
                            }

                        }

                    }
                }

                Log.d("response", "categoriesFetched: "+categoriesFetched.toString())
                Log.d("response", "json cat array: "+autoposting_configuration.getJSONArray(JsonKeys.arrays.KEY_CATEGORY).toString())



                for(i in 0..autoposting_configuration.getJSONArray(JsonKeys.arrays.KEY_POST_ON_DAYS).length() - 1)
                {
                    postOnDays?.add(autoposting_configuration.getJSONArray(JsonKeys.arrays.KEY_POST_ON_DAYS).get(i) as Int)
                }

                    //categoriesFetched = response.getJSONArray(JsonKeys.arrays.KEY_CATEGORY)
                    //Log.d("response", ""+status+" "+time+" "+categoriesFetched.toString()+" "+postOnDays.toString())





                //}







                //progressDialog?.dismiss()
            } catch (e: Exception) {
                //progressDialog?.dismiss()

            } finally {
            }


        }, Response.ErrorListener { error ->

            //progressDialog?.dismiss()


        }
        )
        {

            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                return Constants.getAuthHeader()

            }
        }

        request.setRetryPolicy(DefaultRetryPolicy(
                25000,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT))
        ApplicationController.instance?.addToRequestQueue(request)


    }


}
