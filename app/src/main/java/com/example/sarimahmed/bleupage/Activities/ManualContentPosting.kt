package com.example.sarimahmed.bleupage.Activities


import android.app.FragmentManager
import android.app.ProgressDialog
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.ArrayAdapter
import android.widget.ImageButton
import android.widget.TextView
import com.android.volley.AuthFailureError
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Response
import com.android.volley.toolbox.JsonArrayRequest
import com.example.sarimahmed.bleupage.Adapter.BlogFeedAdapter
import com.example.sarimahmed.bleupage.Adapter.QueueAdapter
import com.example.sarimahmed.bleupage.Application.ApplicationController
import com.example.sarimahmed.bleupage.Fragments.AddUrlToQueue
import com.example.sarimahmed.bleupage.Fragments.ContentFetchedListFragment
import com.example.sarimahmed.bleupage.HelperClasses.BuildUrl
import com.example.sarimahmed.bleupage.HelperClasses.Constants
import com.example.sarimahmed.bleupage.HelperClasses.JsonKeys

import com.example.sarimahmed.bleupage.R
import com.example.sarimahmed.bleupage.SessionManager.LoginManager
import org.json.JSONArray

class ManualContentPosting : AppCompatActivity(),View.OnClickListener {


    private var homeToolbar: Toolbar? = null
    private var toolbar_title: TextView? = null

    private var textview_title: TextView? = null
    private var textview_link: TextView? = null
    private var page_title: String? = null
    private var page_link: String? = null
    private var page_description: String? = null



    private var button_manual_posting: ImageButton? = null
    private var button_auto_configuration: ImageButton? =null
    private lateinit var fragManager: FragmentManager
    private lateinit var contentFetchedListFragment: ContentFetchedListFragment
    private lateinit var addUrlToQueueFragment: AddUrlToQueue
    private var bundle: Bundle? = null



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_manual_content_posting)

        homeToolbar = findViewById(R.id.my_toolbar) as Toolbar
        setSupportActionBar(homeToolbar)
        //supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_user)


        super.setTitle("")


        supportActionBar?.setDefaultDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        textview_title = findViewById(R.id.textView_title) as TextView?
        textview_link = findViewById(R.id.textView_link) as TextView?
        toolbar_title = findViewById(R.id.toolbar_title) as TextView?

        loadQueues()


        var intent = intent
        var extras = intent.extras

        try {
            page_title = extras.getString("PAGE_TITLE")
            page_link = extras.getString("PAGE_LINK")
            page_description = extras.getString("DESCRIPTION")
            textview_title?.text = page_title
            textview_link?.text = page_link
        } catch (e: Exception) {
        } finally {
        }




        button_manual_posting = findViewById(R.id.ic_manual_content_post) as ImageButton?
        button_auto_configuration = findViewById(R.id.ic_auto_content_post) as ImageButton?
        button_manual_posting?.setOnClickListener(this)
        button_auto_configuration?.setOnClickListener(this)


        bundle = Bundle()
        bundle?.putString("PAGE_TITLE",page_title)
        bundle?.putString("PAGE_LINK",page_link)
        bundle?.putString("DESCRIPTION",page_description)


        fragManager = fragmentManager

        contentFetchedListFragment = ContentFetchedListFragment()

        contentFetchedListFragment.setArguments(bundle)

        addUrlToQueueFragment = AddUrlToQueue()

        addUrlToQueueFragment.setArguments(bundle)

        loadManualPostingContent()





    }

    override fun onBackPressed() {

        try {
            BlogFeedAdapter.contentList?.clear()
            QueueAdapter?.queueList?.clear()
        } catch (e: Exception) {
        } finally {
        }

        super.onBackPressed()


    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId

        if (id == android.R.id.home) {
            onBackPressed()
            overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left)
            finish()
            return true
        }

        return super.onOptionsItemSelected(item)

    }

    fun loadManualPostingContent()
    {
        toolbar_title?.text = resources.getString(R.string.string_toolbarTitle_manual_posting)

        button_manual_posting?.imageTintList = resources.getColorStateList(R.color.smokeWhite)
        button_auto_configuration?.imageTintList = resources.getColorStateList(R.color.darkgrey)



        var transaction = fragManager.beginTransaction()
        transaction.replace(R.id.linear_layout_content,contentFetchedListFragment,"")
        transaction.commit()

    }

    fun loadAddtoQueueFrag()
    {

        toolbar_title?.text = resources.getString(R.string.string_toolbarTitle_auto_configuration)

        button_manual_posting?.imageTintList = resources.getColorStateList(R.color.darkgrey)
        button_auto_configuration?.imageTintList = resources.getColorStateList(R.color.smokeWhite)


        var transaction = fragManager.beginTransaction()
        transaction.replace(R.id.linear_layout_content, addUrlToQueueFragment,"")
        transaction.commit()
    }

    override fun onClick(v: View?) {

        when(v?.id)
        {
            R.id.ic_manual_content_post ->
            {
                loadManualPostingContent()
            }
            R.id.ic_auto_content_post ->
            {
                loadAddtoQueueFrag()
            }
        }
    }

    fun loadQueues() {


        var loginManager = LoginManager(this)

        BuildUrl.setFetchQueuesUrl(loginManager?.getUserID())


        Log.d("response", "in do in background")
        var request = object : JsonArrayRequest(Method.GET, BuildUrl.getFetchQueuesUrl(), null, Response.Listener<JSONArray> {

            response ->
            try {
                Log.d("response", "" + response.toString())



                for (i in 0..response.length() - 0) {

                    var result = response.getJSONObject(i)

                    var status = result.getInt(JsonKeys.variables.KEY_STATUS)


                    var queue_id = result.getInt(JsonKeys.variables.KEY_QUEUE_ID)
                    var queue_name = result.getString(JsonKeys.variables.KEY_NAME)

                    QueueAdapter.queueList?.add(QueueAdapter(queue_id, queue_name))





                }


                //Log.d("responseaccounts",""+queues_arraylist?.get(1).toString())
                //listview_queues?.adapter = QueuesListAdapter
                //QueuesListAdapter.notifyDataSetChanged()


            } catch (e: Exception) {
                Log.d("response", "" + e.message)
                Log.d("response", "" + e.printStackTrace().toString())

            } finally {

            }


        },
                Response.ErrorListener {

                    error ->



                }) {

            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                return Constants.getAuthHeader()
            }
        }

        request.setRetryPolicy(DefaultRetryPolicy(
                30000,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT))

        ApplicationController.instance?.addToRequestQueue(request)
    }
}
