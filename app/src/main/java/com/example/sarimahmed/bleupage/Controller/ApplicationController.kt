package com.example.sarimahmed.bleupage.Application


import android.app.Application
import android.graphics.Bitmap
import android.support.v4.util.LruCache
import android.text.TextUtils
import com.android.volley.Request

import com.android.volley.RequestQueue
import com.android.volley.toolbox.Volley
import com.example.sarimahmed.bleupage.BroadcastReceivers.ConnectivityReceiver
import com.example.sarimahmed.bleupage.HelperClasses.Constants
import android.annotation.SuppressLint
import java.security.SecureRandom
import java.security.cert.X509Certificate
import javax.net.ssl.*

import android.os.StrictMode




/**
 * Created by Averox on 6/12/2017.
 */

class ApplicationController : Application() {



    override fun onCreate() {
        super.onCreate()

        Constants.context = applicationContext
        instance = this
        handleSSLHandshake()

        val builder = StrictMode.VmPolicy.Builder()
        StrictMode.setVmPolicy(builder.build())







    }

    val requestQueue: RequestQueue? = null
        get() {
            if (field == null) {
                return Volley.newRequestQueue(Constants.context)
            }
            return field
        }

    fun <T> addToRequestQueue(request: Request<T>, tag: String) {
        request.tag = if (TextUtils.isEmpty(tag)) TAG else tag
        requestQueue?.add(request)
    }

    fun <T> addToRequestQueue(request: Request<T>) {
        request.tag = TAG
        requestQueue?.add(request)
    }

    fun cancelPendingRequests(tag: Any) {
        if (requestQueue != null) {
            requestQueue!!.cancelAll(tag)
        }
    }


    companion object {
        private val TAG = ApplicationController::class.java.simpleName
        @get:Synchronized var instance: ApplicationController? = null
            private set
    }



    fun setConnectivityListener(listener: ConnectivityReceiver.ConnectivityReceiverListener) {
        ConnectivityReceiver.connectivityReceiverListener = listener
    }

    /**
     * Enables https connections
     */
    @SuppressLint("TrulyRandom")
    fun handleSSLHandshake() {
        try {
            val trustAllCerts = arrayOf<TrustManager>(object : X509TrustManager {
                override fun getAcceptedIssuers(): Array<X509Certificate?> {
                    return arrayOfNulls(0)
                }

                override fun checkClientTrusted(certs: Array<X509Certificate>, authType: String) {}

                override fun checkServerTrusted(certs: Array<X509Certificate>, authType: String) {}
            })

            val sc = SSLContext.getInstance("SSL")
            sc.init(null, trustAllCerts, SecureRandom())
            HttpsURLConnection.setDefaultSSLSocketFactory(sc.socketFactory)
            HttpsURLConnection.setDefaultHostnameVerifier(object : HostnameVerifier {
                override fun verify(arg0: String, arg1: SSLSession): Boolean {
                    if(arg0.equals("www.bleupage.online",true) || arg0.equals("bleupage.online",true) || arg0.equals(".facebook.com",true)
                            || arg0.equals("*.xx.fbcdn.net",true) || arg0.equals("scontent.xx.fbcdn.net",true)
                            || arg0.equals("pbs.twimg.com",true) || arg0.equals("media.licdn.com",true)
                            || arg0.equals("scontent-iad3-1.cdninstagram.com",true) || arg0.equals("s-media-cache-ak0.pinimg.com",true)
                            || arg0.equals("lh6.googleusercontent.com",true) || arg0.equals("i.pinimg.com",true)
                            || arg0.equals("graph.facebook.com",true))
                    {

                        return true
                    }
                    else
                    {
                        return false
                    }


                }
            })
        } catch (ignored: Exception) {
        }

    }


}
