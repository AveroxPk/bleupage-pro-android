package com.example.sarimahmed.bleupage.HelperClasses

/**
 * Created by Averox on 6/19/2017.
 * This class contains all objects,arrays and variable names for JSON data of bleupage api
 */
public object JsonKeys {

    public object objects
    {
        const val KEY_USER_DATA = "data"
        const val KEY_USER_ID = "ID"
        const val KEY_USER_TYPE = "usertype"
        const val KEY_PRODUCTS = "products"
        const val KEY_RECORDS = "records"

    }
    public object arrays
    {
        const val KEY_PRODUCTS_STATUS = "products_status"
        const val KEY_FBACCOUNTS = "fbaccounts"
        const val KEY_TWACCOUNTS = "twaccounts"
        const val KEY_LIACCOUNTS = "liaccounts"
        const val KEY_INSTAACCOUTS = "instaaccounts"
        const val KEY_PINACCOUNTS = "pinaccounts"
        const val KEY_GPACCOUNTS = "gpaccounts"
        const val KEY_ACCOUNTS = "accounts"
        const val KEY_PAGES = "pages"
        const val KEY_GROUPS = "groups"
        const val KEY_TWITTER = "twitter"
        const val KEY_LINKEDIN = "linkedin"
        const val KEY_LICOMPANIES = "licompanies"
        const val KEY_GPCIRCLES = "gpcircles"
        const val KEY_GPCOLLECTIONS = "gpcollections"
        const val KEY_POST_ON_DAYS = "PostOnDays"
        const val KEY_CATEGORY =  "category"
        const val KEY_ENTRIES = "entries"




    }
    public object variables
    {
        const val KEY_USER_ID = "user_id"
        const val KEY_USER_LOGIN = "user_login"
        const val KEY_USER_PASSWORD = "user_pass"
        const val KEY_USER_NICENAME = "user_nicename"
        const val KEY_USER_EMAIL = "user_email"
        const val KEY_USER_URL = "user_url"
        const val KEY_USER_REGISTERED = "user_registered"
        const val KEY_USER_ACTIVATION_KEY = "user_activation_key"
        const val KEY_USER_STATUS = "user_status"
        const val KEY_USER_DISPLAY_NAME = "display_name"

        const val KEY_STATUS = "status"
        const val KEY_FRONTEND = "frontend"
        const val KEY_CONTENTS_FETCHER = "contents"
        const val KEY_WORDPRESS = "wordpress"
        const val KEY_AUTOMATION = "automation"
        const val KEY_IOS = "ios"

        const val KEY_ID = "id"
        const val KEY_FB_ID = "fb_id"
        const val KEY_NAME = "name"
        const val KEY_PICTURE = "picture"
        const val KEY_ACCESS_TOKEN = "access_token"
        const val KEY_PAGES_COUNT = "pages_count"
        const val KEY_POSTS_COUNT = "posts_count"
        const val KEY_USERNAME = "username"
        const val KEY_PASSWORD = "password"
        const val KEY_TOKEN = "token"
        const val KEY_GROUP_PRIVACY = "group_privacy"
        const val KEY_GP_CIRCLES_ID = "gp_circle_id"
        const val KEY_GP_COLLECTIONS_ID = "gp_collection_id"
        const val KEY_THUMBNAIL= "thumbnail"
        const val KEY_LINKEDIN_ID = "linkedin_id"
        const val KEY_COMPANY_ID = "company_id"
        const val KEY_WEBSITE_URL = "website_url"
        const val KEY_USER_ID_TWITTER = "user_id"
        const val KEY_GROUP_ID = "group_id"
        const val KEY_GROUP_NAME = "group_name"
        const val KEY_AUTOPOST_CONFIGURATION = "autoposting_configuration"
        const val KEY_TIME = "time"
        const val KEY_TITLE = "title"
        const val KEY_CONTENT = "content"
        const val KEY_LINK = "link"
        const val KEY_QUEUE_ID = "queueid"
        const val KEY_LOCAL_TIME = "localtime"
        const val KEY_LINKS = "links"
        const val KEY_ACCOUNTS = "accounts"
        const val KEY_DAYS = "days"
        const val KEY_MESSAGE = "message"
        const val KEY_DESCRIPTION = "description"


    }
}