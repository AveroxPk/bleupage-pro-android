package com.example.sarimahmed.bleupage.Activities

import android.content.Context
import android.content.Intent
import android.os.Build
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.annotation.RequiresApi
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.example.sarimahmed.bleupage.HelperClasses.Constants

import com.example.sarimahmed.bleupage.R
import com.example.sarimahmed.bleupage.SessionManager.LoginManager
import com.example.sarimahmed.bleupage.SessionManager.UserDataManager
import org.jetbrains.anko.toast

class AddAccountActivity : AppCompatActivity(),View.OnClickListener {


   // private var listView_connect_accounts: ListView? = null
    private var listView_others_add_accounts: ListView? = null
    private var listView_others_add_accounts_items: Array<String>? = null
    private var login: LoginManager? = null
    private var button_close: Button? = null




    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_account)


        //listView_connect_accounts = findViewById(R.id.listView_connect_accounts) as ListView?
        listView_others_add_accounts = findViewById(R.id.listView_others_add_accounts) as ListView?

        button_close = findViewById(R.id.button_close) as Button?
        button_close?.setOnClickListener(this)


        login = LoginManager(this)


        val textView = TextView(applicationContext)
        textView.text = "Connect"
        textView.textSize = R.dimen.textSize_large.toFloat()
        textView.gravity = Gravity.CENTER_HORIZONTAL
        textView.setTextAppearance(this, android.R.style.TextAppearance_DeviceDefault_Large)

        try {
           // listView_connect_accounts!!.addHeaderView(textView)
        } catch(e: Exception) {

            Log.d("header",""+e.message)
        }

        listView_others_add_accounts_items = resources.getStringArray(R.array.others_add_accounts)


        val itemsAdapter = ArrayAdapter<String>(this, R.layout.my_simple_list_item, listView_others_add_accounts_items)
        listView_others_add_accounts?.adapter = itemsAdapter


        //var connectListAdapter = ConnectListAdapter(this)
        //listView_connect_accounts!!.adapter = connectListAdapter

        listView_others_add_accounts?.setOnItemClickListener(AdapterView.OnItemClickListener()
        { adapterView: AdapterView<*>, view1: View, i: Int, l: Long ->


               when(i)
               {
                   0->
                   {
                       login?.logoutUser()
                       overridePendingTransition(R.anim.fade_in_animation, R.anim.fade_out_animation)

                       UserDataManager.FBAccountsList?.clear()
                       UserDataManager.TWAccountsList?.clear()
                       UserDataManager.LIAccountsList?.clear()
                       UserDataManager.INAccountsList?.clear()
                       UserDataManager.PINAccountsList?.clear()
                       UserDataManager.GPAccountsList?.clear()
                       UserDataManager.FBGroupList?.clear()
                       UserDataManager.LICompanyList?.clear()
                       UserDataManager.FBPagesList?.clear()
                       UserDataManager.GPCirclesList?.clear()
                       UserDataManager.GPCollectionsList?.clear()
                       UserDataManager.FBListForPost?.clear()
                       UserDataManager.LIListForPost?.clear()
                       UserDataManager?.GPListForPost?.clear()

                       finishAffinity()
                       //finish()
                   }
               }

        })


    }


    /*internal inner class ConnectListAdapter(private val context: Context) : BaseAdapter() {
        var listview_enteries: Array<String>
        var images = intArrayOf(R.drawable.ic_colored_facebook, R.drawable.ic_colored_twitter, R.drawable.ic_colored_linkedin, R.drawable.ic_colored_instagram, R.drawable.ic_colored_pinterest, R.drawable.ic_colored_googleplus)

        init {
            listview_enteries = context.resources.getStringArray(R.array.connect_accounts_enteries)
        }

        override fun getCount(): Int {
            return listview_enteries.size
        }

        override fun getItem(position: Int): Any {
            return listview_enteries[position]
        }

        override fun getItemId(position: Int): Long {
            return position.toLong()
        }

        override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {

            var row: View? = null
            if (convertView == null) {
                val inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
                row = inflater.inflate(R.layout.connect_accounts_list_item, parent, false)
            } else {
                row = convertView
            }
            val drawer_item_text = row!!.findViewById(R.id.drawer_item_text) as TextView
            val drawer_item_icon = row.findViewById(R.id.drawer_item_icon) as ImageView
            drawer_item_text.text = listview_enteries[position]
            try {
                drawer_item_icon.setImageResource(images[position])
            } catch (e: Exception) {
                //e.printStackTrace()
                Log.d("icon", e.message)
            }

            return row
        }
    }*/

    override fun onBackPressed() {
        super.onBackPressed()

        overridePendingTransition(R.anim.top_to_bottom, R.anim.bottom_to_top)
        finish()


    }

    override fun onClick(v: View?) {
        var id = v?.id

        when(id)
        {
            R.id.button_close ->
            {
                onBackPressed()
                overridePendingTransition(R.anim.top_to_bottom, R.anim.bottom_to_top)

            }
        }

    }
}


