package com.example.sarimahmed.bleupage.Activities

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler

import com.example.sarimahmed.bleupage.R

class SplashActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_splash)

        Handler().postDelayed(object : Runnable {

            // Using handler with postDelayed called runnable run method


                override fun run() {
                val i = Intent(this@SplashActivity, LoginActivity::class.java)
                startActivity(i)
                    overridePendingTransition(R.anim.fade_in_animation, R.anim.fade_out_animation)

                    // close this activity
                finish()
            }
        }, 2 * 1000)    //wait for 5 seconds
    }


}
