package com.example.sarimahmed.bleupage.Fragments

import android.annotation.TargetApi
import android.app.Fragment
import android.app.ProgressDialog
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.text.BoringLayout
import android.util.Log
import android.view.*
import android.widget.*
import com.android.volley.AuthFailureError
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Response
import com.android.volley.toolbox.JsonArrayRequest
import com.android.volley.toolbox.JsonObjectRequest
import com.example.sarimahmed.bleupage.Activities.AddQueueActivity
import com.example.sarimahmed.bleupage.Activities.GeneralPost
import com.example.sarimahmed.bleupage.Adapter.QueuesDataAdapter
import com.example.sarimahmed.bleupage.Application.ApplicationController
import com.example.sarimahmed.bleupage.HelperClasses.AlertManager
import com.example.sarimahmed.bleupage.HelperClasses.BuildUrl
import com.example.sarimahmed.bleupage.HelperClasses.Constants
import com.example.sarimahmed.bleupage.HelperClasses.JsonKeys
import com.example.sarimahmed.bleupage.R
import com.example.sarimahmed.bleupage.SessionManager.LoginManager
import kotlinx.android.synthetic.main.activity_schedule.*
import org.jetbrains.anko.toast
import org.json.JSONArray
import org.json.JSONObject

/**
 * Created by Averox on 9/19/2017.
 */
class AllQueues : Fragment(),View.OnClickListener,AdapterView.OnItemClickListener
{



    private var listview_queues: ListView? = null
    private var button_active: Button? = null
    private var button_inactive: Button? = null
    private var loginManager: LoginManager? = null
    private var active_queues: ArrayList<QueuesDataAdapter>? = null
    private var inactive_queues: ArrayList<QueuesDataAdapter>? = null
    /*private var time_arraylist: ArrayList<String>? = null
    private var localtime_arraylist: ArrayList<String>? = null
    private var days_arraylist: ArrayList<String>? = null
    private var accounts_arraylist: ArrayList<String>? = null*/
    private var queues_arraylist: ArrayList<QueuesDataAdapter>? = null
    private var progressDialog: ProgressDialog? = null
    private var queue_id: Int? = null
    private var localtime_arraylist: ArrayList<String>? = null
    private var days_arraylist: ArrayList<Int>? = null
    private var queue_accounts_arraylist: ArrayList<String>? = null
    private var queue_name: String? = null

    private var type: String = "active"



    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View {


        var view = inflater?.inflate(R.layout.layout_all_queues, container, false)

        listview_queues = view?.findViewById<ListView>(R.id.listview_queues)
        button_active = view?.findViewById<Button>(R.id.button_active)
        button_inactive = view?.findViewById<Button>(R.id.button_inactive)
        active_queues = ArrayList()
        inactive_queues = ArrayList()
        queues_arraylist = ArrayList()

/*
        time_arraylist = ArrayList()
        localtime_arraylist = ArrayList()
        days_arraylist = ArrayList()
        accounts_arraylist = ArrayList()
*/

        loginManager = LoginManager(Constants.context!!)


        return view!!
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {

        button_active?.setOnClickListener(this)
        button_inactive?.setOnClickListener(this)


      /*  var load = fetchQueuesData(loginManager?.getUserID()!!)
        load.execute()*/
        button_active?.setBackgroundColor(resources.getColor(R.color.darkred))
        button_inactive?.setBackgroundColor(resources.getColor(R.color.transparent))
        button_inactive?.setBackground(resources.getDrawable(R.drawable.border))

        listview_queues?.setOnItemClickListener(this)
        super.onActivityCreated(savedInstanceState)
    }

    override fun onResume() {
        super.onResume()
        loadActiveQueues()

    }




    override fun onClick(v: View?) {
        when(v?.id)
        {
            R.id.button_active ->
        {
            button_active?.setBackgroundColor(resources.getColor(R.color.darkred))
            button_inactive?.setBackgroundColor(resources.getColor(R.color.transparent))
            button_inactive?.setBackground(resources.getDrawable(R.drawable.border))


            loadActiveQueues()


        }
            R.id.button_inactive ->
            {
                button_inactive?.setBackgroundColor(resources.getColor(R.color.darkred))
                button_active?.setBackgroundColor(resources.getColor(R.color.transparent))
                button_active?.setBackground(resources.getDrawable(R.drawable.border))



                loadInActiveQueues()



            }
        }
    }

    fun loadActiveQueues()
    {
        type = "active"
       queues_arraylist?.clear()

        var QueuesListAdapter = CustomAdapter()

        listview_queues?.adapter = QueuesListAdapter
        QueuesListAdapter.notifyDataSetChanged()

        progressDialog = ProgressDialog.show(activity, "Fetching Data", "Please wait....", true)
        progressDialog?.setCancelable(false)

        BuildUrl.setFetchQueuesUrl(loginManager?.getUserID())


        Log.d("response","in do in background")

        var request = object : JsonArrayRequest(Method.GET,BuildUrl.getFetchQueuesUrl(),null, Response.Listener<JSONArray> {

            response ->
            try {
                Log.d("response",""+response.toString())



                for(i in 0..response.length() - 0)
                {

                   /* time_arraylist?.clear()
                    localtime_arraylist?.clear()
                    days_arraylist?.clear()
                    accounts_arraylist?.clear()*/

                    var result = response.getJSONObject(i)

                    var status = result.getInt(JsonKeys.variables.KEY_STATUS)

                    if(status == 1)
                    {

                    var queue_id = result.getInt(JsonKeys.variables.KEY_QUEUE_ID)
                    var user_id = result.getInt(JsonKeys.variables.KEY_USER_ID)
                    var name = result.getString(JsonKeys.variables.KEY_NAME)
                    //var time = result.getJSONArray(JsonKeys.variables.KEY_TIME)
                    var time = result.optJSONArray(JsonKeys.variables.KEY_TIME)

                    var localtime = result.optJSONArray(JsonKeys.variables.KEY_LOCAL_TIME)
                    var days = result.optJSONArray(JsonKeys.variables.KEY_DAYS)
                    var links = result.optJSONArray(JsonKeys.variables.KEY_LINKS)
                    var accounts = result.optJSONArray(JsonKeys.variables.KEY_ACCOUNTS)


                        var time_arraylist = ArrayList<String>()
                        var localtime_arraylist = ArrayList<String>()
                        var days_arraylist = ArrayList<Int>()
                        var accounts_arraylist = ArrayList<String>()
                        var links_arraylist = ArrayList<String>()





                        if(time != null)
                    {
                        for(j in 0..time.length() - 1)
                        {
                            time_arraylist?.add(time.get(j) as String)
                        }
                        Log.d("response",""+time_arraylist.toString())
                    }

                    if(localtime != null) {
                        for (k in 0..localtime.length() - 1) {
                            localtime_arraylist?.add(localtime.get(k) as String)
                        }
                        Log.d("response", "" + localtime_arraylist.toString())
                    }
                    if(days != null) {

                        for (l in 0..days.length() - 1)
                        {
                            var item = days.get(l) as String


                                    days_arraylist?.add(item.toInt())
                        }
                        Log.d("response", "" + days_arraylist.toString())
                    }

                    if(accounts != null) {
                        for (m in 0..accounts.length() - 1) {
                            //accounts_arraylist?.add(accounts.get(m) as String)
                            accounts_arraylist.add(accounts.get(m) as String)
                        }
                        Log.d("responseaccounts", "account: " + accounts_arraylist.toString())
                    }

                        if(links!= null) {

                            for (n in 0..links.length() - 1) {
                                //accounts_arraylist?.add(accounts.get(m) as String)
                                links_arraylist.add(links.get(n) as String)
                                //Log.d("response", "links: item item item" )

                            }
                            Log.d("responseaccounts", "links: " + links_arraylist.toString())
                        }

                 //   Log.d("response",""+time_arraylist.toString())

                        queues_arraylist?.add(QueuesDataAdapter(queue_id,user_id,name,time_arraylist!!,localtime_arraylist!!,days_arraylist!!,links_arraylist,accounts_arraylist!!,status))

                    }



                    //Log.d("responseaccounts",""+queues_arraylist?.get(1).toString())
                    listview_queues?.adapter = QueuesListAdapter
                    QueuesListAdapter.notifyDataSetChanged()

                }

                progressDialog?.dismiss()

            }

            catch (e: Exception)
            {
                Log.d("response",""+e.message)
                Log.d("response",""+e.printStackTrace().toString())
                progressDialog?.dismiss()

            }
            finally
            {

            }


        },
                Response.ErrorListener {

                    error ->

                    progressDialog?.dismiss()


                }      ){

            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                return Constants.getAuthHeader()
            }
        }

        request.setRetryPolicy(DefaultRetryPolicy(
                30000,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT))

        ApplicationController.instance?.addToRequestQueue(request)
    }
    fun loadInActiveQueues()
    {

        type = "inactive"


        queues_arraylist?.clear()

        var QueuesListAdapter = CustomAdapter()

        listview_queues?.adapter = QueuesListAdapter
        QueuesListAdapter.notifyDataSetChanged()


        progressDialog = ProgressDialog.show(activity, "Fetching Data", "Please wait....", true)
        progressDialog?.setCancelable(false)
        BuildUrl.setFetchQueuesUrl(loginManager?.getUserID())


        queues_arraylist?.clear()


        Log.d("response","in do in background")

        var request = object : JsonArrayRequest(Method.GET,BuildUrl.getFetchQueuesUrl(),null, Response.Listener<JSONArray> {

            response ->
            try {
                Log.d("response",""+response.toString())


                for(i in 0..response.length() - 1)
                {




                    var result = response.getJSONObject(i)
                    var status = result.getInt(JsonKeys.variables.KEY_STATUS)


                    if(status == 0)
                    {

                        var queue_id = result.getInt(JsonKeys.variables.KEY_QUEUE_ID)
                        var user_id = result.getInt(JsonKeys.variables.KEY_USER_ID)
                        var name = result.getString(JsonKeys.variables.KEY_NAME)
                        //var time = result.getJSONArray(JsonKeys.variables.KEY_TIME)
                        var time = result.optJSONArray(JsonKeys.variables.KEY_TIME)

                        var localtime = result.optJSONArray(JsonKeys.variables.KEY_LOCAL_TIME)
                        var days = result.optJSONArray(JsonKeys.variables.KEY_DAYS)
                        var links = result.optJSONArray(JsonKeys.variables.KEY_LINKS)
                        var accounts = result.optJSONArray(JsonKeys.variables.KEY_ACCOUNTS)

                        var time_arraylist = ArrayList<String>()
                        var localtime_arraylist = ArrayList<String>()
                        var days_arraylist = ArrayList<Int>()
                        var accounts_arraylist = ArrayList<String>()
                        var links_arraylist = ArrayList<String>()


                        if(time != null)
                    {
                        for(j in 0..time.length() - 1)
                        {
                            time_arraylist?.add(time.get(j) as String)
                        }
                        Log.d("response",""+time_arraylist.toString())
                    }

                    if(localtime != null) {
                        for (k in 0..localtime.length() - 1) {
                            localtime_arraylist?.add(localtime.get(k) as String)
                        }
                        Log.d("response", "" + localtime_arraylist.toString())
                    }
                    if(days != null) {
                        for (l in 0..days.length() - 1) {
                            var item = days.get(l) as String
                            days_arraylist?.add(item.toInt())
                        }
                        Log.d("response", "" + days_arraylist.toString())
                    }
                    if(accounts != null) {
                        for (m in 0..accounts.length() - 1) {
                            accounts_arraylist?.add(accounts.get(m) as String)
                        }
                        Log.d("responseaccounts", "account: " + accounts_arraylist.toString())
                    }


                        if(links!= null) {

                            for (n in 0..links.length() - 1) {
                                //accounts_arraylist?.add(accounts.get(m) as String)
                                links_arraylist.add(links.get(n) as String)
                                //Log.d("response", "links: item item item" )

                            }
                            Log.d("responseaccounts", "links: " + links_arraylist.toString())
                        }


                        //active_queues?.add(QueuesDataAdapter(queue_id,user_id,name,a,))

                        queues_arraylist?.add(QueuesDataAdapter(queue_id,user_id,name,time_arraylist!!,localtime_arraylist!!,days_arraylist!!,links_arraylist,accounts_arraylist!!,status))


                    }




                    listview_queues?.adapter = QueuesListAdapter
                    QueuesListAdapter.notifyDataSetChanged()


                }
                progressDialog?.dismiss()

            } catch (e: Exception) {
                progressDialog?.dismiss()

            } finally {
            }


        },
                Response.ErrorListener {

                    error ->
                    progressDialog?.dismiss()


                }      ){

            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                return Constants.getAuthHeader()
            }
        }

        request.setRetryPolicy(DefaultRetryPolicy(
                30000,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT))

        ApplicationController.instance?.addToRequestQueue(request)
    }

    override fun onItemClick(v: AdapterView<*>?, p1: View?, position: Int, p3: Long) {
        queue_id = queues_arraylist?.get(position)?.getQueueId()
        Log.d("queue",""+queue_id)
        localtime_arraylist = queues_arraylist?.get(position)?.getLocalTime()
        queue_name = queues_arraylist?.get(position)?.getName()
        queue_accounts_arraylist = queues_arraylist?.get(position)?.getAccounts()
        days_arraylist = queues_arraylist?.get(position)?.getDays()
        registerForContextMenu(listview_queues)
        listview_queues?.showContextMenu()

    }

    override fun onCreateContextMenu(menu: ContextMenu?, v: View?, menuInfo: ContextMenu.ContextMenuInfo?) {

        if(type.equals("active"))
        {
            activity.getMenuInflater().inflate(R.menu.active_queues_context_menu, menu)

        }
        else if(type.equals("inactive"))
        {
            activity.getMenuInflater().inflate(R.menu.inactive_queues_context_menu, menu)

        }

        super.onCreateContextMenu(menu, v, menuInfo)
    }

    override fun onContextItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {


        R.id.context_pause ->
        {

            var request = object : JsonObjectRequest(Method.GET,BuildUrl.getPauseQueueUrl(queue_id!!),null,Response.Listener<JSONObject>
            {
                response ->

            Log.d("response",""+response.toString())

                if(type.equals("active"))
                {
                    loadActiveQueues()

                }
                else if(type.equals("inactive"))
                {
                    loadInActiveQueues()

                }

            },
                    Response.ErrorListener {

                error ->

                        AlertManager("Unsuccessfull",2000,activity)


            })
            {
                @Throws(AuthFailureError::class)
                override fun getHeaders(): Map<String, String> {
                    return Constants.getAuthHeader()
                }
            }

            request.setRetryPolicy(DefaultRetryPolicy(
                    25000,
                    0,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT))

            ApplicationController.instance?.addToRequestQueue(request)

            return true

        }
        R.id.context_activate ->
        {

            var request = object : JsonObjectRequest(Method.GET,BuildUrl.getActivateQueueUrl(queue_id!!),null,Response.Listener<JSONObject>
            {
                response ->

                Log.d("response",""+response.toString())

                if(type.equals("active"))
                {
                    loadActiveQueues()
                }
                else if(type.equals("inactive"))
                {
                    loadInActiveQueues()

                }

            },
                    Response.ErrorListener {

                        error ->

                        AlertManager("Unsuccessfull",2000,activity)


                    })
            {
                @Throws(AuthFailureError::class)
                override fun getHeaders(): Map<String, String> {
                    return Constants.getAuthHeader()
                }
            }

            request.setRetryPolicy(DefaultRetryPolicy(
                    25000,
                    0,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT))

            ApplicationController.instance?.addToRequestQueue(request)


            return true

        }
            R.id.context_edit ->
            {

                var intent = Intent(activity,AddQueueActivity::class.java)

                intent.putExtra("TITLE","edit_queue")
                intent.putExtra("QUEUE_NAME",queue_name)
                intent.putExtra("QUEUE_ID",queue_id)
                intent.putExtra("DAYS",days_arraylist)
                intent.putExtra("LOCALTIME",localtime_arraylist)
                intent.putExtra("QUEUE_ACCOUNTS",queue_accounts_arraylist)


                startActivity(intent)
                activity.overridePendingTransition(R.anim.enter_vertical, R.anim.exit_vertical)


                return true

            }
            R.id.context_delete ->
            {
                var request = object : JsonObjectRequest(Method.GET,BuildUrl.getDeleteQueueUrl(queue_id!!),null,Response.Listener<JSONObject>
                {
                    response ->

                    Log.d("response",""+response.toString())

                    if(type.equals("active"))
                    {
                        loadActiveQueues()
                    }
                    else if(type.equals("inactive"))
                    {
                        loadInActiveQueues()
                    }

                },
                        Response.ErrorListener {

                            error ->

                            AlertManager("Unsuccessfull",2000,activity)


                        })
                {
                    @Throws(AuthFailureError::class)
                    override fun getHeaders(): Map<String, String> {
                        return Constants.getAuthHeader()
                    }
                }

                request.setRetryPolicy(DefaultRetryPolicy(
                        25000,
                        0,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT))

                ApplicationController.instance?.addToRequestQueue(request)

                return true
            }


        else -> return super.onContextItemSelected(item)


    }

    }
    /*private inner class fetchQueuesData(user_id: Int): AsyncTask<Void, Void, Void>()
    {


        private var user_id: Int? = null



        init {
            this.user_id = user_id


            try {
            } catch(e: Exception) {
            } finally {
            }

        }
        override fun onPreExecute() {
            try {
                
                BuildUrl.setFetchQueuesUrl(user_id)
                Log.d("response",""+BuildUrl.getFetchQueuesUrl())

            } catch(e: Exception) {

            } finally {
            }
            super.onPreExecute()
        }

        override fun doInBackground(vararg params: Void?): Void? {
            try
            {


            }
            catch (e: Exception )
            {
                e.printStackTrace();
            }
            return null
        }

        override fun onPostExecute(result: Void?) {


            super.onPostExecute(result)
        }
    }*/
         inner class CustomAdapter : ArrayAdapter<QueuesDataAdapter>(Constants.context,R.layout.queues_list_item,queues_arraylist) {



            override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
                var convertView = convertView

                if (convertView == null) {
                    convertView = activity.layoutInflater.inflate(R.layout.queues_list_item, parent, false)
                    Log.d("response","in custom class")
                }
                Log.d("response","in custom class")

                var queuesDataView = queues_arraylist?.get(position)

                // Log.d("response",""+queuesDataView?.getName())
                //Log.d("response",""+queuesDataView?.getStatus())

                var imageview_active_inactive = convertView!!.findViewById<ImageView>(R.id.imageview_active_inactive)
                var textview_queue_name = convertView!!.findViewById<TextView>(R.id.textview_queue_name)
                var textview_url = convertView!!.findViewById<TextView>(R.id.textView_url)
                var imageview_fb = convertView!!.findViewById<ImageView>(R.id.imageview_fb)
                var imageview_tw = convertView!!.findViewById<ImageView>(R.id.imageview_tw)
                var imageview_li = convertView!!.findViewById<ImageView>(R.id.imageview_li)
                var imageview_gp = convertView!!.findViewById<ImageView>(R.id.imageview_gp)

                imageview_fb?.visibility = View.GONE
                imageview_tw?.visibility = View.GONE
                imageview_li?.visibility = View.GONE
                imageview_gp?.visibility = View.GONE

                if(queuesDataView?.getStatus() == 1)
                    {
                        imageview_active_inactive?.setImageDrawable(resources.getDrawable(R.drawable.active_circle))
                    }
                else if(queuesDataView?.getStatus() == 0)
                    {
                        imageview_active_inactive?.setImageDrawable(resources.getDrawable(R.drawable.inactive_circle))
                    }
                textview_queue_name?.setText(queuesDataView?.getName())

                Log.d("responseaccounts", "account: " + queuesDataView?.getAccounts().toString())

                textview_url?.setText(queuesDataView?.getLinks().toString())
               // Log.d("responseaccounts",""+a.toString())

                for(i in 0..queuesDataView?.getAccounts()!!.size - 1)
                {



                    var temp = queuesDataView?.getAccounts()?.get(i)
                    var item = temp?.split(":")
                    Log.d("responseaccounts",""+item)

                    if(item!![0].equals("fb") || item!![0].equals("pg") || item!![0].equals("gp"))
                    {
                        imageview_fb?.visibility = View.VISIBLE
                    }
                    else if(item!![0].equals("tw"))
                    {
                        imageview_tw?.visibility = View.VISIBLE

                    }

                    else if(item!![0].equals("li") || item!![0].equals("lic"))
                    {
                        imageview_li?.visibility = View.VISIBLE

                    }

                    else if(item!![0].equals("gpu") || item!![0].equals("gpl") || item!![0].equals("gpc"))
                    {
                        imageview_gp?.visibility = View.VISIBLE

                    }


                }






                return convertView
            }
        }


}