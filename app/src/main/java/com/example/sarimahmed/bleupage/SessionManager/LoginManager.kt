package com.example.sarimahmed.bleupage.SessionManager

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.widget.Toast
import com.example.sarimahmed.bleupage.Activities.LoginActivity
import com.example.sarimahmed.bleupage.HelperClasses.Constants
import com.example.sarimahmed.bleupage.R

/**
 * Created by Averox on 6/19/2017.
 */
class LoginManager(var context: Context) {

    private var pref: SharedPreferences? = null
    internal var editor: SharedPreferences.Editor? = null
    internal var PRIVATE_MODE = 0
    internal var _context: Context? = null
    val isLoggedIn: Boolean
        get() = pref!!.getBoolean(IS_LOGIN, false)

    init {
        this._context = Constants.context
        pref = _context?.getSharedPreferences(PREF_NAME, PRIVATE_MODE)
        editor = pref?.edit()
    }

    companion object {

        // Sharedpref file name
        private val PREF_NAME = "UserLoginPref"

        private val IS_LOGIN = "IsLoggedIn"

        // All Shared Preferences Keys

        val KEY_USERNAME = "username"

        val KEY_PASSWORD = "password"

        val KEY_PASS = "pass"


        val KEY_USER_ID = "user_id"

        val KEY_USER_DISPLAY_NAME = "display_name"

        var KEY_USER_STATUS = "status"

        var KEY_AUTOMATION = "automation"

        var KEY_CONTENT_FETCHER = "contents"

        var KEY_APP_PURCHASE_STATUS = "purchase_status"


    }

    //this function creates login session

    fun createLoginSession(username: String, password: String, user_id: Int, display_name: String, status: Boolean, automation: Boolean, contents: Boolean, ios: Boolean) {
        // Storing login value as TRUE

        // Storing data in sharedpref
        editor!!.putString(KEY_USERNAME, username)
        editor!!.putString(KEY_PASSWORD, password)
        editor!!.putInt(KEY_USER_ID, user_id)
        editor!!.putString(KEY_USER_DISPLAY_NAME, display_name)
        editor!!.putBoolean(IS_LOGIN, true)
        editor!!.putBoolean(KEY_USER_STATUS, status)
        editor!!.putBoolean(KEY_AUTOMATION, automation)
        editor!!.putBoolean(KEY_APP_PURCHASE_STATUS, ios)
        editor!!.putBoolean(KEY_CONTENT_FETCHER, contents)


        // commit changes
        editor!!.commit()


    }

    fun savePass(password: String?)
    {
        editor!!.putString(KEY_PASS, password)
        editor!!.commit()

    }

    //this function checks the login session is already created or not

    fun checkLogin() {
        // Check login status
        if (!this.isLoggedIn) {

            // user is not logged in redirect him to LoginActivity Activity
            val i = Intent(Constants.context, LoginActivity::class.java)
            // Closing all the Activities
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
            // Add new Flag to start new Activity
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
            // Starting LoginActivity Activity
            Constants.context?.startActivity(i)

        }


    }


    fun logoutUser() {
        // Clearing all data from Shared Preferences
        editor!!.clear()
        editor!!.commit()

        // After logout redirect user to Loing Activity
        val i = Intent(Constants.context, LoginActivity::class.java)
        // Closing all the Activities
        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)


        // Add new Flag to start new Activity
        i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK)


        // Starting LoginActivity Activity
        Constants.context?.startActivity(i)

    }

    // Get Login State
    fun isLogedIn(): Boolean {
        return pref!!.getBoolean(IS_LOGIN, false)
    }


    fun getDisplayName(): String {

        return pref!!.getString(KEY_USER_DISPLAY_NAME, "Anonymous")
    }

    fun getUserID(): Int {
        return pref!!.getInt(KEY_USER_ID, 0)

    }

    fun getUserName(): String
    {
        return pref!!.getString(KEY_USERNAME, null)
    }


    fun getPassword(): String{
        return pref!!.getString(KEY_PASSWORD,  "Anonymous")

    }

    fun getPass(): String{
        return pref!!.getString(KEY_PASS,  "Anonymous")

    }

    fun getUserStatus(): Boolean{
        return pref!!.getBoolean(KEY_USER_STATUS,  false)

    }
    fun getAutomationStatus(): Boolean
    {
        return pref!!.getBoolean(KEY_AUTOMATION,  false)

    }
    fun getPurchaseStatus(): Boolean
    {
        return pref!!.getBoolean(KEY_APP_PURCHASE_STATUS,false)
    }

    fun getContentFetcherStatus(): Boolean
    {
        return pref!!.getBoolean(KEY_CONTENT_FETCHER,  false)

    }

}