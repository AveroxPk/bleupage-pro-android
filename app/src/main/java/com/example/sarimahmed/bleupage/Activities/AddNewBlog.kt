package com.example.sarimahmed.bleupage.Activities

import android.app.FragmentManager
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.*
import com.android.volley.AuthFailureError
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Response
import com.android.volley.toolbox.JsonArrayRequest
import com.android.volley.toolbox.JsonObjectRequest
import com.example.sarimahmed.bleupage.Adapter.BlogFeedAdapter
import com.example.sarimahmed.bleupage.Adapter.QueuesDataAdapter
import com.example.sarimahmed.bleupage.Application.ApplicationController
import com.example.sarimahmed.bleupage.Fragments.AllQueues
import com.example.sarimahmed.bleupage.Fragments.ContentFetchedListFragment
import com.example.sarimahmed.bleupage.Fragments.VerifyUrl
import com.example.sarimahmed.bleupage.HelperClasses.AlertManager
import com.example.sarimahmed.bleupage.HelperClasses.BuildUrl
import com.example.sarimahmed.bleupage.HelperClasses.Constants
import com.example.sarimahmed.bleupage.HelperClasses.JsonKeys

import com.example.sarimahmed.bleupage.R
import com.example.sarimahmed.bleupage.SessionManager.LoginManager
import kotlinx.android.synthetic.main.activity_add_new_blog.*
import org.jetbrains.anko.alert
import org.jetbrains.anko.toast
import org.json.JSONArray
import org.json.JSONObject

class AddNewBlog : AppCompatActivity(),View.OnClickListener {

    private var homeToolbar: Toolbar? = null
    private var toolbar_title: TextView? = null

    private var button_add_blog_url: ImageButton? = null
    private var button_all_queues: ImageButton? = null
    private lateinit var verifyUrlFrag: VerifyUrl
    private lateinit var allQueuesFrag: AllQueues
    private lateinit var fragManager: FragmentManager
    private var button_activity_add_queue: ImageButton? = null
    private var fragment_type = "VerifyUrl"



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_new_blog)

        homeToolbar = findViewById(R.id.my_toolbar) as Toolbar
        toolbar_title = findViewById(R.id.toolbar_title) as TextView?
        setSupportActionBar(homeToolbar)
        //supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_user)

        super.setTitle("")

        supportActionBar?.setDefaultDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)



        button_add_blog_url = findViewById(R.id.ic_add_blog_url) as ImageButton?
        button_add_blog_url?.setOnClickListener(this)

        button_all_queues = findViewById(R.id.ic_all_queues) as ImageButton?
        button_all_queues?.setOnClickListener(this)
        button_activity_add_queue = findViewById(R.id.button_activity_add_queue) as ImageButton?
        button_activity_add_queue?.setOnClickListener(this)

        button_add_blog_url?.imageTintList = resources.getColorStateList(R.color.darkgrey)
        button_all_queues?.imageTintList = resources.getColorStateList(R.color.darkgrey)



        fragManager = fragmentManager
        verifyUrlFrag = VerifyUrl()
        allQueuesFrag = AllQueues()
        loadVerifyUrlFragment()


    }

    override fun onBackPressed() {
        super.onBackPressed()

    }

    override fun onResume() {
        try {
           /* if(fragment_type.equals("AllQueues"))
            {
                var transaction = fragManager.beginTransaction()
                transaction.detach(allQueuesFrag)
                transaction.replace(R.id.linaer_layout_add_new_blog_main,allQueuesFrag,"")

                transaction.commit()
            }*/
        } catch (e: Exception) {
            Log.d("frag",""+e.message.toString())
        } finally {
        }
        super.onResume()
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId

        if (id == android.R.id.home) {
            onBackPressed()
            overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left)
            finish()
            return true
        }

        return super.onOptionsItemSelected(item)
    }
    override fun onClick(v: View?) {
    when(v?.id)
    {
        R.id.ic_add_blog_url ->
        {
            loadVerifyUrlFragment()
           /* button_add_blog_url?.imageTintList = resources.getColorStateList(R.color.white)

            button_all_queues?.imageTintList = resources.getColorStateList(R.color.darkgrey)
            var transaction = fragManager.beginTransaction()
            transaction.replace(R.id.linaer_layout_add_new_blog_main,verifyUrlFrag,"")
            transaction.commit()*/
        }
        R.id.ic_all_queues ->
        {
                loadAllQueuesFragment()
           /* button_add_blog_url?.imageTintList = resources.getColorStateList(R.color.darkgrey)
            button_all_queues?.imageTintList = resources.getColorStateList(R.color.white)

            var transaction = fragManager.beginTransaction()
            transaction.replace(R.id.linaer_layout_add_new_blog_main,allQueuesFrag,"")
            transaction.commit()*/
        }
        R.id.button_activity_add_queue ->
        {

            var loginManager = LoginManager(this@AddNewBlog)
            BuildUrl.setFetchQueuesUrl(loginManager?.getUserID())

            var request = object : JsonArrayRequest(Method.GET,BuildUrl.getFetchQueuesUrl(),null, Response.Listener<JSONArray> {

                response ->
                Log.d("response",""+response.toString())

                try {

                    if(response.length()>=5)
                    {
                        alert("You have exceeded your queue limit").show()
                    }
                    else
                    {
                        val intent = Intent(this, AddQueueActivity::class.java)
                        startActivity(intent)
                        overridePendingTransition(R.anim.enter_vertical, R.anim.exit_vertical)
                    }









                } catch (e: Exception) {

                } finally {
                }


            },
                    Response.ErrorListener {

                        error ->


                    }      ){

                @Throws(AuthFailureError::class)
                override fun getHeaders(): Map<String, String> {
                    return Constants.getAuthHeader()
                }
            }

            request.setRetryPolicy(DefaultRetryPolicy(
                    30000,
                    0,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT))

            ApplicationController.instance?.addToRequestQueue(request)

        }
    }

    }

    fun loadVerifyUrlFragment()
    {
        fragment_type = "VerifyUrl"

        toolbar_title?.text = resources.getString(R.string.string_toolbarTitle_add_new_blog)

        button_add_blog_url?.imageTintList = resources.getColorStateList(R.color.white)

        button_all_queues?.imageTintList = resources.getColorStateList(R.color.darkgrey)
        var transaction = fragManager.beginTransaction()
        transaction.replace(R.id.linaer_layout_add_new_blog_main,verifyUrlFrag,"")
        transaction.commit()

    }

    fun loadAllQueuesFragment()
    {

        fragment_type = "AllQueues"

        toolbar_title?.text = resources.getString(R.string.string_toolbarTitle_all_queues)

        button_add_blog_url?.imageTintList = resources.getColorStateList(R.color.darkgrey)
        button_all_queues?.imageTintList = resources.getColorStateList(R.color.white)
        var transaction = fragManager.beginTransaction()
        transaction.replace(R.id.linaer_layout_add_new_blog_main,allQueuesFrag,"")
        transaction.commit()



    }




}
