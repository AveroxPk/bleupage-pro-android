package com.example.sarimahmed.bleupage.Activities

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.MotionEvent
import android.view.View
import android.widget.Button
import android.widget.ImageView
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.example.sarimahmed.bleupage.Application.ApplicationController
import com.example.sarimahmed.bleupage.HelperClasses.AlertManager
import com.example.sarimahmed.bleupage.HelperClasses.BuildUrl
import com.example.sarimahmed.bleupage.HelperClasses.JsonKeys

import com.example.sarimahmed.bleupage.R
import com.example.sarimahmed.bleupage.SessionManager.LoginManager
import com.example.sarimahmed.bleupage.SessionManager.UserDataManager
import org.jetbrains.anko.toast
import org.json.JSONObject

class CreatePostActivity : AppCompatActivity(), View.OnTouchListener,View.OnClickListener{

    private var general_post: ImageView? = null
    private var discount_coupon: ImageView? = null
    private var autopost: ImageView? = null
    private var content_fetcher: ImageView? = null
    private var button_home: Button? = null
    private var loginManager: LoginManager? = null
    private var status: Boolean? = false
    private var automation: Boolean? = false
    private var ios: Boolean? = false




    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_post)

        loginManager = LoginManager(this@CreatePostActivity)




       //checkPackages()
        if(UserDataManager.user_status == true && UserDataManager.automation_status == true)
        {


            //Log.d("abc","automation not purchased in if")
            return
        }
        else
        {

            Log.d("abc","automation  purchased in else")

            autopost = findViewById(R.id.autopost) as ImageView?

            autopost?.setImageDrawable(resources.getDrawable(R.drawable.autopost_default))
            autopost?.setOnTouchListener(this@CreatePostActivity)
            autopost?.setOnClickListener(this@CreatePostActivity)
        }

        if(UserDataManager.user_status == true && UserDataManager.content_fetcher_status == true)
        {


            //Log.d("abc","automation not purchased in if")
            return
        }
        else
        {

            Log.d("abc","content fetcher in else")


            content_fetcher = findViewById(R.id.content_fetcher) as ImageView?
            content_fetcher?.setImageDrawable(resources.getDrawable(R.drawable.content_fetcher_default))

            content_fetcher?.setOnTouchListener(this@CreatePostActivity)
        content_fetcher?.setOnClickListener(this@CreatePostActivity)
        }





        general_post = findViewById(R.id.general_post) as ImageView?
        general_post?.setOnClickListener(this)
        general_post?.setOnTouchListener(this)
        discount_coupon = findViewById(R.id.discount_coupon) as ImageView?
        discount_coupon?.setOnTouchListener(this)
        discount_coupon?.setOnClickListener(this)


        button_home = findViewById(R.id.button_home) as Button?
        button_home?.setOnClickListener(this)

    }

    override fun onBackPressed() {
        super.onBackPressed()
        overridePendingTransition(R.anim.top_to_bottom, R.anim.bottom_to_top)
    }
    override fun onTouch(v: View?, event: MotionEvent?): Boolean {
        var id =v?.id

        when(id)
        {
            R.id.general_post ->
            {


                if (event?.getAction() === MotionEvent.ACTION_DOWN) {
                    general_post?.setImageResource(R.drawable.general_post_pressed)

                }
                if (event?.getAction() === MotionEvent.ACTION_UP) {
                    general_post?.setImageResource(R.drawable.general_post_default)


                }

            }
            R.id.discount_coupon ->
            {
                if (event?.getAction() === MotionEvent.ACTION_DOWN) {
                    discount_coupon?.setImageResource(R.drawable.discount_coupons_pressed)

                }
                if (event?.getAction() === MotionEvent.ACTION_UP) {
                    discount_coupon?.setImageResource(R.drawable.discount_coupon_default)


                }
            }
            R.id.autopost ->
            {
                if (event?.getAction() === MotionEvent.ACTION_DOWN) {
                    autopost?.setImageResource(R.drawable.autopost_pressed)

                }
                if (event?.getAction() === MotionEvent.ACTION_UP) {
                    autopost?.setImageResource(R.drawable.autopost_default)


                }
            }
            R.id.content_fetcher ->
            {
                if (event?.getAction() === MotionEvent.ACTION_DOWN) {
                    content_fetcher?.setImageResource(R.drawable.content_fetcher_pressed)

                }
                if (event?.getAction() === MotionEvent.ACTION_UP) {
                    content_fetcher?.setImageResource(R.drawable.content_fetcher_default)


                }
            }
        }

        return super.onTouchEvent(event)
    }

    override fun onClick(v: View?) {
        var id = v?.id

        when(id)
        {
            R.id.button_home ->
            {
                onBackPressed()
                overridePendingTransition(R.anim.top_to_bottom, R.anim.bottom_to_top)
            }
            R.id.general_post ->
            {
                val intent = Intent(this, GeneralPost::class.java)
                intent.putExtra("TITLE",resources.getString(R.string.string_toolbarTitle_generalPost))
                startActivity(intent)
                overridePendingTransition(R.anim.enter_horizontal, R.anim.exit_horizontal)

               /* val intent = Intent(this, GeneralPost::class.java)
                startActivity(intent)
                overridePendingTransition(R.anim.right_to_left, R.anim.left_to_right);*/

            }
            R.id.discount_coupon ->
            {
                val intent = Intent(this, Templates_Discount_Post::class.java)
                intent.putExtra("TITLE",resources.getString(R.string.string_toolbarTitle_discountPost))
                startActivity(intent)
                overridePendingTransition(R.anim.enter_horizontal, R.anim.exit_horizontal)

            }
            R.id.autopost ->
            {
                val intent = Intent(this, Categories::class.java)
                intent.putExtra("FROM","createpost")
                startActivity(intent)
                overridePendingTransition(R.anim.enter_horizontal, R.anim.exit_horizontal)
            }
            R.id.content_fetcher ->
            {
                val intent = Intent(this, AddNewBlog::class.java)
                startActivity(intent)
                overridePendingTransition(R.anim.enter_horizontal, R.anim.exit_horizontal)
            }
        }
    }


}
