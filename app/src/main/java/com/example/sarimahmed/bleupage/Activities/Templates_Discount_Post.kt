package com.example.sarimahmed.bleupage.Activities

import android.content.Intent
import android.content.IntentFilter
import android.net.ConnectivityManager
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.android.volley.AuthFailureError
import com.android.volley.Response
import com.android.volley.toolbox.ImageLoader
import com.android.volley.toolbox.JsonArrayRequest
import com.example.sarimahmed.bleupage.Adapter.TemplatesAdapter
import com.example.sarimahmed.bleupage.Application.ApplicationController
import com.example.sarimahmed.bleupage.BroadcastReceivers.ConnectivityReceiver
import com.example.sarimahmed.bleupage.HelperClasses.Constants
import com.example.sarimahmed.bleupage.HelperClasses.JsonKeys

import com.example.sarimahmed.bleupage.R
import org.json.JSONArray
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.drawable.GlideDrawable
import com.bumptech.glide.request.RequestListener
import com.example.sarimahmed.bleupage.HelperClasses.BuildUrl
import com.example.sarimahmed.bleupage.HelperClasses.SquaredImageView
import org.jetbrains.anko.alert
import java.util.*



class Templates_Discount_Post : AppCompatActivity() , ConnectivityReceiver.ConnectivityReceiverListener{

    private var homeToolbar: Toolbar? = null
    private var button_next: Button? = null

    internal var isInternetConnected = ConnectivityReceiver.isConnected
    private var ConnectionReceiver: ConnectivityReceiver? = null
    private var arraylist_templates: ArrayList<TemplatesAdapter>? = null
    private var arraylist_templates_reverse: ArrayList<TemplatesAdapter>? = null

    private var gridView: GridView? = null
    private var imageview: ImageView? = null

    private var title: String = ""



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_templates__discount__post)

        homeToolbar = findViewById(R.id.my_toolbar) as Toolbar
        setSupportActionBar(homeToolbar)
        //supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_user)


        super.setTitle("")

        supportActionBar?.setDefaultDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)



        try {
            var intent = intent
            var extras = intent.extras

            title = extras.getString("TITLE")


        } catch(e: Exception) {
            Log.d("a",e.message)
            Log.d("a",e.printStackTrace().toString())
        } finally {
        }


        val filter = IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION)
        ConnectionReceiver = ConnectivityReceiver()
        try {
            registerReceiver(ConnectionReceiver, filter)
        } catch(e: Exception) {
            Log.d("reciever",e.message)
        } finally {
        }


        arraylist_templates = ArrayList<TemplatesAdapter>()
        arraylist_templates_reverse = ArrayList<TemplatesAdapter>()


        gridView = findViewById(R.id.image_gridview) as GridView?

        fetchData()

        button_next = findViewById(R.id.button_next) as Button
        button_next?.setOnClickListener {
            var general_intent = Intent(this, GeneralPost::class.java)
            general_intent.putExtra("TITLE",title)
            startActivity(general_intent)
            overridePendingTransition(R.anim.enter_horizontal, R.anim.exit_horizontal)


        }


        gridView?.setOnItemClickListener { parent, view, position, id ->




                BuildUrl.templates_url = arraylist_templates?.get(position)?.url!!

                var general_intent = Intent(this@Templates_Discount_Post, GeneralPost::class.java)
            general_intent.putExtra("TITLE",title)

                startActivity(general_intent)
                overridePendingTransition(R.anim.enter_horizontal, R.anim.exit_horizontal)






        }
        //loadTemplates()





    }


    override fun onResume() {
        super.onResume()


        try {
            var intent = intent
            var extras = intent.extras

            title = extras.getString("TITLE")


        } catch(e: Exception) {
            Log.d("a",e.message)
            Log.d("a",e.printStackTrace().toString())

        } finally {
        }
        ApplicationController.instance?.setConnectivityListener(this)

    }



    override fun onBackPressed() {
        super.onBackPressed()
        overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left)

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.getItemId()

        if (id == android.R.id.home) {
            onBackPressed()
            overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left)
            finish()
            return true
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onNetworkConnectionChanged(isConnected: Boolean) {

        isInternetConnected = ConnectivityReceiver.isConnected
        if (isConnected) {
            //implement here for connected services

        } else if (!isConnected) {
            //implement here for no network services

        }
    }


    private inner class GridViewAdapter:  BaseAdapter()
    {
        override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {


            var convertView = convertView
            var progressBar: ProgressBar
            var imageview: SquaredImageView
            var layout: RelativeLayout
            var imageLoader: ImageLoader
            if (convertView == null) {
                // if it's not recycled, initialize some attributes

                //convertView = layoutInflater.inflate(R.layout.temp_image_item,parent,false)
                //imageview = convertView!!.findViewById(R.id.temp_image_view) as SquaredImageView

                imageview = SquaredImageView(this@Templates_Discount_Post)
                imageview.setPadding(4, 4, 4, 4);


            }
            else
            {
                imageview = convertView as SquaredImageView
            }

            //Log.d("picasso",""+arraylist_templates?.get(position)?.url)

            var url = arraylist_templates?.get(position)?.url

            try {

                Glide.with(this@Templates_Discount_Post)
                        .load(url).diskCacheStrategy(DiskCacheStrategy.NONE).centerCrop().placeholder(R.drawable.loading_gif)
                        .into(imageview);

        /*        .listener(object: RequestListener<String, GlideDrawable> {
                    override fun onException(e: java.lang.Exception?, model: String?, target: com.bumptech.glide.request.target.Target<GlideDrawable>?, isFirstResource: Boolean): Boolean {
                        progressBar.setVisibility(View.GONE)
                        return false;                    }

                    override fun onResourceReady(resource: GlideDrawable?, model: String?, target: com.bumptech.glide.request.target.Target<GlideDrawable>?, isFromMemoryCache: Boolean, isFirstResource: Boolean): Boolean {
                        progressBar.setVisibility(View.GONE)
                        return false;                    }


                }).diskCacheStrategy(DiskCacheStrategy.ALL).centerCrop().crossFade()*/




            } catch(e: Exception) {
                Log.d("picasso", e.localizedMessage)
                Log.d("picasso", e.printStackTrace().toString())
            } finally {
                Log.d("picasso","final")

            }


            return imageview


        }

        override fun getItem(position: Int): Any? {
            return null

        }

        override fun getItemId(position: Int): Long {
            return 0
        }


        override fun getCount(): Int {
            return arraylist_templates!!.size
        }


    }

    private fun fetchData()
    {
        var request = object : JsonArrayRequest(Method.GET, Constants.URL_TEMPLATES_DISCOUNT_COUPON, null, Response.Listener<JSONArray> {

            response ->

            //alert(response.toString()).show()
            for (i in response.length() - 1 downTo 0) {

                val result = response.getJSONObject(i)

                var id = result.getInt(JsonKeys.variables.KEY_ID)
                var name = result.getString(JsonKeys.variables.KEY_NAME)
                var thumbnail = result.getString(JsonKeys.variables.KEY_THUMBNAIL)

                //Log.d("template",""+id+name+thumbnail)

                arraylist_templates?.add(TemplatesAdapter(id,name,thumbnail))


            }



            Log.d("template","arraylist_templates "+arraylist_templates?.size)

            //arraylist_templates?.reverse()






            val DataAdapter = GridViewAdapter()


            gridView?.adapter = DataAdapter
            DataAdapter.notifyDataSetChanged()






        },
                Response.ErrorListener {
                    error ->

                    Log.d("template",""+error.message)


                }){  @Throws(AuthFailureError::class)
        override fun getHeaders(): Map<String, String> {
            return Constants.getAuthHeader()
        }}

        ApplicationController.instance?.addToRequestQueue(request)



    }
    /*private fun loadTemplates()
    {

        for (i in 0..arraylist_fetch_data!!.size - 1)
        {
            arraylist_templates?.add(TemplatesAdapter(arraylist_fetch_data?.get(i)?.id,arraylist_fetch_data?.get(i)?.name,arraylist_fetch_data?.get(i)?.url))
        }

        Log.d("template",""+arraylist_templates?.size)



    }
*/

    fun loadTemplates()
    {
        for(i in arraylist_templates!!.size -1 ..0)
        {
            arraylist_templates_reverse?.add(TemplatesAdapter(arraylist_templates?.get(i)?.id,arraylist_templates?.get(i)?.name,arraylist_templates?.get(i)?.url))

        }
    }


}
