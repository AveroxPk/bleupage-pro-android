package com.example.sarimahmed.bleupage.HelperClasses

/**
 * Created by Averox on 6/13/2017.
 */

object  BuildUrl {


    private var loginUrl: String? = null
    private var socialaccountsUrl: String? = null
    private var fbpagesUrl: String? = null
    private var fbgroupsUrl: String? = null
    private var licompaniesUrl: String? = null
    private var gpciclesUrl: String? = null
    private var gpcollectionsUrl: String? = null
    private var generalPostUrl: String? = null
    private var fbaccpagesUrl: String? = null
    public var templates_url: String = ""
    private var AllAccountsUrl: String? = null
    private var autoPostConfigUrl: String? = null
    private var verify_url: String? = null
    private var fetchQueuesUrl: String? = null
    private var pauseQueueUrl: String? = null
    private var activateQueueUrl: String? = null
    private var deleteQueueUrl: String? = null


    fun setLoginUrl(username: String?, password: String?) {
        loginUrl = Constants.URL_BASE + "login?user=" + username + "&pass=" + password
    }

    fun getLoginUrl(): String? {
        return loginUrl
    }

    fun setSocialAccountsUrl(user_id: Int?) {
        socialaccountsUrl = Constants.URL_BASE + "socialaccounts?user_id=" + user_id
    }

    fun getSocialAccountsUrl(): String? {
        return socialaccountsUrl
    }

    fun setfbaccpagesUrl(user_id: Int?) {
        fbaccpagesUrl = Constants.URL_BASE + "fbaccpages?user_id=" + user_id
    }

    fun getfbaccpagesUrl(): String? {
        return fbaccpagesUrl
    }

    fun setFbPagesUrl(id: Int?) {

        fbpagesUrl = Constants.URL_BASE + "fbpages?fb_user_id=" + id
    }

    fun getFbPagesUrl(): String? {
        return fbpagesUrl

    }

    fun setFbGroupsUrl(id: Int?) {

        fbgroupsUrl = Constants.URL_BASE + "fbgroups?fb_user_id=" + id
    }

    fun getFbGroupsUrl(): String? {
        return fbgroupsUrl

    }

    fun setLiCompaniesUrl(id: Int?) {
        licompaniesUrl = Constants.URL_BASE + "licompanies?fb_user_id=" + id
    }

    fun getLiCompaniesUrl(): String? {
        return licompaniesUrl

    }

    fun setGpCirlcesUrl(id: Int?) {
        gpciclesUrl = Constants.URL_BASE + "getgpcircles?id=" + id
    }

    fun getGpCirlcesUrl(): String? {
        return gpciclesUrl

    }

    fun setGpCollectionsUrl(id: Int?) {
        gpcollectionsUrl = Constants.URL_BASE + "getgpcollections?id=" + id
    }

    fun getGpCollectionsUrl(): String? {
        return gpcollectionsUrl

    }

    fun setAllAccountsUrl(user_id: Int?) {
        AllAccountsUrl = Constants.URL_BASE + "fbaccpages?user_id=" + user_id
    }

    fun getAllAccountsUrl(): String? {
        return AllAccountsUrl

    }

    fun setFetchAutoPostConfigUrl(type: String?, id: Int?) {
        autoPostConfigUrl = "https://bleupage.online/api/getfeedlyconfig?type=" + type + "&fb_id=" + id
    }

    fun getFetchAutoPostConfigUrl(): String? {
        return autoPostConfigUrl
    }

    fun setVerifyUrl(url: String) {
        verify_url = Constants.URL_BASE + "checkurl?url=" + url
    }

    fun getVerifyUrl(): String? {
        return verify_url
    }

    fun setFetchQueuesUrl(user_id: Int?)
    {
        fetchQueuesUrl = Constants.URL_BASE+ "allqueues?user_id=" + user_id
    }
    fun getFetchQueuesUrl(): String?
    {
        return fetchQueuesUrl
    }

    fun getPauseQueueUrl(queue_id: Int): String?
    {
        pauseQueueUrl = Constants.URL_BASE+"pausequeue?queue_id="+queue_id


        return pauseQueueUrl
    }
    fun getActivateQueueUrl(queue_id: Int): String?
    {
        activateQueueUrl = Constants.URL_BASE+"activiatequeue?queue_id="+queue_id


        return activateQueueUrl
    }
    fun getDeleteQueueUrl(queue_id: Int): String?
    {
        deleteQueueUrl = Constants.URL_BASE+"deletequeue?queue_id="+queue_id


        return deleteQueueUrl
    }



}
