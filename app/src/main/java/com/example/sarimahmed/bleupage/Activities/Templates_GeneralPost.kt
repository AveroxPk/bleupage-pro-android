package com.example.sarimahmed.bleupage.Activities

import android.content.IntentFilter
import android.net.ConnectivityManager
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.android.volley.AuthFailureError
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Response
import com.android.volley.toolbox.ImageLoader
import com.android.volley.toolbox.JsonArrayRequest
import com.example.sarimahmed.bleupage.Adapter.TemplatesAdapter
import com.example.sarimahmed.bleupage.Application.ApplicationController
import com.example.sarimahmed.bleupage.BroadcastReceivers.ConnectivityReceiver
import com.example.sarimahmed.bleupage.HelperClasses.Constants
import com.example.sarimahmed.bleupage.HelperClasses.JsonKeys

import com.example.sarimahmed.bleupage.R
import org.json.JSONArray
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.load.resource.drawable.GlideDrawable
import com.bumptech.glide.request.RequestListener
import com.example.sarimahmed.bleupage.HelperClasses.BuildUrl
import com.example.sarimahmed.bleupage.HelperClasses.SquaredImageView
import java.util.*



class Templates_GeneralPost : AppCompatActivity() , ConnectivityReceiver.ConnectivityReceiverListener{

    private var homeToolbar: Toolbar? = null
    internal var isInternetConnected = ConnectivityReceiver.isConnected
    private var ConnectionReceiver: ConnectivityReceiver? = null
    private var arraylist_templates: ArrayList<TemplatesAdapter>? = null
    private var arraylist_templates_reverse: ArrayList<TemplatesAdapter>? = null
    private var gridView: GridView? = null
    private var imageview: ImageView? = null



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_templates__general_post)

        homeToolbar = findViewById(R.id.my_toolbar) as Toolbar
        setSupportActionBar(homeToolbar)
        //supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_user)


        super.setTitle("")

        supportActionBar?.setDefaultDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)




        val filter = IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION)
        ConnectionReceiver = ConnectivityReceiver()
        try {
            registerReceiver(ConnectionReceiver, filter)
        } catch(e: Exception) {
            Log.d("reciever",e.message)
        } finally {
        }






        arraylist_templates = ArrayList<TemplatesAdapter>()
        arraylist_templates_reverse = ArrayList<TemplatesAdapter>()


        gridView = findViewById(R.id.image_gridview) as GridView?

        fetchData()

        gridView?.setOnItemClickListener { parent, view, position, id ->

            BuildUrl.templates_url = arraylist_templates?.get(position)?.url!!


            onBackPressed()
            overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left)


        }
        //loadTemplates()





    }


    override fun onResume() {
        super.onResume()

        ApplicationController.instance?.setConnectivityListener(this)

    }


    override fun onPause() {
        super.onPause()
        unregisterReceiver(ConnectionReceiver)

    }

    override fun onBackPressed() {
        super.onBackPressed()
        overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left)

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.getItemId()

        if (id == android.R.id.home) {
            onBackPressed()
            overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left)
            finish()
            return true
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onNetworkConnectionChanged(isConnected: Boolean) {

        isInternetConnected = ConnectivityReceiver.isConnected
        if (isConnected) {
            //implement here for connected services

        } else if (!isConnected) {
            //implement here for no network services

        }
    }


    private inner class GridViewAdapter:  BaseAdapter()
    {
        override fun getView(position: Int, convertView: View?, parent: ViewGroup?): View {


            var convertView = convertView
            var progressBar: ProgressBar
            var imageview: SquaredImageView
            var layout: RelativeLayout
            var imageLoader: ImageLoader
            if (convertView == null) {
                // if it's not recycled, initialize some attributes

                //convertView = layoutInflater.inflate(R.layout.temp_image_item,parent,false)
                //imageview = convertView!!.findViewById(R.id.temp_image_view) as SquaredImageView

                imageview = SquaredImageView(this@Templates_GeneralPost)
                imageview.setPadding(4, 4, 4, 4);


            }
            else
            {
                imageview = convertView as SquaredImageView
            }

            //imageview = convertView!!.findViewById(R.id.temp_image_view) as SquaredImageView

            //progressBar =  convertView!!.findViewById(R.id.progressBar_image) as ProgressBar

                //Log.d("picasso",""+arraylist_templates?.get(position)?.url)

                var url = arraylist_templates?.get(position)?.url

                try {


                    Glide.with(this@Templates_GeneralPost)
                            .load(url).diskCacheStrategy(DiskCacheStrategy.NONE).centerCrop().placeholder(R.drawable.loading_gif)
                            .into(imageview)

                    //your image url


//initialize image view

//download and display image from url
/*
                .listener(object:  RequestListener<String, GlideDrawable>  {
                    override fun onException(e: java.lang.Exception?, model: String?, target: com.bumptech.glide.request.target.Target<GlideDrawable>?, isFirstResource: Boolean): Boolean {
                        progressBar.setVisibility(View.GONE);
                        return false;                    }

                    override fun onResourceReady(resource: GlideDrawable?, model: String?, target: com.bumptech.glide.request.target.Target<GlideDrawable>?, isFromMemoryCache: Boolean, isFirstResource: Boolean): Boolean {
                        progressBar.setVisibility(View.GONE);
                        return false;                    }


                }).*/


                } catch(e: Exception) {
                    Log.d("picasso", e.localizedMessage)
                    Log.d("picasso", e.printStackTrace().toString())
                } finally {
                    Log.d("picasso", "final")

                }



            return imageview

        }

        override fun getItem(position: Int): Any? {
            return null

        }

        override fun getItemId(position: Int): Long {
            return 0
        }


        override fun getCount(): Int {
            return arraylist_templates!!.size
        }


    }

    private fun fetchData()
    {

        
        var request = object : JsonArrayRequest(Method.GET, Constants.URL_TEMPLATES_GENERAL_POST, null, Response.Listener<JSONArray> {

            response ->


            Log.d("temp","response received: "+response.toString())

            //alert(response.toString()).show()
            for (i in response.length() - 1 downTo 0) {

                val result = response.getJSONObject(i)

                var id = result.getInt(JsonKeys.variables.KEY_ID)
                var name = result.getString(JsonKeys.variables.KEY_NAME)
                var thumbnail = result.getString(JsonKeys.variables.KEY_THUMBNAIL)

                //Log.d("template",""+id+name+thumbnail)

                arraylist_templates?.add(TemplatesAdapter(id,name,thumbnail))


            }



            Log.d("template","arraylist_templates "+arraylist_templates?.size)

            //arraylist_templates?.reverse()






            val DataAdapter = GridViewAdapter()


            gridView?.adapter = DataAdapter
            DataAdapter.notifyDataSetChanged()






        },
                Response.ErrorListener {
                    error ->

                    Log.d("template",""+error.message)


                }){  @Throws(AuthFailureError::class)
        override fun getHeaders(): Map<String, String> {
            return Constants.getAuthHeader()
        }}

        request.setRetryPolicy(DefaultRetryPolicy(
                30000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT))


        ApplicationController.instance?.addToRequestQueue(request)



    }
    /*private fun loadTemplates()
    {

        for (i in 0..arraylist_fetch_data!!.size - 1)
        {
            arraylist_templates?.add(TemplatesAdapter(arraylist_fetch_data?.get(i)?.id,arraylist_fetch_data?.get(i)?.name,arraylist_fetch_data?.get(i)?.url))
        }

        Log.d("template",""+arraylist_templates?.size)



    }
*/

    fun loadTemplates()
    {
        for(i in arraylist_templates!!.size -1 ..0)
        {
            arraylist_templates_reverse?.add(TemplatesAdapter(arraylist_templates?.get(i)?.id,arraylist_templates?.get(i)?.name,arraylist_templates?.get(i)?.url))

        }
    }


}
