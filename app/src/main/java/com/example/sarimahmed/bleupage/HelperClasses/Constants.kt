package com.example.sarimahmed.bleupage.HelperClasses

import android.content.Context

import com.android.volley.RequestQueue
import com.example.sarimahmed.bleupage.Application.ApplicationController



/**
 * Created by Averox on 6/13/2017.
 */

object Constants {


    val URL_BASE = "https://bleupage.online/api/"
    val URL_GENERAL_POST = "https://www.bleupage.online/api/postpublish"
    val URL_AUTO_POST ="https://www.bleupage.online/api/feedlypostconfig"
    val URL_QUEUE_SAVE ="https://www.bleupage.online/api/savequeues"
    val URL_ADD_POST_TO_QUEUE = "https://www.bleupage.online/api/addposttoqueue"
    val URL_ADD_URL_TO_QUEUE = "https://www.bleupage.online/api/addurltoqueue"


    var context: Context? = null
    const val AUTHORIZATION_HEADER = "14abd57ece42d9489aeae6e186506475"
    const val VAR_AUTOHRIZATION = "Authorization"
    const val URL_TEMPLATES_GENERAL_POST = "https://bleupage.online/api/statictemplates?type=tw_general"
    const val URL_TEMPLATES_DISCOUNT_COUPON = "https://bleupage.online/api/statictemplates?type=tw_coupon"
    const val URL_CATEGORIES = "https://bleupage.online/api/feedlygetcats"


    fun getAuthHeader(): HashMap<String, String>
    {
        val params = HashMap<String, String>()
        params.put(VAR_AUTOHRIZATION, AUTHORIZATION_HEADER)


        return params
    }

   /* var volleyController: ApplicationController? = null
    var requestQueue: RequestQueue? = null*/


}
