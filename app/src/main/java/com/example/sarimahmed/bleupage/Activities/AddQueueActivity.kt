package com.example.sarimahmed.bleupage.Activities

import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.ViewGroup
import android.widget.*

import com.example.sarimahmed.bleupage.R
import org.jetbrains.anko.toast
import android.widget.ArrayAdapter
import android.view.ViewGroup.LayoutParams.FILL_PARENT
import android.widget.Spinner
import android.widget.LinearLayout
import android.widget.AdapterView
import android.widget.TextView
import java.util.*


class AddQueueActivity : AppCompatActivity(),CompoundButton.OnCheckedChangeListener,AdapterView.OnItemSelectedListener,View.OnClickListener {


    private var times_spinner: Spinner? = null
    private var times_spinner_items: Array<String>? = null

    private var toggle_monday: ToggleButton? = null
    private var toggle_tuesday: ToggleButton? = null
    private var toggle_wednesday: ToggleButton? = null
    private var toggle_thursday: ToggleButton? = null
    private var toggle_friday: ToggleButton? = null
    private var toggle_saturday: ToggleButton? = null
    private var toggle_sunday: ToggleButton? = null
    private var linear_layout_times: LinearLayout? = null
    private var button_close: Button? = null
    private var button_next: Button? = null
    private var localtime_arraylist: ArrayList<String>? = null
    private var time_arraylist: ArrayList<String>? = null
    private var input_queue_name: TextView? = null
    private var days_arraylist: ArrayList<Int>? = null
    private var request: String = "new"


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_queue)

        times_spinner = findViewById(R.id.times_spinner) as Spinner?
        times_spinner_items = getResources().getStringArray(R.array.content_fetcher_times_array)
        times_spinner?.setOnItemSelectedListener(this)

        var arrayAdapter1 = ArrayAdapter(this@AddQueueActivity,R.layout.support_simple_spinner_dropdown_item,times_spinner_items)



        toggle_monday = findViewById(R.id.toggleButton_monday) as ToggleButton?
        toggle_tuesday = findViewById(R.id.toggleButton_tuesday) as ToggleButton?
        toggle_wednesday = findViewById(R.id.toggleButton_wednesday) as ToggleButton?
        toggle_thursday = findViewById(R.id.toggleButton_thursday) as ToggleButton?
        toggle_friday = findViewById(R.id.toggleButton_friday) as ToggleButton?
        toggle_saturday = findViewById(R.id.toggleButton_saturday) as ToggleButton?
        toggle_sunday = findViewById(R.id.toggleButton_sunday) as ToggleButton?
        toggle_monday?.setOnCheckedChangeListener(this)
        toggle_tuesday?.setOnCheckedChangeListener(this)
        toggle_wednesday?.setOnCheckedChangeListener(this)
        toggle_thursday?.setOnCheckedChangeListener(this)
        toggle_friday?.setOnCheckedChangeListener(this)
        toggle_saturday?.setOnCheckedChangeListener(this)
        toggle_sunday?.setOnCheckedChangeListener(this)

        button_close = findViewById(R.id.button_close) as Button?
        button_close?.setOnClickListener(this)


        days_arraylist = ArrayList()


        input_queue_name = findViewById(R.id.input_queue_name) as TextView?
        button_next = findViewById(R.id.button_next) as Button?
        button_next?.setOnClickListener(this)




        times_spinner?.setAdapter(arrayAdapter1)

        times_spinner?.setSelection(0, true);
        var v = times_spinner?.getSelectedView()
        (v as TextView).setTextColor(resources.getColor(R.color.smokeWhite))

        linear_layout_times = findViewById(R.id.linear_layout_times) as LinearLayout?


        var intent = intent
        var extras = intent.extras



        try
        {
            if(extras.getString("TITLE").equals("edit_queue"))
            {
                input_queue_name?.setText(extras.getString("QUEUE_NAME"))
                var times = extras.getStringArrayList("LOCALTIME")
                for(i in 0..extras.getIntegerArrayList("DAYS").size - 1)
                {


                    var item = extras.getIntegerArrayList("DAYS").get(i)
                    if(item == 1)
                    {
                            toggle_monday?.isChecked = true
                    }
                    else if(item == 2)
                    {
                        toggle_tuesday?.isChecked = true

                    }
                    else if(item == 3)
                    {
                        toggle_wednesday?.isChecked = true

                    }else if(item == 4)
                    {
                        toggle_thursday?.isChecked = true

                    }
                    else if(item == 5)
                    {
                        toggle_friday?.isChecked = true

                    }
                    else if(item == 6)
                    {
                        toggle_saturday?.isChecked = true


                    }
                    else if(item == 7)
                    {
                        toggle_sunday?.isChecked = true

                    }



                }
                times_spinner?.setSelection(times.size)

            }
        }
        catch (e: Exception)
        {

            Log.d("queue_days",""+e.message)
        }
        finally
        {
        }

    }

    override fun onBackPressed() {

        super.onBackPressed()
        overridePendingTransition(R.anim.top_to_bottom, R.anim.bottom_to_top)

    }

    override fun onCheckedChanged(v: CompoundButton?, isChecked: Boolean) {


        var id = v?.id

        when(id)
        {
            R.id.toggleButton_monday ->
            {
                if(isChecked)
                {
                    toggle_monday?.setBackgroundColor(resources.getColor(R.color.darkred))

                    days_arraylist?.add(1)

                }
                else if(!isChecked)
                {
                    toggle_monday?.setBackgroundColor(resources.getColor(R.color.transparent))

                    toggle_monday?.setBackground(resources.getDrawable(R.drawable.border))

                    if(days_arraylist?.contains(1) == true)
                    {
                        days_arraylist?.remove(1)


                    }
                }
            }
            R.id.toggleButton_tuesday ->
            {
                if(isChecked)
                {
                    toggle_tuesday?.setBackgroundColor(resources.getColor(R.color.darkred))
                    days_arraylist?.add(2)

                }
                else if(!isChecked)
                {
                    toggle_tuesday?.setBackgroundColor(resources.getColor(R.color.transparent))

                    toggle_tuesday?.setBackground(resources.getDrawable(R.drawable.border))
                    if(days_arraylist?.contains(2) == true)
                    {
                        days_arraylist?.remove(2)


                    }
                }
            }
            R.id.toggleButton_wednesday ->
            {
                if(isChecked)
                {
                    toggle_wednesday?.setBackgroundColor(resources.getColor(R.color.darkred))
                    days_arraylist?.add(3)

                }
                else if(!isChecked)
                {
                    toggle_wednesday?.setBackgroundColor(resources.getColor(R.color.transparent))

                    toggle_wednesday?.setBackground(resources.getDrawable(R.drawable.border))
                    if(days_arraylist?.contains(3) == true)
                    {
                        days_arraylist?.remove(3)


                    }
                }
            }
            R.id.toggleButton_thursday ->
            {
                if(isChecked)
                {
                    toggle_thursday?.setBackgroundColor(resources.getColor(R.color.darkred))
                    days_arraylist?.add(4)

                }
                else if(!isChecked)
                {
                    toggle_thursday?.setBackgroundColor(resources.getColor(R.color.transparent))

                    toggle_thursday?.setBackground(resources.getDrawable(R.drawable.border))
                    if(days_arraylist?.contains(4) == true)
                    {
                        days_arraylist?.remove(4)


                    }
                }
            }
            R.id.toggleButton_friday ->
            {
                if(isChecked)
                {
                    toggle_friday?.setBackgroundColor(resources.getColor(R.color.darkred))
                    days_arraylist?.add(5)

                }
                else if(!isChecked)
                {
                    toggle_friday?.setBackgroundColor(resources.getColor(R.color.transparent))

                    toggle_friday?.setBackground(resources.getDrawable(R.drawable.border))

                    if(days_arraylist?.contains(5) == true)
                    {
                        days_arraylist?.remove(5)


                    }
                }
            }
            R.id.toggleButton_saturday ->
            {
                if(isChecked)
                {
                    toggle_saturday?.setBackgroundColor(resources.getColor(R.color.darkred))
                    days_arraylist?.add(6)

                }
                else if(!isChecked)
                {
                    toggle_saturday?.setBackgroundColor(resources.getColor(R.color.transparent))

                    toggle_saturday?.setBackground(resources.getDrawable(R.drawable.border))
                    if(days_arraylist?.contains(6) == true)
                    {
                        days_arraylist?.remove(6)


                    }
                }
            }
            R.id.toggleButton_sunday ->
            {
                if(isChecked)
                {
                    toggle_sunday?.setBackgroundColor(resources.getColor(R.color.darkred))
                    days_arraylist?.add(7)

                }
                else if(!isChecked)
                {
                    toggle_sunday?.setBackgroundColor(resources.getColor(R.color.transparent))

                    toggle_sunday?.setBackground(resources.getDrawable(R.drawable.border))

                    if(days_arraylist?.contains(7) == true)
                    {
                        days_arraylist?.remove(7)


                    }
                }
            }

        }
    }

    override fun onNothingSelected(p0: AdapterView<*>?) {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }

    override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
        when (parent?.getId())
        {
            R.id.times_spinner ->
            {

                (view as TextView).setTextColor(resources.getColor(R.color.smokeWhite))


                linear_layout_times?.removeAllViews()


                var count = 0

                localtime_arraylist = ArrayList()
                time_arraylist = ArrayList()

                var intent = intent
                var extras = intent.extras
                var time = ArrayList<Int>()


                try {
                    if(extras.getString("TITLE").equals("edit_queue")) {

                        for(j in 0..extras.getStringArrayList("LOCALTIME").size - 1) {

                            var item = extras.getStringArrayList("LOCALTIME").get(j)
                            if (item.length == 4) {

                                time.add(item.substring(0,1).toInt())
                                Log.d("response", "time: " + item)

                                Log.d("response", "time: " + "in if")

                            } else {

                                time.add(item.substring(0,2).toInt())

                                Log.d("response", "time: " + "in else")

                            }
                        }





                    }
                } catch (e: Exception) {
                } finally {
                }










                for(i in 0..position - 1)
                {



                    var spinner = Spinner(this)
                    //Make sure you have valid layout parameters.

                    var strs = resources.getStringArray(R.array.array_time_24hr)

                    spinner.layoutParams = ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT)

                    spinner.id = count


                    var spinnerArrayAdapter = ArrayAdapter(this, R.layout.support_simple_spinner_dropdown_item, strs)

                    spinner.adapter = spinnerArrayAdapter




                    linear_layout_times?.addView(spinner)

                    spinner.setSelection(0,true)

                    try {
                        if(extras.getString("TITLE").equals("edit_queue"))
                        {


                            spinner.setSelection(time.get(spinner.id),true)



                        }
                    } catch (e: Exception) {
                    } finally {
                    }


                    spinner.backgroundTintList = resources.getColorStateList(R.color.smokeWhite)

                    localtime_arraylist!!.add(i,spinner.selectedItem as String)

                    time_arraylist!!.add(i,getServerTime(spinner.selectedItemPosition))




                    var v = spinner?.getSelectedView()
                    (v as TextView).setTextColor(resources.getColor(R.color.smokeWhite))

                    count++

                    spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {

                        override fun onItemSelected(parent: AdapterView<*>, view: View, position: Int, id: Long) {
                            //Change the selected item's text color
                            (view as TextView).setTextColor(resources.getColor(R.color.smokeWhite))
                            localtime_arraylist!![spinner.id] = spinner.selectedItem as String

                            localtime_arraylist!!.set(spinner.id,spinner.selectedItem as String)
                            time_arraylist!!.set(spinner.id,getServerTime(spinner.selectedItemPosition))



                        }
                        override fun onNothingSelected(parent: AdapterView<*>) {}
                    }
                }
            }
        }
    }

    override fun onClick(v: View?) {

        when(v?.id)
        {
            R.id.button_close ->
            {

                onBackPressed()

            }
            R.id.button_next ->
            {

                var i = intent
                var extras = i.extras
                var name = input_queue_name?.text.toString().trim()
                if(name.equals("") || name.equals(null)  )
                {
                    input_queue_name?.setError("Please enter a queue name")
                }
                else
                {


                    var intent = Intent(this@AddQueueActivity,PlatformSelection::class.java)
                    intent.putExtra("POST_TYPE","queue")
                    intent.putExtra("QUEUE_NAME",name)
                    intent.putExtra("REQUEST",request)
                    intent.putExtra("DAYS",days_arraylist)
                    intent.putExtra("LOCALTIME",localtime_arraylist)
                    intent.putExtra("TIME",time_arraylist)

                    try {
                        if(extras.getString("TITLE").equals("edit_queue")) {


                            intent.putExtra("QUEUE_ACCOUNTS",extras.getStringArrayList("QUEUE_ACCOUNTS"))
                            intent.putExtra("TITLE",extras.getString("TITLE"))
                            intent.putExtra("QUEUE_ID",extras.getInt("QUEUE_ID"))


                        }
                    } catch (e: Exception) {
                    } finally {
                    }


                    startActivity(intent)
                    overridePendingTransition(R.anim.enter_horizontal,R.anim.exit_horizontal)

                }


            }


        }
    }


    fun getServerTime(time: Int): String
    {
        var serverTime: String

        var serverTimeZone = TimeZone.getTimeZone("US/Arizona")

        val TimeZoneOffset = serverTimeZone.getRawOffset() / (60 * 1000)
        var serverhrs = TimeZoneOffset / 60


        if(serverhrs < 0)
        {
            serverhrs =  serverhrs * (-1)
        }

        Log.d("localtimezone",""+serverhrs)


        var localTimeZone = TimeZone.getDefault()

        val TimeZoneOffset1 = localTimeZone.getRawOffset() / (60 * 1000)
        val localhrs1 = TimeZoneOffset1 / 60

        Log.d("localtimezone",""+localhrs1)


        var result = serverhrs + localhrs1

        if(result <0)
        {
            var finalresult = time + result
            if(finalresult < 0)
            {
                finalresult = finalresult * (-1)
            }
            serverTime = ""+finalresult
            return serverTime!!+":00"
        }
        else
        {
            var finalresult = time.minus(result)
            if(finalresult < 0)
            {
                finalresult = finalresult * (-1)
            }
            serverTime = ""+finalresult

            return serverTime!!+":00"

        }
    }


}
