package com.example.sarimahmed.bleupage.Fragments

import android.app.Fragment
import android.app.ProgressDialog
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.android.volley.AuthFailureError
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Response
import com.android.volley.toolbox.JsonArrayRequest
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.StringRequest
import com.example.sarimahmed.bleupage.Adapter.BlogFeedAdapter
import com.example.sarimahmed.bleupage.Adapter.BlogFeedAdapter.Companion.contentList
import com.example.sarimahmed.bleupage.Adapter.QueueAdapter
import com.example.sarimahmed.bleupage.Application.ApplicationController
import com.example.sarimahmed.bleupage.HelperClasses.AlertManager
import com.example.sarimahmed.bleupage.HelperClasses.BuildUrl
import com.example.sarimahmed.bleupage.HelperClasses.Constants
import com.example.sarimahmed.bleupage.HelperClasses.JsonKeys
import com.example.sarimahmed.bleupage.R
import com.example.sarimahmed.bleupage.SessionManager.LoginManager
import org.jetbrains.anko.toast
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject

/**
 * Created by Averox on 10/4/2017.
 */
class AddUrlToQueue : Fragment(),View.OnClickListener
{


    private var spinner_queue: Spinner? = null
    private var button_add_url_to_queue: Button? = null
    private var progressDialog: ProgressDialog? = null
    private var page_title: String? = null
    private var page_link: String? = null
    private var page_description: String? = null
    private var queue_id: Int? = null



    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View {
        var view = inflater?.inflate(R.layout.layout_addurltoqueue, container, false)


        spinner_queue = view?.findViewById<Spinner>(R.id.queue_spinner)

        /*var v = spinner_queue?.getSelectedView()
        (v as TextView).setTextColor(resources.getColor(R.color.smokeWhite))*/
        button_add_url_to_queue = view?.findViewById(R.id.button_add_url_to_queue)

        try {
            if (getArguments() != null) {
                page_title = getArguments().getString("PAGE_TITLE")
                page_link = getArguments().getString("PAGE_LINK")
                page_description = getArguments().getString("DESCRIPTION")

                Log.d("frag",page_title+" "+page_link+" "+page_description)
            }
        } catch (e: Exception) {
        } finally {
        }


        return view!!
    }



    override fun onActivityCreated(savedInstanceState: Bundle?) {
        button_add_url_to_queue?.setOnClickListener(this)

        spinner_queue?.onItemSelectedListener = object: AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(p0: AdapterView<*>?) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {
                var v = spinner_queue?.getSelectedView()
                (v as TextView).setTextColor(resources.getColor(R.color.smokeWhite))

                try {
                    queue_id = QueueAdapter.queueList?.get(spinner_queue?.selectedItemPosition?.minus(1) as Int)?.queue_id
                } catch (e: Exception) {
                } finally {
                }
            }
        }

        loadQueues()
        super.onActivityCreated(savedInstanceState)
    }

    override fun onClick(v: View?) {
        when (v?.id) {
            R.id.button_add_url_to_queue -> {
                if (spinner_queue?.selectedItemPosition != 0) {
                    try {

                        progressDialog = ProgressDialog.show(activity, "Saving Configuration", "Please wait....", true)
                        progressDialog?.setCancelable(false)

                        var request = object : JsonObjectRequest(Method.POST, Constants.URL_ADD_URL_TO_QUEUE, null, Response.Listener<JSONObject> {

                            response ->
                            try {
                                Log.d("response", "response:  " + response.toString())

                                var status = response.getString(JsonKeys.variables.KEY_STATUS)
                                Log.d("response",""+status)

                                if(status.equals("true"))
                                {
                                    progressDialog?.dismiss()


                                        var alert = AlertManager("Url added Successfully",5000,activity)



                                }
                                else
                                {
                                    progressDialog?.dismiss()

                                    var alert = AlertManager("Problem adding Url to Queue!",5000,activity)
                                }


                                //Log.d("responseaccounts",""+queues_arraylist?.get(1).toString())
                                //listview_queues?.adapter = QueuesListAdapter
                                //QueuesListAdapter.notifyDataSetChanged()


                            } catch (e: Exception) {
                                Log.d("response", "" + e.message)
                                Log.d("response", "" + e.printStackTrace().toString())
                                progressDialog?.dismiss()

                            } finally {

                            }


                        },
                                Response.ErrorListener {

                                    error ->
                                    Log.d("response", "error : " + error.message.toString())
                                    Log.d("response", "error : " + error.printStackTrace().toString())

                                    //progressDialog?.dismiss()


                                }) {


                            override fun getBody(): ByteArray {

                                var jsonObject = JSONObject()

                                var content = JSONArray()

                                var body: String? = null
                                Log.d("queue", "In get Body")

                                try {


                                    jsonObject.put("description", page_description)
                                    jsonObject.put("link", page_link)


                                    jsonObject.put("title", page_title)
                                    jsonObject.put("queueid", queue_id)

                                    for (i in 0..BlogFeedAdapter?.contentList!!.size - 1) {
                                        var temp = JSONObject()
                                        temp.put("description", "" + BlogFeedAdapter?.contentList?.get(i)?.description)
                                        temp.put("link", "" + BlogFeedAdapter?.contentList?.get(i)?.link)
                                        temp.put("title", "" + BlogFeedAdapter?.contentList?.get(i)?.title)
                                        temp.put("content", "" + BlogFeedAdapter?.contentList?.get(i)?.content)


                                        content.put(temp)

                                    }

                                    jsonObject.put("content", content)

                                    body = jsonObject.toString()

                                    Log.d("queue", "json object string: " + jsonObject.toString())
                                    //Log.d("autpost", "json object bytearray: " + jsonObject.toString().toByteArray())

                                } catch (e: JSONException) {

                                    Log.d("queue", e.message)
                                    var alert = AlertManager("Network problem!", 5000, activity)


                                }



                                return jsonObject.toString().toByteArray()
                            }

                            @Throws(AuthFailureError::class)
                            override fun getHeaders(): Map<String, String> {
                                return Constants.getAuthHeader()
                            }
                        }

                        request.setRetryPolicy(DefaultRetryPolicy(
                                30000,
                                0,
                                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT))

                        ApplicationController.instance?.addToRequestQueue(request)
                    } catch (e: Exception) {
                    } finally {
                    }

                }
            }


        }
    }


    fun loadQueues() {


        var temp_array = ArrayList<String>()

        temp_array.add("No Queue Selected")

        var spinnerArrayAdapter = ArrayAdapter(activity, R.layout.support_simple_spinner_dropdown_item, temp_array)

        spinner_queue?.adapter = spinnerArrayAdapter

        spinner_queue?.setSelection(0, true)

        var v = spinner_queue?.getSelectedView()
        (v as TextView).setTextColor(resources.getColor(R.color.smokeWhite))

                    for(i in 0..QueueAdapter?.queueList!!.size - 1) {
                        temp_array.add(QueueAdapter.queueList?.get(i)?.queue_name!!)
                    }


                    spinnerArrayAdapter = ArrayAdapter(activity, R.layout.support_simple_spinner_dropdown_item, temp_array)

                    spinner_queue?.adapter = spinnerArrayAdapter

                    spinner_queue?.setSelection(0, true)


                    (v as TextView).setTextColor(resources.getColor(R.color.smokeWhite))


    }


    }


