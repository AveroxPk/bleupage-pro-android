package com.example.sarimahmed.bleupage.Activities

import android.app.ProgressDialog
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.ContextMenu
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.widget.*
import com.android.volley.AuthFailureError
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Response
import com.android.volley.toolbox.JsonArrayRequest
import com.android.volley.toolbox.JsonObjectRequest
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.animation.GlideAnimation
import com.bumptech.glide.request.target.SimpleTarget
import com.example.sarimahmed.bleupage.Adapter.AccountsDataAdapter
import com.example.sarimahmed.bleupage.Application.ApplicationController
import com.example.sarimahmed.bleupage.HelperClasses.*

import com.example.sarimahmed.bleupage.R
import com.example.sarimahmed.bleupage.SessionManager.LoginManager
import com.example.sarimahmed.bleupage.SessionManager.UserDataManager
import org.jetbrains.anko.alert
import org.jetbrains.anko.toast
import org.json.JSONArray
import org.json.JSONObject

class AccountsList : AppCompatActivity() ,AdapterView.OnItemClickListener {



    private var homeToolbar: Toolbar? = null
    private var toolbar_title: TextView? = null
    private var listView_accounts: ListView? = null
    private var accounts_arraylist: ArrayList<AccountsDataAdapter>? = null
    private var imageview_display_picture: RoundedImageView? = null
    private var textview_username: TextView? = null
    private var textview_pages_count: TextView? = null
    private var activity_title: String = ""
    private var progressDialog: ProgressDialog? = null
    private var accountid: Int? = null
    private var type: String = ""
    private var id: Int?= null
    private var status: Boolean? = false
    private var automation: Boolean? = false
    private var ios: Boolean? = false
    private var loginManager: LoginManager? = null




    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_accounts_list)

        homeToolbar = findViewById(R.id.my_toolbar) as Toolbar
        setSupportActionBar(homeToolbar)
        //supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_user)


        super.setTitle("")


        loginManager = LoginManager(this@AccountsList)

        checkPackages()



        toolbar_title = findViewById(R.id.toolbar_title_accounts_list) as TextView?

        var intent = intent
        var extras = intent.extras

        try {
            activity_title = extras.getString("TITLE")
            accountid = extras.getInt("ID")


            toolbar_title?.text = activity_title
        }
        catch (e: Exception)
        {

        }

        supportActionBar?.setDefaultDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)


        listView_accounts = findViewById(R.id.accounts_listview) as ListView?

        //registerForContextMenu(listView_accounts)

        listView_accounts?.setOnItemClickListener(this)

        accounts_arraylist = ArrayList<AccountsDataAdapter>()



        loadData()

    }

    override fun onBackPressed() {

        try {
            super.onBackPressed()
            overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left)
        } catch (e: Exception) {
        } finally {
        }

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.getItemId()

        if (id == android.R.id.home) {
            onBackPressed()
            overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left)
            finish()
            return true
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onItemClick(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

        this.id = accounts_arraylist?.get(position)?.getId()

        registerForContextMenu(listView_accounts)

        listView_accounts?.showContextMenu()

    }

    override fun onCreateContextMenu(menu: ContextMenu?, v: View?, menuInfo: ContextMenu.ContextMenuInfo?) {
        super.onCreateContextMenu(menu, v, menuInfo)

        if(loginManager?.getUserStatus() == true && loginManager?.getAutomationStatus() == true)
        {


            return
        }
        else
        {

            Log.d("abc","automation  purchased in else")
            getMenuInflater().inflate(R.menu.ac_context_menu, menu)


        }
    }

    override fun onContextItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {

            R.id.context_auto_posting ->
            {

                if(activity_title == resources.getString(R.string.string_toolbarTitle_pages))
                {
                    val intent = Intent(this, Categories::class.java)
                    intent.putExtra("FROM","page")
                    intent.putExtra("ID",id)
                    startActivity(intent)
                    overridePendingTransition(R.anim.enter_horizontal, R.anim.exit_horizontal)

                }
                else if(activity_title == resources.getString(R.string.string_toolbarTitle_groups))
                {
                    val intent = Intent(this, Categories::class.java)
                    intent.putExtra("FROM","groups")
                    intent.putExtra("ID",id)

                    startActivity(intent)
                    overridePendingTransition(R.anim.enter_horizontal, R.anim.exit_horizontal)
                }
                else if(activity_title == resources.getString(R.string.string_toolbarTitle_companies))
                {
                    val intent = Intent(this, Categories::class.java)
                    intent.putExtra("FROM","li")
                    intent.putExtra("ID",id)

                    startActivity(intent)
                    overridePendingTransition(R.anim.enter_horizontal, R.anim.exit_horizontal)
                }
                else if(activity_title == resources.getString(R.string.string_toolbarTitle_circles))
                {
                    val intent = Intent(this, Categories::class.java)
                    intent.putExtra("FROM","gpc")
                    intent.putExtra("ID",id)

                    startActivity(intent)
                    overridePendingTransition(R.anim.enter_horizontal, R.anim.exit_horizontal)
                }
                else if(activity_title == resources.getString(R.string.string_toolbarTitle_collections))
                {
                    val intent = Intent(this, Categories::class.java)
                    intent.putExtra("FROM","gpcl")
                    intent.putExtra("ID",id)
                    startActivity(intent)
                    overridePendingTransition(R.anim.enter_horizontal, R.anim.exit_horizontal)
                }

                return true


            }

            else -> return super.onContextItemSelected(item)
        }
    }

    private inner class CustomAdapter : ArrayAdapter<AccountsDataAdapter>(this@AccountsList, R.layout.list_item, accounts_arraylist) {


        override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
            var convertView = convertView

            if (convertView == null) {
                convertView = layoutInflater.inflate(R.layout.list_item, parent, false)

            }

            val AccountsDataView = accounts_arraylist?.get(position)

            imageview_display_picture = convertView!!.findViewById<RoundedImageView>(R.id.imageView_display_picture)
            textview_username = convertView.findViewById<TextView>(R.id.textView_username)
            textview_pages_count = convertView.findViewById<TextView>(R.id.textView_pages_count)

            // imageview_display_picture.setImageURI(AccountsDataView?.getPictureUrl())
            if (activity_title == resources.getString(R.string.string_toolbarTitle_companies)) {
                textview_username?.text = AccountsDataView?.getUsername()

            }
            else if (activity_title == resources.getString(R.string.string_toolbarTitle_groups))
            {
                textview_username?.text = AccountsDataView?.getGroupName()

            }
            else {
                textview_username?.text = AccountsDataView?.getName()

            }

            try {
                Glide.with(this@AccountsList).load(AccountsDataView?.getPictureUrl()).asBitmap().fitCenter().placeholder(R.drawable.avatar_user).diskCacheStrategy(DiskCacheStrategy.ALL).into(object: SimpleTarget<Bitmap>(75,75)
                {
                    override fun onResourceReady(resource: Bitmap?, glideAnimation: GlideAnimation<in Bitmap>?) {

                        imageview_display_picture?.setImageBitmap(resource)
                    }

                    override fun onLoadFailed(e: java.lang.Exception?, errorDrawable: Drawable?) {

                        Log.d("view1",e?.message.toString())
                        super.onLoadFailed(e, errorDrawable)
                    }

                })
            } catch (e: Exception) {
            } finally {
            }

            return convertView
        }
    }

    fun loadData()
    {
        if(activity_title == resources.getString(R.string.string_toolbarTitle_pages))
        {

            try {

                progressDialog = ProgressDialog.show(this@AccountsList, "Accessing Pages", "Please wait....", true)
                progressDialog?.setCancelable(false)
                BuildUrl.setFbPagesUrl(accountid)
                Log.d("url", BuildUrl.getFbPagesUrl())

                var request = object : JsonArrayRequest(Method.GET, BuildUrl.getFbPagesUrl(), null, Response.Listener<JSONArray> {


                    response ->


                    try {
                        accounts_arraylist?.clear()
                        //alert(array_fbaccounts.toString()).show()

                        if(response.length() < 1)
                        {

                            progressDialog?.dismiss()

                            alert("No Facebook Page!").show()

                            Handler().postDelayed(object : Runnable {

                                // Using handler with postDelayed called runnable run method


                                override fun run() {

                                    onBackPressed()

                                }
                            }, 2 * 1000) // wait for 5 seconds
                        }
                        else {
                            for (i in 0..response.length() - 1) {
                                Log.d("url", "in loop")
                                val result = response.getJSONObject(i)
                                Log.d("url", "in loop")
                                val id = result.getInt(JsonKeys.variables.KEY_ID)
                                val fb_id = result.getInt(JsonKeys.variables.KEY_FB_ID)
                                val name = result.getString(JsonKeys.variables.KEY_NAME)
                                val picture = result.getString(JsonKeys.variables.KEY_PICTURE)
                                val access_token = result.getString(JsonKeys.variables.KEY_ACCESS_TOKEN)
                                val posts_count = result.getInt(JsonKeys.variables.KEY_POSTS_COUNT)

                                //(FBAccountsList as ArrayList<AccountsDataAdapter>).add(AccountsDataAdapter(id, fb_id, name, picture, access_token, pages_count))
                                //Log.d("url", "name = " + FBAccountsList?.get(i)?.getUsername())

                                accounts_arraylist?.add(AccountsDataAdapter(id, fb_id, name, picture, access_token, posts_count))

                            }

                            val DataAdapter = CustomAdapter()

                            listView_accounts?.adapter = DataAdapter
                            DataAdapter.notifyDataSetChanged()

                            Log.d("size", "accounts array list size on load default " + accounts_arraylist?.size.toString())

                            type = "pg"

                            progressDialog?.dismiss()

                        }



                    } catch(e: Exception) {

                        Log.d("JsonResponse", ""+e.message)

                        progressDialog?.dismiss()
                        var alert = AlertManager("Network problem!",5000,this@AccountsList)


                    }


                },
                        Response.ErrorListener {

                            error ->

                        Log.d("response", ""+error.message)
                            //Toast.makeText(this, "error", Toast.LENGTH_LONG).show()
                            progressDialog?.dismiss()
                            var alert = AlertManager("Network problem!",5000,this@AccountsList)



                        }
                ) {

                    @Throws(AuthFailureError::class)
                    override fun getHeaders(): Map<String, String> {
                        return Constants.getAuthHeader()
                    }
                }

                request.setRetryPolicy(DefaultRetryPolicy(
                        25000,
                        0,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT))
                ApplicationController.instance?.addToRequestQueue(request)


            } catch (e: Exception) {

                Log.d("oncreate", e.message)

            }

        }
        else if(activity_title == resources.getString(R.string.string_toolbarTitle_groups))
        {


            try {

                progressDialog = ProgressDialog.show(this@AccountsList, "Accessing Groups", "Please wait....", true)
                progressDialog?.setCancelable(false)
                BuildUrl.setFbGroupsUrl(accountid)
                Log.d("url", BuildUrl.getFbGroupsUrl())

                var request = object : JsonArrayRequest(Method.GET, BuildUrl.getFbGroupsUrl(), null, Response.Listener<JSONArray> {


                    response ->

                    Log.d("response","response"+ response.toString())

                    try {
                        accounts_arraylist?.clear()
                        //alert(array_fbaccounts.toString()).show()

                        if(response.length() < 1)
                        {

                            progressDialog?.dismiss()

                            alert("No Facebook Group!").show()

                            Handler().postDelayed(object : Runnable {

                                // Using handler with postDelayed called runnable run method


                                override fun run() {

                                    try {
                                        onBackPressed()
                                    } catch (e: Exception) {
                                    } finally {
                                    }

                                }
                            }, 2 * 1000) // wait for 5 seconds
                        }
                        else {
                            for (i in 0..response.length() - 1) {
                                Log.d("url", "in loop")

                                val result = response.getJSONObject(i)
                                val id = result.getInt(JsonKeys.variables.KEY_ID)
                                val group_id = result.getInt(JsonKeys.variables.KEY_GROUP_ID)
                                val group_name = result.getString(JsonKeys.variables.KEY_GROUP_NAME)
                                val picture = result.getString(JsonKeys.variables.KEY_PICTURE)
                                val access_token = result.getString(JsonKeys.variables.KEY_ACCESS_TOKEN)
                                val group_privacy = result.getString(JsonKeys.variables.KEY_GROUP_PRIVACY)



                                accounts_arraylist?.add(AccountsDataAdapter(id, group_id, group_name, picture, access_token, group_privacy))

                            }

                            val DataAdapter = CustomAdapter()

                            listView_accounts?.adapter = DataAdapter
                            DataAdapter.notifyDataSetChanged()

                            Log.d("size", "accounts array list size on load default " + accounts_arraylist?.size.toString())

                            type = "gp"

                            progressDialog?.dismiss()
                        }

                    } catch(e: Exception) {

                        Log.d("JsonResponse",""+ e.message)
                        progressDialog?.dismiss()
                        var alert = AlertManager("Network problem!",5000,this@AccountsList)


                    }


                },
                        Response.ErrorListener {

                            error ->

                            Log.d("response",""+ error.message)
                            Toast.makeText(this, "error", Toast.LENGTH_LONG).show()
                            progressDialog?.dismiss()

                            var alert = AlertManager("Network problem!",5000,this@AccountsList)



                        }
                ) {

                    @Throws(AuthFailureError::class)
                    override fun getHeaders(): Map<String, String> {
                        return Constants.getAuthHeader()
                    }
                }
                request.setRetryPolicy(DefaultRetryPolicy(
                        25000,
                        0,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT))


                ApplicationController.instance?.addToRequestQueue(request)


            } catch (e: Exception) {

                Log.d("oncreate",""+ e.message)
                var alert = AlertManager("Network problem!",5000,this@AccountsList)

            }

        }
        else if(activity_title == resources.getString(R.string.string_toolbarTitle_companies))
        {


            try {

                progressDialog = ProgressDialog.show(this@AccountsList, "Accessing Companies", "Please wait....", true)
                progressDialog?.setCancelable(false)
                BuildUrl.setLiCompaniesUrl(accountid)
                Log.d("url", BuildUrl.getLiCompaniesUrl())

                var request = object : JsonArrayRequest(Method.GET, BuildUrl.getLiCompaniesUrl(), null, Response.Listener<JSONArray> {


                    response ->
                    Log.d("response","response"+ response.toString())


                    try {
                        accounts_arraylist?.clear()
                        //alert(array_fbaccounts.toString()).show()

                        if(response.length() < 1)
                        {

                            progressDialog?.dismiss()

                            alert("No Linkedin Company!").show()

                            Handler().postDelayed(object : Runnable {

                                // Using handler with postDelayed called runnable run method


                                override fun run() {

                                    onBackPressed()

                                }
                            }, 2 * 1000) // wait for 5 seconds
                        }
                        else {
                            for (i in 0..response.length() - 1) {
                                Log.d("url", "in loop")
                                val result = response.getJSONObject(i)
                                val id = result.getInt(JsonKeys.variables.KEY_ID)
                                val linkedin_id = result.getInt(JsonKeys.variables.KEY_LINKEDIN_ID)
                                val company_id = result.getInt(JsonKeys.variables.KEY_COMPANY_ID)
                                val username = result.getString(JsonKeys.variables.KEY_USERNAME)
                                val picture = result.getString(JsonKeys.variables.KEY_PICTURE)
                                val website_url = result.getString(JsonKeys.variables.KEY_WEBSITE_URL)


                                //(FBAccountsList as ArrayList<AccountsDataAdapter>).add(AccountsDataAdapter(id, fb_id, name, picture, access_token, pages_count))
                                //Log.d("url", "name = " + FBAccountsList?.get(i)?.getUsername())

                                accounts_arraylist?.add(AccountsDataAdapter(id, linkedin_id, company_id, username, picture, website_url))
                                val DataAdapter = CustomAdapter()

                                listView_accounts?.adapter = DataAdapter
                                DataAdapter.notifyDataSetChanged()
                            }

                            Log.d("size", "accounts array list size on load default " + accounts_arraylist?.size.toString())

                            type = "company"
                            progressDialog?.dismiss()

                        }


                    } catch(e: Exception) {

                        Log.d("JsonResponse", ""+e.message)
                        progressDialog?.dismiss()
                        var alert = AlertManager("Network problem!",5000,this@AccountsList)


                    }


                },
                        Response.ErrorListener {

                            error ->

                            Log.d("response",""+ error.message)
                            Toast.makeText(this, "error", Toast.LENGTH_LONG).show()
                            progressDialog?.dismiss()
                            var alert = AlertManager("Network problem!",5000,this@AccountsList)



                        }
                ) {

                    @Throws(AuthFailureError::class)
                    override fun getHeaders(): Map<String, String> {
                        return Constants.getAuthHeader()
                    }
                }
                request.setRetryPolicy(DefaultRetryPolicy(
                        25000,
                        0,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT))


                ApplicationController.instance?.addToRequestQueue(request)


            } catch (e: Exception) {

                Log.d("oncreate",""+ e.message)
                var alert = AlertManager("Network problem!",5000,this@AccountsList)


            }


        }
        else if(activity_title == resources.getString(R.string.string_toolbarTitle_circles))
        {


            try {



                progressDialog = ProgressDialog.show(this@AccountsList, "Accessing Circles", "Please wait....", true)
                progressDialog?.setCancelable(false)
                BuildUrl.setGpCirlcesUrl(accountid)
                Log.d("url", BuildUrl.getGpCirlcesUrl())

                var request = object : JsonArrayRequest(Method.GET, BuildUrl.getGpCirlcesUrl(), null, Response.Listener<JSONArray> {


                    response ->
                    Log.d("response","response"+ response.toString())


                    try {
                        accounts_arraylist?.clear()
                        //alert(array_fbaccounts.toString()).show()


                        if(response.length() < 1)
                        {

                            progressDialog?.dismiss()

                            alert("No Google+ Circle!").show()

                            Handler().postDelayed(object : Runnable {

                                // Using handler with postDelayed called runnable run method


                                override fun run() {

                                    onBackPressed()

                                }
                            }, 2 * 1000) // wait for 5 seconds
                        }
                        else {
                            for (i in 0..response.length() - 1) {
                                Log.d("url", "in loop")
                                val result = response.getJSONObject(i)
                                val id = result.getInt(JsonKeys.variables.KEY_ID)
                                val gp_circle_id = result.getString(JsonKeys.variables.KEY_GP_CIRCLES_ID)
                                val fb_id = result.getInt(JsonKeys.variables.KEY_FB_ID)
                                val name = result.getString(JsonKeys.variables.KEY_NAME)

                                //(FBAccountsList as ArrayList<AccountsDataAdapter>).add(AccountsDataAdapter(id, fb_id, name, picture, access_token, pages_count))
                                //Log.d("url", "name = " + FBAccountsList?.get(i)?.getUsername())

                                accounts_arraylist?.add(AccountsDataAdapter(id, gp_circle_id, fb_id, name))
                                val DataAdapter = CustomAdapter()

                                listView_accounts?.adapter = DataAdapter
                                DataAdapter.notifyDataSetChanged()
                            }

                            Log.d("size", "accounts array list size on load default " + accounts_arraylist?.size.toString())
                            type = "gpc"

                            progressDialog?.dismiss()


                        }

                    } catch(e: Exception) {

                        Log.d("JsonResponse",""+ e.message)
                        progressDialog?.dismiss()

                        var alert = AlertManager("Network problem!",5000,this@AccountsList)


                    }


                },
                        Response.ErrorListener {

                            error ->

                            Log.d("response",""+ error.message)
                            Toast.makeText(this, "error", Toast.LENGTH_LONG).show()
                            progressDialog?.dismiss()
                            var alert = AlertManager("Network problem!",5000,this@AccountsList)



                        }
                ) {

                    @Throws(AuthFailureError::class)
                    override fun getHeaders(): Map<String, String> {
                        return Constants.getAuthHeader()
                    }
                }

                request.setRetryPolicy(DefaultRetryPolicy(
                        25000,
                        0,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT))
                ApplicationController.instance?.addToRequestQueue(request)


            } catch (e: Exception) {

                Log.d("oncreate",""+ e.message)
                var alert = AlertManager("Network problem!",5000,this@AccountsList)


            }


        }
        else if(activity_title == resources.getString(R.string.string_toolbarTitle_collections))
        {



            try {

                progressDialog = ProgressDialog.show(this@AccountsList, "Accessing Collections", "Please wait....", true)
                progressDialog?.setCancelable(false)
                BuildUrl.setGpCollectionsUrl(accountid)
                Log.d("url", BuildUrl.getGpCollectionsUrl())

                var request = object : JsonArrayRequest(Method.GET, BuildUrl.getGpCollectionsUrl(), null, Response.Listener<JSONArray> {


                    response ->

                    Log.d("response","response"+ response.toString())

                    try {
                        accounts_arraylist?.clear()
                        //alert(array_fbaccounts.toString()).show()

                        if(response.length() < 1)
                        {

                            progressDialog?.dismiss()

                            alert("No Google+ Collection!").show()

                            Handler().postDelayed(object : Runnable {

                                // Using handler with postDelayed called runnable run method


                                override fun run() {

                                    onBackPressed()

                                }
                            }, 2 * 1000) // wait for 5 seconds
                        }
                        else {
                            for (i in 0..response.length() - 1) {
                                Log.d("url", "in loop")
                                val result = response.getJSONObject(i)
                                val id = result.getInt(JsonKeys.variables.KEY_ID)
                                val gp_collection_id = result.getString(JsonKeys.variables.KEY_GP_COLLECTIONS_ID)
                                val fb_id = result.getInt(JsonKeys.variables.KEY_FB_ID)
                                val name = result.getString(JsonKeys.variables.KEY_NAME)
                                val picture = result.getString(JsonKeys.variables.KEY_PICTURE)
                                //(FBAccountsList as ArrayList<AccountsDataAdapter>).add(AccountsDataAdapter(id, fb_id, name, picture, access_token, pages_count))
                                //Log.d("url", "name = " + FBAccountsList?.get(i)?.getUsername())

                                accounts_arraylist?.add(AccountsDataAdapter(id, gp_collection_id, fb_id, name, picture))
                                val DataAdapter = CustomAdapter()

                                listView_accounts?.adapter = DataAdapter
                                DataAdapter.notifyDataSetChanged()
                            }

                            Log.d("size", "accounts array list size on load default " + accounts_arraylist?.size.toString())
                            type = "gpcl"

                            progressDialog?.dismiss()

                        }

                    } catch(e: Exception) {

                        Log.d("JsonResponse",""+ e.message)
                        progressDialog?.dismiss()
                        var alert = AlertManager("Network problem!",5000,this@AccountsList)


                    }


                },
                        Response.ErrorListener {

                            error ->

                            Log.d("response",""+error.message)
                            Toast.makeText(this, "error", Toast.LENGTH_LONG).show()
                            progressDialog?.dismiss()
                            var alert = AlertManager("Network problem!",5000,this@AccountsList)



                        }
                ) {

                    @Throws(AuthFailureError::class)
                    override fun getHeaders(): Map<String, String> {
                        return Constants.getAuthHeader()
                    }
                }
                request.setRetryPolicy(DefaultRetryPolicy(
                        25000,
                        0,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT))

                ApplicationController.instance?.addToRequestQueue(request)


            } catch (e: Exception) {

                Log.d("oncreate",""+ e.message)

                var alert = AlertManager("Network problem!",5000,this@AccountsList)


            }



        }
    }

    fun checkPackages()
    {

        BuildUrl.setLoginUrl(loginManager?.getUserName(), loginManager?.getPass())

        var request = object : JsonObjectRequest(Method.GET, BuildUrl.getLoginUrl(), null, Response.Listener<JSONObject> {

            response ->

            try {
                //Log.d("response", response.getJSONObject("data").toString())

                //alert(response.getString("ID")).show()



                var products = response.getJSONObject(JsonKeys.objects.KEY_PRODUCTS)

                status = products.getBoolean(JsonKeys.variables.KEY_STATUS)
                automation = products.getBoolean(JsonKeys.variables.KEY_AUTOMATION)
                ios = products.getBoolean(JsonKeys.variables.KEY_IOS)








            } catch(e: Exception) {



            }


        },
                Response.ErrorListener {

                    error ->

                    //progressDialog?.dismiss()

                    //Toast.makeText(this, "error " +error.message, Toast.LENGTH_LONG).show()


                }
        ) {}

        request.setRetryPolicy(DefaultRetryPolicy(
                25000,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT))

        ApplicationController.instance?.addToRequestQueue(request)
    }

}
