package com.example.sarimahmed.bleupage.Activities

import android.app.ProgressDialog
import android.content.Intent
import android.content.IntentFilter
import android.graphics.Bitmap
import android.graphics.drawable.Drawable
import android.net.ConnectivityManager
import android.os.AsyncTask
import android.support.v7.app.ActionBarDrawerToggle
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Handler
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.*
import android.widget.*

import com.example.sarimahmed.bleupage.R
import com.example.sarimahmed.bleupage.SessionManager.LoginManager
import android.widget.ArrayAdapter
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.example.sarimahmed.bleupage.Application.ApplicationController
import com.example.sarimahmed.bleupage.HelperClasses.*
import org.json.JSONObject
import com.android.volley.AuthFailureError
import android.view.ViewGroup
import com.android.volley.DefaultRetryPolicy
import com.android.volley.toolbox.JsonArrayRequest
import com.bumptech.glide.Glide
import com.bumptech.glide.load.engine.DiskCacheStrategy
import com.bumptech.glide.request.animation.GlideAnimation
import com.bumptech.glide.request.target.SimpleTarget
import com.example.sarimahmed.bleupage.Adapter.AccountsDataAdapter
import com.example.sarimahmed.bleupage.BroadcastReceivers.ConnectivityReceiver
import com.example.sarimahmed.bleupage.SessionManager.UserDataManager
import org.jetbrains.anko.alert
import org.jetbrains.anko.toast
import org.json.JSONArray


class MainActivity : AppCompatActivity(),View.OnClickListener,AdapterView.OnItemClickListener , ConnectivityReceiver.ConnectivityReceiverListener,View.OnTouchListener{



    private var drawerToggle: ActionBarDrawerToggle? = null
    private var homeToolbar: Toolbar? = null
    private var button_createPost: Button? = null
    private var loginManager: LoginManager? = null
    private var alert: AlertManager? = null
    private var ic_facebook: ImageButton? = null
    private var ic_twitter: ImageButton? = null
    private var ic_linkedin: ImageButton? = null
    private var ic_instagram: ImageButton? = null
    private var ic_pinterest: ImageButton? = null
    private var ic_googleplus: ImageButton? = null
    private var textView_welcome: TextView? = null
    private var listView_accounts: ListView? = null
    private var listview_items: Array<String>? = null
    private var imageview_display_picture: RoundedImageView? = null
    private var textview_username: TextView? = null
    private var textview_pages_count: TextView? = null
    private var accounts_arraylist: ArrayList<AccountsDataAdapter>? = null
    private var accountsdataadapter: AccountsDataAdapter? = null
    private var progressDialog: ProgressDialog? = null
    //private var list: ArrayList<AccountsDataAdapter>? = null
    private var FBAccountsList: ArrayList<AccountsDataAdapter>? = null
    private var TWAccountsList: ArrayList<AccountsDataAdapter>? = null
    private var LIAccountsList: ArrayList<AccountsDataAdapter>? = null
    private var INAccountsList: ArrayList<AccountsDataAdapter>? = null
    private var PINAccountsList: ArrayList<AccountsDataAdapter>? = null
    private var GPAccountsList: ArrayList<AccountsDataAdapter>? = null
    private var AccountType: String = "FB"
    private var DataAdapter: CustomAdapter? = null
    private var ic_add_account: ImageButton? = null
    internal var isInternetConnected = ConnectivityReceiver.isConnected
    private var ConnectionReceiver: ConnectivityReceiver? = null
    private var id: Int? = null
    private var type = ""


    override fun onCreate(savedInstanceState: Bundle?) {

        loginManager = LoginManager(this.applicationContext)
        loginManager?.checkLogin()






        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)


        homeToolbar = findViewById(R.id.my_toolbar) as Toolbar
        setSupportActionBar(homeToolbar)
        //supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_user)
        checkPackages()

        DataAdapter = CustomAdapter()


        super.setTitle("")






        textView_welcome = findViewById(R.id.textView_welcome) as TextView?
        textView_welcome?.text = "Welcome " + loginManager?.getDisplayName()

        accounts_arraylist = ArrayList<AccountsDataAdapter>()

        //list = ArrayList<AccountsDataAdapter>()


        listView_accounts = findViewById(R.id.accounts_listview) as ListView?

        //registerForContextMenu(listView_accounts)


        listView_accounts?.setOnItemClickListener(this)
        /* listview_items = resources.getStringArray(R.array.hello_array)

        val itemsAdapter = ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, listview_items)
        listView_accounts?.adapter = itemsAdapter*/


//--------------------Load All ACcounts in arrays------------------//


        button_createPost = findViewById(R.id.button_createPost) as Button
        button_createPost!!.setOnClickListener(this)
        ic_facebook = findViewById(R.id.ic_facebook) as ImageButton
        ic_facebook!!.setOnClickListener(this)
        ic_facebook!!.setOnTouchListener(this)
        ic_twitter = findViewById(R.id.ic_twitter) as ImageButton
        ic_twitter!!.setOnClickListener(this)
        ic_linkedin = findViewById(R.id.ic_linkedin) as ImageButton
        ic_linkedin!!.setOnClickListener(this)
        ic_instagram = findViewById(R.id.ic_instagram) as ImageButton
        ic_instagram!!.setOnClickListener(this)
        ic_pinterest = findViewById(R.id.ic_pinterest) as ImageButton
        ic_pinterest!!.setOnClickListener(this)
        ic_googleplus = findViewById(R.id.ic_googleplus) as ImageButton
        ic_googleplus!!.setOnClickListener(this)
        ic_add_account = findViewById(R.id.ic_add_account) as ImageButton?
        ic_add_account!!.setOnClickListener(this)


        try {
            ic_facebook?.imageTintList = resources.getColorStateList(R.color.darkgrey)
            ic_twitter?.imageTintList = resources.getColorStateList(R.color.darkgrey)
            ic_linkedin?.imageTintList = resources.getColorStateList(R.color.darkgrey)
            ic_instagram?.imageTintList = Constants.context?.resources?.getColorStateList(R.color.darkgrey)
            ic_pinterest?.imageTintList = resources.getColorStateList(R.color.darkgrey)
            ic_googleplus?.imageTintList = resources.getColorStateList(R.color.darkgrey)
        } catch (e: Exception) {
        } finally {
        }

        try {
            checkPackages()
        } catch (e: Exception) {
        } finally {
        }

        val filter = IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION)
        ConnectionReceiver = ConnectivityReceiver()
        try {
            registerReceiver(ConnectionReceiver, filter)
        } catch(e: Exception) {
            Log.d("reciever",e.message)
        } finally {
        }




        loadAllAccounts()

    }
//------------------onCreate() ends here-------------------//


    override fun onBackPressed() {

        try {
            UserDataManager.FBAccountsList?.clear()
            UserDataManager.TWAccountsList?.clear()
            UserDataManager.LIAccountsList?.clear()
            UserDataManager.INAccountsList?.clear()
            UserDataManager.PINAccountsList?.clear()
            UserDataManager.GPAccountsList?.clear()
            UserDataManager.FBGroupList?.clear()
            UserDataManager.LICompanyList?.clear()
            UserDataManager.FBPagesList?.clear()
            UserDataManager.GPCirclesList?.clear()
            UserDataManager.GPCollectionsList?.clear()
            UserDataManager.FBListForPost?.clear()
            UserDataManager?.LIListForPost?.clear()
            UserDataManager?.GPListForPost?.clear()

            unregisterReceiver(ConnectionReceiver)
        } catch (e: Exception) {
        } finally {
        }

        super.onBackPressed()

    }

    override fun onPause() {

        super.onPause()
    }

    override fun onResume() {
        ApplicationController.instance?.setConnectivityListener(this)

        super.onResume()
    }

    override fun onTouch(v: View?, event: MotionEvent?): Boolean {
        val id = v?.getId()
        when(id)
        {
            R.id.ic_facebook ->
            {
                if (event?.getAction() === MotionEvent.ACTION_DOWN) {
                    //Button Pressed
                    // voiceactivitybtn.setColorFilter(getResources().getColor(R.color.mytoolbarbackgroundcolor));
Log.d("touch","action down")
                }
                if (event?.getAction() === MotionEvent.ACTION_UP) {
                    //finger was lifted
                    //voiceactivitybtn.setColorFilter(getResources().getColor(R.color.mytextcolor));
                    Log.d("touch","action up")


                }
            }


        }
        return false

    }

    //OnClick Listener Implementation below
    override fun onClick(v: View?) {

        val id = v?.getId()

        when (id) {

            R.id.ic_add_account -> {
                val intent = Intent(this, AddAccountActivity::class.java)
                startActivity(intent)

                overridePendingTransition(R.anim.enter_vertical, R.anim.exit_vertical);
            }


            R.id.button_createPost -> {

                val intent = Intent(this, CreatePostActivity::class.java)
                startActivity(intent)
                overridePendingTransition(R.anim.enter_vertical, R.anim.exit_vertical);

            }
            R.id.ic_facebook -> {

                try {
                    loadFBAccounts()
                    ic_facebook?.imageTintList = resources.getColorStateList(R.color.white)
                    ic_twitter?.imageTintList = resources.getColorStateList(R.color.darkgrey)
                    ic_linkedin?.imageTintList = resources.getColorStateList(R.color.darkgrey)
                    ic_instagram?.imageTintList = resources.getColorStateList(R.color.darkgrey)
                    ic_pinterest?.imageTintList = resources.getColorStateList(R.color.darkgrey)
                    ic_googleplus?.imageTintList = resources.getColorStateList(R.color.darkgrey)

                } catch(e: Exception) {
                    Log.d("oncreate", e.printStackTrace().toString())
                }
            }
            R.id.ic_twitter -> {
                try {
                    loadTWAccounts()

                    ic_twitter?.imageTintList = resources.getColorStateList(R.color.white)
                    ic_facebook?.imageTintList = resources.getColorStateList(R.color.darkgrey)
                    ic_linkedin?.imageTintList = resources.getColorStateList(R.color.darkgrey)
                    ic_instagram?.imageTintList = resources.getColorStateList(R.color.darkgrey)
                    ic_pinterest?.imageTintList = resources.getColorStateList(R.color.darkgrey)
                    ic_googleplus?.imageTintList = resources.getColorStateList(R.color.darkgrey)


                } catch(e: Exception) {
                    Log.d("oncreate", e.printStackTrace().toString())
                }
            }
            R.id.ic_linkedin -> {
                try {
                    loadLIAccounts()
                    ic_linkedin?.imageTintList = resources.getColorStateList(R.color.white)
                    ic_facebook?.imageTintList = resources.getColorStateList(R.color.darkgrey)
                    ic_twitter?.imageTintList = resources.getColorStateList(R.color.darkgrey)
                    ic_instagram?.imageTintList = resources.getColorStateList(R.color.darkgrey)
                    ic_pinterest?.imageTintList = resources.getColorStateList(R.color.darkgrey)
                    ic_googleplus?.imageTintList = resources.getColorStateList(R.color.darkgrey)

                } catch(e: Exception) {
                    Log.d("oncreate", e.printStackTrace().toString())
                }
            }
            R.id.ic_instagram -> {
                try {
                    loadINAccounts()
                    ic_instagram?.imageTintList = resources.getColorStateList(R.color.white)
                    ic_facebook?.imageTintList = resources.getColorStateList(R.color.darkgrey)
                    ic_twitter?.imageTintList = resources.getColorStateList(R.color.darkgrey)
                    ic_linkedin?.imageTintList = resources.getColorStateList(R.color.darkgrey)
                    ic_pinterest?.imageTintList = resources.getColorStateList(R.color.darkgrey)
                    ic_googleplus?.imageTintList = resources.getColorStateList(R.color.darkgrey)
                    Log.d("instagram", INAccountsList!!.size.toString())


                } catch(e: Exception) {
                    Log.d("oncreate", e.printStackTrace().toString())
                }

            }
            R.id.ic_pinterest -> {
                try {
                    loadPINAccounts()
                    ic_pinterest?.imageTintList = resources.getColorStateList(R.color.white)
                    ic_facebook?.imageTintList = resources.getColorStateList(R.color.darkgrey)
                    ic_twitter?.imageTintList = resources.getColorStateList(R.color.darkgrey)
                    ic_linkedin?.imageTintList = resources.getColorStateList(R.color.darkgrey)
                    ic_instagram?.imageTintList = resources.getColorStateList(R.color.darkgrey)
                    ic_googleplus?.imageTintList = resources.getColorStateList(R.color.darkgrey)

                } catch(e: Exception) {
                    Log.d("oncreate", e.printStackTrace().toString())
                }
            }
            R.id.ic_googleplus -> {
                try {
                    loadGPAccounts()

                    ic_googleplus?.imageTintList = resources.getColorStateList(R.color.white)
                    ic_facebook?.imageTintList = resources.getColorStateList(R.color.darkgrey)
                    ic_twitter?.imageTintList = resources.getColorStateList(R.color.darkgrey)
                    ic_linkedin?.imageTintList = resources.getColorStateList(R.color.darkgrey)
                    ic_instagram?.imageTintList = resources.getColorStateList(R.color.darkgrey)
                    ic_pinterest?.imageTintList = resources.getColorStateList(R.color.darkgrey)



                } catch(e: Exception) {
                    Log.d("oncreate", e.printStackTrace().toString())
                }
            }
        }
    }


    private inner class CustomAdapter : ArrayAdapter<AccountsDataAdapter>(this@MainActivity, R.layout.list_item, accounts_arraylist) {


        override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
            var convertView = convertView


            if (convertView == null) {
                convertView = layoutInflater.inflate(R.layout.list_item, parent, false)
            }

            val AccountsDataView = accounts_arraylist?.get(position)

            imageview_display_picture = convertView!!.findViewById<RoundedImageView>(R.id.imageView_display_picture)
/*
            try {
                Glide.with(this@MainActivity).load(AccountsDataView?.getPictureUrl()).asBitmap().into(imageview_display_picture)
            } catch (e: Exception) {
                Log.d("view1",""+e.printStackTrace().toString())
                Log.d("view1",""+e.message)

            } finally {
            }*/

            try {
                Glide.with(this@MainActivity).load(AccountsDataView?.getPictureUrl()).asBitmap().fitCenter().placeholder(resources.getDrawable(R.drawable.avatar_user)).diskCacheStrategy(DiskCacheStrategy.ALL).into(object: SimpleTarget<Bitmap>(75,75)
                {
                    override fun onResourceReady(resource: Bitmap?, glideAnimation: GlideAnimation<in Bitmap>?) {

                        imageview_display_picture?.setImageBitmap(resource)
                    }

                    override fun onLoadFailed(e: java.lang.Exception?, errorDrawable: Drawable?) {

                        Log.d("view1",e?.message.toString())
                        super.onLoadFailed(e, errorDrawable)
                    }

                })
            } catch (e: Exception) {
            } finally {
            }





            textview_username = convertView.findViewById<TextView>(R.id.textView_username)
            textview_pages_count = convertView.findViewById<TextView>(R.id.textView_pages_count)



            // imageview_display_picture.setImageURI(AccountsDataView?.getPictureUrl())
            if (AccountType == "TW" || AccountType == "LI" || AccountType == "IN" || AccountType == "PIN" || AccountType == "GP") {
                textview_username?.text = AccountsDataView?.getDisplayName()

            } else {
                textview_username?.text = AccountsDataView?.getUsername()

            }
            if (AccountsDataView?.getPagesCount() != null) {
                textview_pages_count?.text = ""+ AccountsDataView?.getPagesCount()+" Page(s)"
            }

            //var load = SetThumbnail(AccountsDataView?.getPictureUrl(),imageview_display_picture!!)
           //load.execute()

            Log.d("view1", AccountsDataView?.getPictureUrl())
            //textview_pages_count?.text = AccountsDataView?.getPagesCount()



            return convertView
        }
    }

    fun loadDefaulAccount() {
        try {
            BuildUrl.setSocialAccountsUrl(loginManager!!.getUserID())
            Log.d("url", BuildUrl.getSocialAccountsUrl())

            var request = object : JsonObjectRequest(Method.GET, BuildUrl.getSocialAccountsUrl(), null, Response.Listener<JSONObject> {


                response ->


                try {
                    accounts_arraylist?.clear()
                    val array_fbaccounts = response.getJSONArray(JsonKeys.arrays.KEY_FBACCOUNTS)
                    //alert(array_fbaccounts.toString()).show()
                    for (i in 0..array_fbaccounts.length() - 1) {
                        Log.d("url", "in loop")
                        val result = array_fbaccounts.getJSONObject(i)
                        val id = result.getInt(JsonKeys.variables.KEY_ID)
                        val fb_id = result.getInt(JsonKeys.variables.KEY_FB_ID)
                        val name = result.getString(JsonKeys.variables.KEY_NAME)
                        val picture = result.getString(JsonKeys.variables.KEY_PICTURE)
                        val access_token = result.getString(JsonKeys.variables.KEY_ACCESS_TOKEN)
                        val pages_count = result.getInt(JsonKeys.variables.KEY_PAGES_COUNT)

                        //(FBAccountsList as ArrayList<AccountsDataAdapter>).add(AccountsDataAdapter(id, fb_id, name, picture, access_token, pages_count))
                        //Log.d("url", "name = " + FBAccountsList?.get(i)?.getUsername())

                        accounts_arraylist?.add(AccountsDataAdapter(id, fb_id, name, picture, access_token, pages_count))
                        val DataAdapter = CustomAdapter()

                        listView_accounts?.adapter = DataAdapter
                        DataAdapter.notifyDataSetChanged()
                    }

                    Log.d("size", "accounts array list size on load default " + accounts_arraylist?.size.toString())


                } catch(e: Exception) {

                    Log.d("JsonResponse", e.message)
                    progressDialog?.dismiss()

                }


            },
                    Response.ErrorListener {

                        error ->

                        Log.d("response", error.message)
                        Toast.makeText(this, "error", Toast.LENGTH_LONG).show()
                        progressDialog?.dismiss()


                    }
            ) {

                @Throws(AuthFailureError::class)
                override fun getHeaders(): Map<String, String> {
                    return Constants.getAuthHeader()
                }
            }


            ApplicationController.instance?.addToRequestQueue(request)


        } catch (e: Exception) {

            Log.d("oncreate", e.message)

        }
    }

    fun loadAllAccounts() {

        if (isInternetConnected) {


            try {
                progressDialog = ProgressDialog.show(this@MainActivity, "Accessing Accounts", "Please wait....", true)
                progressDialog?.setCancelable(false)



                BuildUrl.setAllAccountsUrl(loginManager!!.getUserID())
                Log.d("buildurl", BuildUrl.getAllAccountsUrl())

                var request = object : JsonObjectRequest(Method.GET, BuildUrl.getAllAccountsUrl(), null, Response.Listener<JSONObject> {


                    response ->
                    Log.d("resp",response.toString())


                    try {


                        val array_fbaccounts = response.getJSONArray(JsonKeys.arrays.KEY_ACCOUNTS)
                        //alert(array_fbaccounts.toString()).show()
                        for (i in 0..array_fbaccounts.length() - 1) {
                            Log.d("url", "in loop")
                            val result = array_fbaccounts.getJSONObject(i)
                            val id = result.getInt(JsonKeys.variables.KEY_ID)
                            val fb_id = result.getInt(JsonKeys.variables.KEY_FB_ID)
                            val name = result.getString(JsonKeys.variables.KEY_NAME)
                            val picture = result.getString(JsonKeys.variables.KEY_PICTURE)
                            val access_token = result.getString(JsonKeys.variables.KEY_ACCESS_TOKEN)
                            val pages_count = result.getInt(JsonKeys.variables.KEY_PAGES_COUNT)

                            //(UserDataManager.FBAccountsList as ArrayList<AccountsDataAdapter>).add(AccountsDataAdapter(id, fb_id, name, picture, access_token, pages_count))
                            UserDataManager.insertFBData(id, fb_id, name, picture, access_token, pages_count)

                            UserDataManager.insertFBDataForPost("fb:"+id,name)

                            //(FBAccountsList as ArrayList<AccountsDataAdapter>).add(AccountsDataAdapter(id, fb_id, name, picture, access_token, pages_count))
                            //Log.d("url", "name = " + FBAccountsList?.get(i)?.getUsername())


                        }


                       // Log.d("size", "FBAccounts list in onloadALL  " + FBAccountsList?.size.toString())


                        val array_fbpages = response.getJSONArray(JsonKeys.arrays.KEY_PAGES)


                        //alert(array_fbaccounts.toString()).show()
                        for (i in 0..array_fbpages.length() - 1) {
                            Log.d("url", "in loop")
                            val result = array_fbpages.getJSONObject(i)
                            val id = result.getInt(JsonKeys.variables.KEY_ID)
                            val fb_id = result.getInt(JsonKeys.variables.KEY_FB_ID)
                            val name = result.getString(JsonKeys.variables.KEY_NAME)
                            val picture = result.getString(JsonKeys.variables.KEY_PICTURE)
                            val access_token = result.getString(JsonKeys.variables.KEY_ACCESS_TOKEN)
                            val posts_count = result.getInt(JsonKeys.variables.KEY_POSTS_COUNT)


                            UserDataManager.insertFBPagesData(id, fb_id, name, picture, access_token, posts_count)
                            UserDataManager.insertFBDataForPost("pg:"+id,name)

                        }

                        val array_fbgroups = response.getJSONArray(JsonKeys.arrays.KEY_GROUPS)

                        Log.d("url", response.getJSONArray(JsonKeys.arrays.KEY_GROUPS).toString())

                        for (i in 0..array_fbgroups.length() - 1) {

                            val result = array_fbgroups.getJSONObject(i)
                            val id = result.getInt(JsonKeys.variables.KEY_ID)
                            val fb_id = result.getInt(JsonKeys.variables.KEY_FB_ID)
                            val group_name = result.getString(JsonKeys.variables.KEY_NAME)
                            val picture = result.getString(JsonKeys.variables.KEY_PICTURE)
                            val access_token = result.getString(JsonKeys.variables.KEY_ACCESS_TOKEN)
                            val group_privacy = result.getString(JsonKeys.variables.KEY_GROUP_PRIVACY)


                            UserDataManager.insertFBGroupsData(id,fb_id,group_name,picture,access_token,group_privacy)
                            UserDataManager.insertFBDataForPost("gp:"+id,group_name)



                        }

                        Log.d("arraysize","gp circles: "+UserDataManager.FBGroupList!!.size)



                        val array_twaccounts = response.getJSONArray(JsonKeys.arrays.KEY_TWITTER)



                        for (i in 0..array_twaccounts.length() - 1) {
                            Log.d("url", "in loop")
                            Log.d("twitter",""+array_twaccounts.length())
                            val result = array_twaccounts.getJSONObject(i)
                            val id = result.getInt(JsonKeys.variables.KEY_ID)
                            val fb_id = result.getInt(JsonKeys.variables.KEY_USER_ID_TWITTER)
                            val username = result.getString(JsonKeys.variables.KEY_USERNAME)
                            val display_name = result.getString(JsonKeys.variables.KEY_NAME)
                            val picture = result.getString(JsonKeys.variables.KEY_PICTURE)
                            Log.d("result",""+id+fb_id+username+display_name+picture)


                            UserDataManager.insertTWData(id, fb_id, username, display_name, picture)

                        }

                        Log.d("usermanager", " " + UserDataManager.TWAccountsList?.size)



                        val array_liaccounts = response.getJSONArray(JsonKeys.arrays.KEY_LINKEDIN)

                        for (i in 0..array_liaccounts.length() - 1) {
                            Log.d("url", "in loop")
                            val result = array_liaccounts.getJSONObject(i)
                            val id = result.getInt(JsonKeys.variables.KEY_ID)
                            val fb_id = result.getInt(JsonKeys.variables.KEY_FB_ID)
                            val username = result.getString(JsonKeys.variables.KEY_USERNAME)
                            val name = result.getString(JsonKeys.variables.KEY_NAME)
                            val picture = result.getString(JsonKeys.variables.KEY_PICTURE)
                            val token = result.getString(JsonKeys.variables.KEY_TOKEN)


                            UserDataManager.insertLIData(fb_id,username,id,token,name,picture)
                            UserDataManager.insertLIDataForPost("li"+":"+id,null,name,token)

                        }

                        val array_licompanies = response.getJSONArray(JsonKeys.arrays.KEY_LICOMPANIES)

                        for (i in 0..array_licompanies.length() - 1) {
                            Log.d("url", "in loop")
                            val result = array_licompanies.getJSONObject(i)
                            val id = result.getInt(JsonKeys.variables.KEY_ID)
                            val linkedin_id = result.getInt(JsonKeys.variables.KEY_LINKEDIN_ID)
                            val company_id = result.getInt(JsonKeys.variables.KEY_COMPANY_ID)
                            val username = result.getString(JsonKeys.variables.KEY_USERNAME)
                            val picture = result.getString(JsonKeys.variables.KEY_PICTURE)
                            val website_url = result.getString(JsonKeys.variables.KEY_WEBSITE_URL)
                            val token = result.getString(JsonKeys.variables.KEY_TOKEN)


                            UserDataManager.insertLICompany(id, linkedin_id, company_id, username, picture,website_url,token)
                            UserDataManager.insertLIDataForPost("lic"+":"+linkedin_id,company_id,username,token)

                        }

                        val array_inaccounts = response.getJSONArray(JsonKeys.arrays.KEY_INSTAACCOUTS)


                        for (i in 0..array_inaccounts.length() - 1) {
                            Log.d("url", "in loop")
                            val result = array_inaccounts.getJSONObject(i)
                            val id = result.getInt(JsonKeys.variables.KEY_ID)
                            val fb_id = result.getInt(JsonKeys.variables.KEY_FB_ID)
                            val username = result.getString(JsonKeys.variables.KEY_USERNAME)
                            val password = result.getString(JsonKeys.variables.KEY_PASSWORD)
                            val name = result.getString(JsonKeys.variables.KEY_NAME)
                            val picture = result.getString(JsonKeys.variables.KEY_PICTURE)

                            UserDataManager.insertINData(fb_id, name, password, id, name, picture)

                        }
                        val array_pinaccounts = response.getJSONArray(JsonKeys.arrays.KEY_PINACCOUNTS)


                        //alert(array_fbaccounts.toString()).show()


                        for (i in 0..array_pinaccounts.length() - 1) {
                            Log.d("url", "in loop")
                            val result = array_pinaccounts.getJSONObject(i)
                            val id = result.getInt(JsonKeys.variables.KEY_ID)
                            val fb_id = result.getInt(JsonKeys.variables.KEY_FB_ID)
                            val username = result.getString(JsonKeys.variables.KEY_USERNAME)
                            val token = result.getString(JsonKeys.variables.KEY_TOKEN)
                            val display_name = result.getString(JsonKeys.variables.KEY_NAME)
                            val picture = result.getString(JsonKeys.variables.KEY_PICTURE)


                            UserDataManager.insertPINData(fb_id, username, id, token, display_name, picture)

                        }


                        val array_gpaccounts = response.getJSONArray(JsonKeys.arrays.KEY_GPACCOUNTS)


                        //alert(array_fbaccounts.toString()).show()


                        for (i in 0..array_gpaccounts.length() - 1) {
                            Log.d("url", "in loop")
                            val result = array_gpaccounts.getJSONObject(i)
                            val id = result.getInt(JsonKeys.variables.KEY_ID)
                            val fb_id = result.getInt(JsonKeys.variables.KEY_FB_ID)
                            val username = result.getString(JsonKeys.variables.KEY_USERNAME)
                            val token = result.getString(JsonKeys.variables.KEY_TOKEN)
                            val name = result.getString(JsonKeys.variables.KEY_NAME)
                            val picture = result.getString(JsonKeys.variables.KEY_PICTURE)



                            UserDataManager.insertGPData(fb_id, username, id, token, name, picture)
                            UserDataManager.insertGPDataForPost("gpu:"+id,name)


                        }

                        val array_gpacircles = response.getJSONArray(JsonKeys.arrays.KEY_GPCIRCLES)

                        for (i in 0..array_gpacircles.length() - 1) {
                            Log.d("url", "in loop")
                            val result = array_gpacircles.getJSONObject(i)
                            val id = result.getInt(JsonKeys.variables.KEY_ID)
                            val gp_circle_id = result.getString(JsonKeys.variables.KEY_GP_CIRCLES_ID)
                            val fb_id = result.getInt(JsonKeys.variables.KEY_FB_ID)
                            val name = result.getString(JsonKeys.variables.KEY_NAME)

                            UserDataManager.insertGPCirlcesData(id,gp_circle_id,fb_id,name)
                            UserDataManager.insertGPDataForPost("gpc:"+id,name)

                        }

                        Log.d("arraysize","gp circles: "+UserDataManager.GPCirclesList!!.size)


                        val array_gpcollections = response.getJSONArray(JsonKeys.arrays.KEY_GPCOLLECTIONS)


                        for (i in 0..array_gpcollections.length() - 1) {
                            Log.d("url", "in loop")
                            val result = array_gpcollections.getJSONObject(i)
                            val id = result.getInt(JsonKeys.variables.KEY_ID)
                            val gp_collection_id = result.getString(JsonKeys.variables.KEY_GP_COLLECTIONS_ID)
                            val fb_id = result.getInt(JsonKeys.variables.KEY_FB_ID)
                            val picture = result.getString(JsonKeys.variables.KEY_PICTURE)
                            val name = result.getString(JsonKeys.variables.KEY_NAME)



                            UserDataManager.insertGPCollectionsData(id,gp_collection_id,fb_id,name,picture)

                            UserDataManager.insertGPDataForPost("gpcl:"+id,name)


                        }
                        loadFBAccounts()



                        progressDialog?.dismiss()


                    } catch(e: Exception) {

                        Log.d("JsonResponse",""+ e.message)
                        var alert = AlertManager("Network problem!",5000,this@MainActivity)

                        progressDialog?.dismiss()

                    }


                },
                        Response.ErrorListener {

                            error ->

                            Log.d("response", ""+error.message)
                            //  Toast.makeText(this, "error", Toast.LENGTH_LONG).show()
                            progressDialog?.dismiss()

                            var alert = AlertManager("Network problem!",5000,this@MainActivity)



                        }
                ) {

                    @Throws(AuthFailureError::class)
                    override fun getHeaders(): Map<String, String> {
                        return Constants.getAuthHeader()
                    }
                }

                request.setRetryPolicy(DefaultRetryPolicy(
                        30000,
                        0,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT))


                ApplicationController.instance?.addToRequestQueue(request)


            } catch (e: Exception) {

                Log.d("oncreate", e.message)

            }

        }

        else if(!isInternetConnected)
        {
            toast("Please Check your Internet connection")
        }


    }

    fun loadFBAccounts() {

        Log.d("size", "load FB called")


        try {
            ic_facebook?.imageTintList = resources.getColorStateList(R.color.white)
            ic_twitter?.imageTintList = resources.getColorStateList(R.color.darkgrey)
            ic_linkedin?.imageTintList = resources.getColorStateList(R.color.darkgrey)
            ic_instagram?.imageTintList = Constants.context?.resources?.getColorStateList(R.color.darkgrey)
            ic_pinterest?.imageTintList = resources.getColorStateList(R.color.darkgrey)
            ic_googleplus?.imageTintList = resources.getColorStateList(R.color.darkgrey)
        } catch (e: Exception) {
        } finally {
        }

        accounts_arraylist?.clear()
        AccountType = "FB"
        type = "fb"
        Log.d("size", AccountType)
        Log.d("usermanager", " FB array size: " + UserDataManager.FBAccountsList?.size)
        val DataAdapter = CustomAdapter()
        listView_accounts?.adapter = DataAdapter
        DataAdapter.notifyDataSetChanged()
        for (i in 0..UserDataManager.FBAccountsList!!.size -1) {

            //accounts_arraylist?.add(AccountsDataAdapter(FBAccountsList?.get(i)?.getId(), FBAccountsList?.get(i)?.getFbid(), FBAccountsList?.get(i)?.getUsername(), FBAccountsList?.get(i)?.getPictureUrl(), FBAccountsList?.get(i)?.getAccessToken(), FBAccountsList?.get(i)?.getPagesCount()))

            //accounts_arraylist?.add(AccountsDataAdapter(UserDataManager.getFBData()?.get(i)?.getId(), UserDataManager.getFBData()?.get(i)?.getFbid(),UserDataManager.getFBData()?.get(i)?.getUsername(), UserDataManager.getFBData()?.get(i)?.getPictureUrl(), UserDataManager.getFBData()?.get(i)?.getAccessToken(), UserDataManager.getFBData()?.get(i)?.getPagesCount()))

            accounts_arraylist?.add(AccountsDataAdapter(UserDataManager.FBAccountsList?.get(i)?.getId(), UserDataManager.FBAccountsList?.get(i)?.getFbid(), UserDataManager.FBAccountsList?.get(i)?.getUsername(), UserDataManager.FBAccountsList?.get(i)?.getPictureUrl(), UserDataManager.FBAccountsList?.get(i)?.getAccessToken(), UserDataManager.FBAccountsList?.get(i)?.getPagesCount()))

            Log.d("usermanager", " " + UserDataManager.FBAccountsList?.get(i)?.getPictureUrl())



        }
        listView_accounts?.adapter = DataAdapter
        DataAdapter.notifyDataSetChanged()





    }

    fun loadTWAccounts() {

        accounts_arraylist?.clear()
        AccountType = "TW"
        type = "tw"
        Log.d("size", AccountType)

        Log.d("usermanager", "twitter array size " + UserDataManager.TWAccountsList?.size)
        val DataAdapter = CustomAdapter()
        listView_accounts?.adapter = DataAdapter
        DataAdapter.notifyDataSetChanged()

        for (i in 0..UserDataManager.TWAccountsList!!.size - 1) {
            accounts_arraylist?.add(AccountsDataAdapter(UserDataManager.TWAccountsList?.get(i)?.getId(), UserDataManager.TWAccountsList?.get(i)?.getFbid(), UserDataManager.TWAccountsList?.get(i)?.getUsername(), UserDataManager.TWAccountsList?.get(i)?.getDisplayName(), UserDataManager.TWAccountsList?.get(i)?.getPictureUrl()))

        }
        listView_accounts?.adapter = DataAdapter
        DataAdapter.notifyDataSetChanged()
    }

    fun loadLIAccounts() {
        Log.d("usermanager", "li array size " + UserDataManager.LIAccountsList?.size)

        accounts_arraylist?.clear()
        AccountType = "LI"
        type = "linkedin"
        Log.d("size", AccountType)
        val DataAdapter = CustomAdapter()
        listView_accounts?.adapter = DataAdapter
        DataAdapter.notifyDataSetChanged()
        for (i in 0..UserDataManager.LIAccountsList!!.size - 1) {

            accounts_arraylist?.add(AccountsDataAdapter(UserDataManager.LIAccountsList?.get(i)?.getFbid(), UserDataManager.LIAccountsList?.get(i)?.getUsername(), UserDataManager.LIAccountsList?.get(i)?.getId(), UserDataManager.LIAccountsList?.get(i)?.getToken(), UserDataManager.LIAccountsList?.get(i)?.getDisplayName(), UserDataManager.LIAccountsList?.get(i)?.getPictureUrl()))

        }
        listView_accounts?.adapter = DataAdapter
        DataAdapter.notifyDataSetChanged()
    }

    fun loadINAccounts() {
        Log.d("usermanager", "insta array size " + UserDataManager.INAccountsList?.size)

        accounts_arraylist?.clear()

        AccountType = "IN"
        type = "ins"
        Log.d("size", AccountType)
        val DataAdapter = CustomAdapter()

        listView_accounts?.adapter = DataAdapter
        DataAdapter.notifyDataSetChanged()


        for (i in 0..UserDataManager.INAccountsList!!.size - 1) {

            accounts_arraylist?.add(AccountsDataAdapter(UserDataManager.INAccountsList?.get(i)?.getFbid(), UserDataManager.INAccountsList?.get(i)?.getUsername(), UserDataManager.INAccountsList?.get(i)?.getPassword(), UserDataManager.INAccountsList?.get(i)?.getId(), UserDataManager.INAccountsList?.get(i)?.getDisplayName(), UserDataManager.INAccountsList?.get(i)?.getPictureUrl()))

        }
        listView_accounts?.adapter = DataAdapter
        DataAdapter.notifyDataSetChanged()

    }

    fun loadPINAccounts() {
        Log.d("usermanager", "pin array size " + UserDataManager.PINAccountsList?.size)

        accounts_arraylist?.clear()

        AccountType = "PIN"
        type = "pin"
        Log.d("size", AccountType)
        val DataAdapter = CustomAdapter()

        listView_accounts?.adapter = DataAdapter
        DataAdapter.notifyDataSetChanged()

        for (i in 0..UserDataManager.PINAccountsList!!.size - 1) {

            accounts_arraylist?.add(AccountsDataAdapter(UserDataManager.PINAccountsList?.get(i)?.getFbid(), UserDataManager.PINAccountsList?.get(i)?.getUsername(), UserDataManager.PINAccountsList?.get(i)?.getId(), UserDataManager.PINAccountsList?.get(i)?.getToken(), UserDataManager.PINAccountsList?.get(i)?.getDisplayName(), UserDataManager.PINAccountsList?.get(i)?.getPictureUrl()))

        }

        listView_accounts?.adapter = DataAdapter
        DataAdapter.notifyDataSetChanged()
    }

    fun loadGPAccounts() {
        Log.d("usermanager", "gp array size " + UserDataManager.GPAccountsList?.size)

        accounts_arraylist?.clear()

        AccountType = "GP"
        type = "gpu"
        Log.d("size", AccountType)
        val DataAdapter = CustomAdapter()

        listView_accounts?.adapter = DataAdapter
        DataAdapter.notifyDataSetChanged()

        for (i in 0..UserDataManager.GPAccountsList!!.size - 1) {

            accounts_arraylist?.add(AccountsDataAdapter(UserDataManager.GPAccountsList?.get(i)?.getFbid(), UserDataManager.GPAccountsList?.get(i)?.getUsername(), UserDataManager.GPAccountsList?.get(i)?.getId(), UserDataManager.GPAccountsList?.get(i)?.getToken(), UserDataManager.GPAccountsList?.get(i)?.getDisplayName(), UserDataManager.GPAccountsList?.get(i)?.getPictureUrl()))

        }

        listView_accounts?.adapter = DataAdapter
        DataAdapter.notifyDataSetChanged()
    }

/*
    fun loadFBPAges() {
        BuildUrl.setFbPagesUrl(login?.getUserID())
        var request = object : JsonArrayRequest(Method.GET, BuildUrl.getFbPagesUrl(), null, Response.Listener<JSONArray> {


            response ->

            try {


                //alert(array_fbaccounts.toString()).show()
                for (i in 0..response.length() - 1) {
                    Log.d("url", "in loop")
                    val result = response.getJSONObject(i)
                    val id = result.getInt(JsonKeys.variables.KEY_ID)
                    val fb_id = result.getInt(JsonKeys.variables.KEY_FB_ID)
                    val name = result.getString(JsonKeys.variables.KEY_NAME)
                    val picture = result.getString(JsonKeys.variables.KEY_PICTURE)
                    val access_token = result.getString(JsonKeys.variables.KEY_ACCESS_TOKEN)
                    val posts_count = result.getInt(JsonKeys.variables.KEY_POSTS_COUNT)


                    UserDataManager.insertFBPagesData(id,fb_id,name,picture,access_token,posts_count)



                }


            } catch(e: Exception) {

                Log.d("JsonResponse", e.message)

            }


        },
                Response.ErrorListener {

                    error ->

                 //   Log.d("response", error.message)
                  //  Toast.makeText(this, "error", Toast.LENGTH_LONG).show()


                }
        ) {

            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                return Constants.getAuthHeader()
            }
        }


        ApplicationController.instance?.addToRequestQueue(request)

    }
*/

/*
    fun loadFBGroups() {



        BuildUrl.setFbGroupsUrl(login?.getUserID())

        Log.d("groups",BuildUrl.getFbGroupsUrl())
        var request = object : JsonArrayRequest(Method.GET, BuildUrl.getFbGroupsUrl(), null, Response.Listener<JSONArray> {


            response ->

            try {


                //alert(array_fbaccounts.toString()).show()
                for (i in 0..response.length() - 1) {
                    Log.d("url", "in loop")
                    val result = response.getJSONObject(i)
                    val id = result.getInt(JsonKeys.variables.KEY_ID)
                    val group_id = result.getInt(JsonKeys.variables.KEY_FB_ID)
                    val group_name = result.getString(JsonKeys.variables.KEY_NAME)
                    val picture = result.getString(JsonKeys.variables.KEY_PICTURE)
                    val access_token = result.getString(JsonKeys.variables.KEY_ACCESS_TOKEN)
                    val group_privacy = result.getString(JsonKeys.variables.KEY_GROUP_PRIVACY)


                    UserDataManager.insertFBGroupsData(id,group_id,group_name,picture,access_token,group_privacy)



                }


            } catch(e: Exception) {

                Log.d("JsonResponse", e.message)

            }


        },
                Response.ErrorListener {

                    error ->

               //     Log.d("response", error.message)
                //    Toast.makeText(this, "error", Toast.LENGTH_LONG).show()


                }
        ) {

            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                return Constants.getAuthHeader()
            }
        }


        ApplicationController.instance?.addToRequestQueue(request)
    }
*/

/*
    fun loadLICompanies() {

    }
*/

/*
    fun loadGPCirlces() {
        BuildUrl.setGpCirlcesUrl(login?.getUserID())
        var request = object : JsonArrayRequest(Method.GET, BuildUrl.getGpCirlcesUrl(), null, Response.Listener<JSONArray> {


            response ->

            try {
                Log.d("gp",response.toString())


                //alert(array_fbaccounts.toString()).show()
                for (i in 0..response.length() - 1) {
                    Log.d("url", "in loop")
                    val result = response.getJSONObject(i)
                    val id = result.getInt(JsonKeys.variables.KEY_ID)
                    val gp_circle_id = result.getString(JsonKeys.variables.KEY_GP_CIRCLES_ID)
                    val fb_id = result.getInt(JsonKeys.variables.KEY_FB_ID)
                    val name = result.getString(JsonKeys.variables.KEY_NAME)



                    UserDataManager.insertGPCirlcesData(id,gp_circle_id,fb_id,name)



                }


                Log.d("gp",""+UserDataManager.GPCirclesList?.size)


            } catch(e: Exception) {

                Log.d("JsonResponse", e.message)

            }


        },
                Response.ErrorListener {

                    error ->

            //        Log.d("response", error.message)
             //       Toast.makeText(this, "error", Toast.LENGTH_LONG).show()


                }
        ) {

            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                return Constants.getAuthHeader()
            }
        }


        ApplicationController.instance?.addToRequestQueue(request)
    }
*/

/*
    fun loadGPCollections() {
        BuildUrl.setGpCollectionsUrl(login!!.getUserID())
        Log.d("gp",BuildUrl.getGpCollectionsUrl())

        var request = object : JsonArrayRequest(Method.GET, BuildUrl.getGpCollectionsUrl(), null, Response.Listener<JSONArray> {


            response ->

            try {

                Log.d("gp",response.toString())
                //alert(array_fbaccounts.toString()).show()
                for (i in 0..response.length() - 1) {
                    Log.d("url", "in loop")
                    val result = response.getJSONObject(i)
                    val id = result.getInt(JsonKeys.variables.KEY_ID)
                    val gp_collection_id = result.getString(JsonKeys.variables.KEY_GP_COLLECTIONS_ID)
                    val fb_id = result.getInt(JsonKeys.variables.KEY_FB_ID)
                    val name = result.getString(JsonKeys.variables.KEY_NAME)



                    UserDataManager.insertGPCollectionsData(id,gp_collection_id,fb_id,name)



                }


            } catch(e: Exception) {

                Log.d("JsonResponse", e.message)

            }


        },
                Response.ErrorListener {

                    error ->

//                    Log.d("response", error.message)
               //     Toast.makeText(this, "error", Toast.LENGTH_LONG).show()


                }
        ) {

            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                return Constants.getAuthHeader()
            }
        }


        ApplicationController.instance?.addToRequestQueue(request)
    }
*/

    override fun onItemClick(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {



        this.id = accounts_arraylist?.get(position)?.getId()
        registerForContextMenu(listView_accounts)
        listView_accounts?.showContextMenu()

    }

        override fun onCreateContextMenu(menu: ContextMenu?, v: View?, menuInfo: ContextMenu.ContextMenuInfo?) {
            super.onCreateContextMenu(menu, v, menuInfo)

            if (AccountType == "FB") {
                if(loginManager?.getUserStatus() == true && loginManager?.getAutomationStatus() == true)
                {

                    getMenuInflater().inflate(R.menu.fb_context_without_automation, menu)

                    //Log.d("abc","automation not purchased in if")

                }
                else
                {

                    Log.d("abc","automation  purchased in else")
                    getMenuInflater().inflate(R.menu.fb_context_menu, menu)


                }

            } else if (AccountType == "TW") {

                if(loginManager?.getUserStatus() == true && loginManager?.getAutomationStatus() == true)
                {


                    return
                }
                else
                {

                    Log.d("abc","automation  purchased in else")
                    getMenuInflater().inflate(R.menu.tw_context_menu, menu)


                }

            } else if (AccountType == "LI") {


                if(loginManager?.getUserStatus() == true && loginManager?.getAutomationStatus() == true)
                {


                    getMenuInflater().inflate(R.menu.li_context_without_automation, menu)
                }
                else
                {

                    Log.d("abc","automation  purchased in else")
                    getMenuInflater().inflate(R.menu.li_context_menu, menu)


                }

            } else if (AccountType == "IN") {

                if(loginManager?.getUserStatus() == true && loginManager?.getAutomationStatus() == true)
                {

                    return
                }
                else
                {

                    Log.d("abc","automation  purchased in else")
                    getMenuInflater().inflate(R.menu.in_context_menu, menu)


                }

            } else if (AccountType == "PIN") {

                if(loginManager?.getUserStatus() == true && loginManager?.getAutomationStatus() == true)
                {


                    return
                }
                else
                {

                    Log.d("abc","automation  purchased in else")
                    getMenuInflater().inflate(R.menu.pin_context_menu, menu)


                }

            } else if (AccountType == "GP") {
                if(loginManager?.getUserStatus() == true && loginManager?.getAutomationStatus() == true)
                {


                    getMenuInflater().inflate(R.menu.gp_context_without_automation, menu)
                }
                else
                {

                    Log.d("abc","automation  purchased in else")
                    getMenuInflater().inflate(R.menu.gp_context_menu, menu)


                }
            }
        }

        override fun onContextItemSelected(item: MenuItem?): Boolean {

            // val info = item?.getMenuInfo() as AdapterContextMenuInfo

            when (item?.itemId) {
                R.id.fb_context_pages -> {
                    var pages = Intent(this@MainActivity,AccountsList::class.java)
                    pages.putExtra("TITLE",resources.getString(R.string.string_toolbarTitle_pages))
                    pages.putExtra("ID",id)
                    startActivity(pages)
                    overridePendingTransition(R.anim.enter_horizontal, R.anim.exit_horizontal)
                    return true
                }
                R.id.fb_context_groups ->
                {
                    var groups = Intent(this@MainActivity,AccountsList::class.java)
                    groups.putExtra("TITLE",resources.getString(R.string.string_toolbarTitle_groups))
                    groups.putExtra("ID",id)
                    startActivity(groups)
                    overridePendingTransition(R.anim.enter_horizontal, R.anim.exit_horizontal)
                    return true
                }
                R.id.li_context_companies ->
                {
                    var companies = Intent(this@MainActivity,AccountsList::class.java)
                    companies.putExtra("TITLE",resources.getString(R.string.string_toolbarTitle_companies))
                    companies.putExtra("ID",id)
                    startActivity(companies)
                    overridePendingTransition(R.anim.enter_horizontal, R.anim.exit_horizontal)
                    return true
                }
                R.id.gp_context_circles ->
                {
                    var circles = Intent(this@MainActivity,AccountsList::class.java)
                    circles.putExtra("TITLE",resources.getString(R.string.string_toolbarTitle_circles))
                    circles.putExtra("ID",id)
                    startActivity(circles)
                    overridePendingTransition(R.anim.enter_horizontal, R.anim.exit_horizontal)
                    return true
                }
                R.id.gp_context_collections ->
                {
                    var collections = Intent(this@MainActivity,AccountsList::class.java)
                    collections.putExtra("TITLE",resources.getString(R.string.string_toolbarTitle_collections))
                    collections.putExtra("ID",id)
                    startActivity(collections)
                    overridePendingTransition(R.anim.enter_horizontal, R.anim.exit_horizontal)
                    return true
                }
                R.id.context_auto_posting ->
                {

                    if(AccountType == "FB")
                    {
                        val intent = Intent(this, Categories::class.java)
                        intent.putExtra("FROM","account")
                        intent.putExtra("ID",id)

                        startActivity(intent)
                        overridePendingTransition(R.anim.enter_horizontal, R.anim.exit_horizontal)

                    }
                    else if(AccountType == "TW")
                    {
                        val intent = Intent(this, Categories::class.java)
                        intent.putExtra("FROM","twitter")
                        intent.putExtra("ID",id)

                        startActivity(intent)
                        overridePendingTransition(R.anim.enter_horizontal, R.anim.exit_horizontal)
                    }
                    else if(AccountType == "LI")
                    {
                        val intent = Intent(this, Categories::class.java)
                        intent.putExtra("FROM","linkedin")
                        intent.putExtra("ID",id)

                        startActivity(intent)
                        overridePendingTransition(R.anim.enter_horizontal, R.anim.exit_horizontal)
                    }
                    else if(AccountType == "IN")
                    {
                        val intent = Intent(this, Categories::class.java)
                        intent.putExtra("FROM","ins")
                        intent.putExtra("ID",id)

                        startActivity(intent)
                        overridePendingTransition(R.anim.enter_horizontal, R.anim.exit_horizontal)
                    }
                    else if(AccountType == "PIN")
                    {
                        val intent = Intent(this, Categories::class.java)
                        intent.putExtra("FROM","pin")
                        intent.putExtra("ID",id)

                        startActivity(intent)
                        overridePendingTransition(R.anim.enter_horizontal, R.anim.exit_horizontal)
                    }
                    else if(AccountType == "GP")
                    {
                        val intent = Intent(this, Categories::class.java)
                        intent.putExtra("FROM","gpu")
                        intent.putExtra("ID",id)

                        startActivity(intent)
                        overridePendingTransition(R.anim.enter_horizontal, R.anim.exit_horizontal)
                    }
                    return true


                }
                else -> return super.onContextItemSelected(item)


            }


        }


    override fun onNetworkConnectionChanged(isConnected: Boolean) {


        isInternetConnected = ConnectivityReceiver.isConnected
        if (isConnected) {
            //implement here for connected services
            try {
                //loadAllAccounts()
            } catch(e: Exception) {
            } finally {
            }

        } else if (!isConnected) {
            //implement here for no network services

        }

    }

    private inner class SetThumbnail constructor( thumbnailUrl: String? ,view: ImageView ): AsyncTask<Object, Object, Object>()

    {

        private var thumbnailUrl: String? = null
        private var view: ImageView? = null
        private var bitmap: Bitmap? = null

        init {
            this.thumbnailUrl = thumbnailUrl
            this.view = view

        }


        override fun onPreExecute() {

            Log.d("load","in pre execute")

            try
            {

            }
            catch (e: Exception )
            {
                e.printStackTrace();
            }


            super.onPreExecute()
        }
        override fun doInBackground(vararg params: Object?): Object? {

            try
            {



                Log.d("load","in do in background")


               bitmap = Glide.with(this@MainActivity).load(thumbnailUrl).asBitmap().fitCenter().placeholder(R.drawable.avatar_user).diskCacheStrategy(DiskCacheStrategy.ALL).into(75,75).get()


            }

            catch (e: Exception )
            {
                e.printStackTrace();
            }

            return null;
        }

        override fun onPostExecute(result: Object?) {

            Log.d("load","on post execute")

            try {

                view?.setImageBitmap(bitmap)
            } catch(e: Exception) {
                Log.d("img",""+e.message)
                Log.d("img",e.printStackTrace().toString())

            } finally {
            }




        }

    }

    fun checkPackages()
    {

        BuildUrl.setLoginUrl(loginManager?.getUserName(), loginManager?.getPass())

        Log.d("response",""+BuildUrl?.getLoginUrl())

        var request = object : JsonObjectRequest(Method.GET, BuildUrl.getLoginUrl(), null, Response.Listener<JSONObject> {

            response ->

            try {
                Log.d("response", ""+response.toString())

                //alert(response.getString("ID")).show()



                var products = response.getJSONObject(JsonKeys.objects.KEY_PRODUCTS)

                UserDataManager.user_status = products.getBoolean(JsonKeys.variables.KEY_STATUS)
                UserDataManager.automation_status = products.getBoolean(JsonKeys.variables.KEY_AUTOMATION)
                UserDataManager.content_fetcher_status = products.getBoolean(JsonKeys.variables.KEY_CONTENTS_FETCHER)
                UserDataManager.app_purchase_status = products.getBoolean(JsonKeys.variables.KEY_IOS)


                try {
                    if(UserDataManager?.user_status == false)
                    {

                    }
                    else if(UserDataManager.app_purchase_status == false)
                    {

                        alert("App is not included in your package!").show()

                        Handler().postDelayed(object : Runnable {

                            // Using handler with postDelayed called runnable run method


                            override fun run() {

                                onBackPressed()
                                finish()

                            }

                        }, 3 * 1000)
                    }

                } catch (e: Exception) {
                } finally {
                }


                /*   if(status == true && automation == true)
                   {


                       Log.d("abc","automation not purchased in if")
                   }
                   else
                   {

                       Log.d("abc","automation  purchased in else")

                       autopost = findViewById(R.id.autopost) as ImageView?

                       autopost?.setImageDrawable(resources.getDrawable(R.drawable.autopost_default))
                       autopost?.setOnTouchListener(this@CreatePostActivity)
                       autopost?.setOnClickListener(this@CreatePostActivity)
                   }
*/





            } catch(e: Exception) {

                //var alert = AlertManager("Network Problem!",3000,this)


            }


        },
                Response.ErrorListener {

                    error ->

                    //progressDialog?.dismiss()
                    //var alert = AlertManager("Network Problem!",3000,this)

                    //Toast.makeText(this, "error " +error.message, Toast.LENGTH_LONG).show()


                }
        ) {}

        request.setRetryPolicy(DefaultRetryPolicy(
                25000,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT))

        ApplicationController.instance?.addToRequestQueue(request)
    }

}




