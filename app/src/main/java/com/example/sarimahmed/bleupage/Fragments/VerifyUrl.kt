package com.example.sarimahmed.bleupage.Fragments

import android.app.Fragment
import android.app.ProgressDialog
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import com.android.volley.AuthFailureError
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.example.sarimahmed.bleupage.Activities.ManualContentPosting
import com.example.sarimahmed.bleupage.Adapter.BlogFeedAdapter
import com.example.sarimahmed.bleupage.Application.ApplicationController
import com.example.sarimahmed.bleupage.HelperClasses.AlertManager
import com.example.sarimahmed.bleupage.HelperClasses.BuildUrl
import com.example.sarimahmed.bleupage.HelperClasses.Constants
import com.example.sarimahmed.bleupage.HelperClasses.JsonKeys
import com.example.sarimahmed.bleupage.R
import org.json.JSONObject

/**
 * Created by Averox on 9/18/2017.
 */
public class VerifyUrl: Fragment(),View.OnClickListener
{

    private var button_verify: Button? = null
    private var input_verify_url: EditText? = null
    private var progressDialog: ProgressDialog? = null


    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View {

        var view = inflater?.inflate(R.layout.layout_verifyurl, container, false)

        button_verify = view?.findViewById<Button>(R.id.button_verify_url)
        input_verify_url = view?.findViewById<EditText>(R.id.input_url)
        return view!!
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        button_verify?.setOnClickListener(this)

        input_verify_url?.setSelection(input_verify_url!!.length())

        super.onActivityCreated(savedInstanceState)
    }

    override fun onClick(v: View?) {
        when(v?.id)
        {
            R.id.button_verify_url ->
            {

                var url = input_verify_url?.text.toString().trim()



                if((url.isEmpty() && url.equals("") && url.equals(" ")) || (!url.contains("https://") && !url.contains("http:/")) || url.equals("https://") || url.equals("https:/") || url.equals("http://") || url.equals("http:/"))
                {

                    AlertManager("Please enter a valid url!",5000,activity)

                }
                else
                {
                    verifyUrl(url)
                }

                Log.d("response",input_verify_url?.text.toString())
            }
        }    }

    fun verifyUrl(url: String)
    {



        progressDialog = ProgressDialog.show(activity, "Processing", "Please wait....", true)
        progressDialog?.setCancelable(false)

        var encoded= Uri.encode(url , "UTF-8")
        BuildUrl.setVerifyUrl(encoded)

        Log.d("response", BuildUrl.getVerifyUrl())


        var request = object : JsonObjectRequest(Method.GET, BuildUrl.getVerifyUrl(),null, Response.Listener<JSONObject>

        {

            response ->


            try {
                var status = response.getBoolean(JsonKeys.variables.KEY_STATUS)
                Log.d("response","status"+status)
                Log.d("response","response: "+response.toString())


                if(status.equals("false"))
                {

                    progressDialog?.dismiss()

                    AlertManager("No feeds found check url!",5000,activity)
                }
                else
                {
                    //val intent = Intent(this, GeneralPost::class.java)
                    // intent.putExtra("TITLE",resources.getString(R.string.string_toolbarTitle_generalPost))


                    //startActivity(intent)

                    // overridePendingTransition(R.anim.enter_horizontal, R.anim.exit_horizontal)


                    var records = response.getJSONObject(JsonKeys.objects.KEY_RECORDS)


                    var entries = records.getJSONArray(JsonKeys.arrays.KEY_ENTRIES)
                    var pageTitle = records.getString(JsonKeys.variables.KEY_TITLE)
                    var pageLink = records.getString(JsonKeys.variables.KEY_LINK)
                    var description = records.getString(JsonKeys.variables.KEY_DESCRIPTION)

                    for(i in 0..entries.length() -1)
                    {
                        var result = entries.getJSONObject(i)
                        var title = result.getString(JsonKeys.variables.KEY_TITLE)
                        var content = result.getString(JsonKeys.variables.KEY_CONTENT)
                        var description1 = result.getString(JsonKeys.variables.KEY_DESCRIPTION)

                        var link = result.getString(JsonKeys.variables.KEY_LINK)


                        BlogFeedAdapter.contentList?.add(BlogFeedAdapter(title,link,description1,content))
                    }

                    Log.d("response","response"+entries.toString())

                    progressDialog?.dismiss()

                    val intent = Intent(activity, ManualContentPosting::class.java)
                    intent.putExtra("PAGE_TITLE",pageTitle)
                    intent.putExtra("PAGE_LINK",pageLink)
                    intent.putExtra("DESCRIPTION",description)

                    startActivity(intent)

                    activity.overridePendingTransition(R.anim.enter_horizontal, R.anim.exit_horizontal)



                }
            } catch (e: Exception) {
                progressDialog?.dismiss()
                var alert = AlertManager("Network Problem or check url!",3000,activity)

                Log.d("response","excepton: "+e.message.toString())


            } finally {
            }


        }, Response.ErrorListener {

            error ->

            Log.d("response","error"+error.message.toString())

            var alert = AlertManager("Network Problem or check url!",3000,activity)

            progressDialog?.dismiss()

        }

        ) {

            @Throws(AuthFailureError::class)
            override fun getHeaders(): Map<String, String> {
                return Constants.getAuthHeader()
            }
        }

        request.setRetryPolicy(DefaultRetryPolicy(
                60000,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT))


        ApplicationController.instance?.addToRequestQueue(request)
    }

}