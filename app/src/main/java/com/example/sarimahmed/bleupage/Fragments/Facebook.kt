package com.example.sarimahmed.bleupage.Fragments

import android.app.Fragment
import android.os.Bundle
import android.support.design.widget.TabLayout
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentPagerAdapter
import android.support.v4.view.PagerAdapter
import android.support.v4.view.ViewPager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.example.sarimahmed.bleupage.R

/**
 * Created by Averox on 10/13/2017.
 */

class Facebook: Fragment()
{

    private var tabLayout: TabLayout? = null
    override fun onCreateView(inflater: LayoutInflater?, container: ViewGroup?, savedInstanceState: Bundle?): View {
        var view = inflater?.inflate(R.layout.layout_facebook, container, false)


    return view!!
    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        tabLayout = view.findViewById<TabLayout>(R.id.tab_layout)
        tabLayout?.addTab(tabLayout?.newTab()?.setText("Accounts")!!)
        tabLayout?.addTab(tabLayout?.newTab()?.setText("Pages")!!)
        tabLayout?.addTab(tabLayout?.newTab()?.setText("Groups")!!)

        tabLayout?.tabGravity = TabLayout.GRAVITY_FILL


        var viewPager = view?.findViewById<ViewPager>(R.id.pager)
        var adapter = object: PagerAdapter(){
            override fun isViewFromObject(view: View?, `object`: Any?): Boolean {
return true
            }

            override fun getCount(): Int {
return 0
            }
        }
        viewPager?.setAdapter(adapter)
        viewPager?.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(tabLayout))
        tabLayout?.setOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                viewPager?.currentItem = tab.position
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {

            }

            override fun onTabReselected(tab: TabLayout.Tab) {

            }
        })


    }

    override fun onResume() {
        super.onResume()
    }

    class MyPagerAdapter(fragmentManager: FragmentManager) : FragmentPagerAdapter(fragmentManager) {

        // Returns total number of pages
        override fun getCount(): Int {
            return NUM_ITEMS
        }

        // Returns the fragment to display for that page
        override fun getItem(position: Int): android.support.v4.app.Fragment? {
            when (position) {

            }/* case 0: // Fragment # 0 - This will show FirstFragment
                    return FirstFragment.newInstance(0, "Page # 1");
                case 1: // Fragment # 0 - This will show FirstFragment different title
                    return FirstFragment.newInstance(1, "Page # 2");
                case 2: // Fragment # 1 - This will show SecondFragment
                    return SecondFragment.newInstance(2, "Page # 3");
                default:*/
            return null
        }

        // Returns the page title for the top indicator
        override fun getPageTitle(position: Int): CharSequence {
            return "Page" + position
        }

        companion object {
            private val NUM_ITEMS = 3
        }

    }

}
