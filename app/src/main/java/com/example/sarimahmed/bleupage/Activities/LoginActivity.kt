package com.example.sarimahmed.bleupage.Activities

import android.app.ProgressDialog
import android.content.Intent
import android.content.IntentFilter
import android.net.ConnectivityManager
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.View.OnClickListener
import android.widget.*
import com.android.volley.AuthFailureError
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.example.sarimahmed.bleupage.Application.ApplicationController
import com.example.sarimahmed.bleupage.BroadcastReceivers.ConnectivityReceiver
import com.example.sarimahmed.bleupage.HelperClasses.AlertManager
import com.example.sarimahmed.bleupage.HelperClasses.BuildUrl
import com.example.sarimahmed.bleupage.HelperClasses.Constants
import com.example.sarimahmed.bleupage.HelperClasses.Constants.context
import com.example.sarimahmed.bleupage.HelperClasses.JsonKeys

import com.example.sarimahmed.bleupage.R
import com.example.sarimahmed.bleupage.SessionManager.LoginManager
import org.jetbrains.anko.alert
import org.jetbrains.anko.toast
import org.json.JSONObject

class LoginActivity : AppCompatActivity(), OnClickListener , ConnectivityReceiver.ConnectivityReceiverListener{



    private var editText_login: EditText? = null
    private var editText_password: EditText? = null
    private var button_login: Button? = null
    private var input_username: String? = null
    private var input_password: String? = null
    internal var isInternetConnected = ConnectivityReceiver.isConnected
    private var ConnectionReceiver: ConnectivityReceiver? = null
    private var login: LoginManager? = null
    private var alert: AlertManager? = null
    private var progressDialog: ProgressDialog? = null



    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        //login Session Manager
         login = LoginManager(this.applicationContext)



        editText_login = findViewById(R.id.editText_login) as EditText
        editText_password = findViewById(R.id.editText_password) as EditText
        button_login = findViewById(R.id.button_login) as Button

        button_login!!.setOnClickListener(this)

        val filter = IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION)
        ConnectionReceiver = ConnectivityReceiver()
        try {
            registerReceiver(ConnectionReceiver, filter)
        } catch(e: Exception) {
            Log.d("reciever",e.message)
        } finally {
        }


        if(login!!.isLogedIn())
        {
            val MainActivityIntent = Intent(this, MainActivity::class.java)

            startActivity(MainActivityIntent)

            finish()
            unregisterReceiver(ConnectionReceiver)

        }

    }

    override fun onResume() {
        super.onResume()

        ApplicationController.instance?.setConnectivityListener(this)

    }


    override fun onPause() {
        super.onPause()
        try {
            unregisterReceiver(ConnectionReceiver)
        } catch (e: Exception) {
            Log.d("loginexception",""+e.message.toString())
        } finally {

            Log.d("loginexception",""+"final block")

        }

    }

    override fun onDestroy() {
//        unregisterReceiver(ConnectionReceiver)
        super.onDestroy()
    }



    override fun onBackPressed() {
       /* val a = Intent(Intent.ACTION_MAIN)
        a.addCategory(Intent.CATEGORY_HOME)
        a.flags = Intent.FLAG_ACTIVITY_NEW_TASK
        a.putExtra("EXIT", true)
        startActivity(a)*/
        super.onBackPressed()
    }


    override fun onClick(v: View) {

        val id = v.getId()

        when (id) {
            R.id.button_login -> {

                if (isInternetConnected) {


                     input_username = editText_login!!.text.toString().trim()
                     input_password = editText_password!!.text.toString()

                     if (input_username!!.isEmpty() || input_username!!.equals(" ") || input_password!!.isEmpty() || input_password!!.equals(" ")) {
                         Toast.makeText(this, resources.getString(R.string.string_fill_both_fields), Toast.LENGTH_LONG).show()

                     } else {

                         try {
                             progressDialog = ProgressDialog.show(this@LoginActivity, "Signing in", "Please wait....", true)
                             progressDialog?.setCancelable(false)
                         } catch (e: Exception) {
                         } finally {
                         }

                         BuildUrl.setLoginUrl(input_username!!, input_password!!)
                         Log.d("url", BuildUrl.getLoginUrl())


                         var request = object : JsonObjectRequest(Method.GET, BuildUrl.getLoginUrl(), null, Response.Listener<JSONObject> {

                             response ->

                             try {
                                 //Log.d("response", response.getJSONObject("data").toString())

                                 //alert(response.getString("ID")).show()

                                 if(response.getString("ID").equals("failled"))
                                 {
                                     progressDialog?.dismiss()

                                     //alert("LOGIN FAILED!\nUsername or Password is incorrect").show()
                                     alert = AlertManager(resources.getString(R.string.string_login_failled),5000,this)

                                 }
                                 else {
                                     val result = response.getJSONObject(JsonKeys.objects.KEY_USER_DATA)

                                     var username = result.getString(JsonKeys.variables.KEY_USER_LOGIN)
                                     var password = result.getString(JsonKeys.variables.KEY_USER_PASSWORD)
                                     var id = result.getInt(JsonKeys.objects.KEY_USER_ID)


                                     var products = response.getJSONObject(JsonKeys.objects.KEY_PRODUCTS)

                                     var status = products.getBoolean(JsonKeys.variables.KEY_STATUS)
                                     var automation = products.getBoolean(JsonKeys.variables.KEY_AUTOMATION)
                                     var ios = products.getBoolean(JsonKeys.variables.KEY_IOS)

                                     var contents = products.getBoolean(JsonKeys.variables.KEY_CONTENTS_FETCHER)


                                     var display_name = result.getString(JsonKeys.variables.KEY_USER_DISPLAY_NAME)


                                     //alert(" "+id).show()

                                     try {

                                         if (status == false) {


                                             login!!.createLoginSession(username, password, id, display_name, status, automation,contents, ios)

                                             login!!.savePass(input_password)

                                             if (login!!.isLoggedIn) {
                                                 //progressDialog?.dismiss()

                                                 val intent = Intent(this, MainActivity::class.java)
                                                 startActivity(intent)
                                                 overridePendingTransition(R.anim.fade_in_animation, R.anim.fade_out_animation);

                                                 finish()


                                             } else {
                                                 progressDialog?.dismiss()

                                                 var alert = AlertManager("Network Problem!", 3000, this)

                                             }
                                         } else if (status == true && ios == false) {

                                             progressDialog?.dismiss()


                                             var alert = AlertManager("App Is Not Included In Your Package", 3000, this)

                                         }
                                         else if(status == true && ios == true)
                                         {
                                             login!!.createLoginSession(username, password, id, display_name, status, automation, contents,ios)
                                             login!!.savePass(input_password)

                                             if (login!!.isLoggedIn) {
                                                 //progressDialog?.dismiss()

                                                 val intent = Intent(this, MainActivity::class.java)
                                                 startActivity(intent)
                                                 overridePendingTransition(R.anim.fade_in_animation, R.anim.fade_out_animation);

                                                 finish()


                                             } else {
                                                 progressDialog?.dismiss()

                                                 var alert = AlertManager("Network Problem!", 3000, this)

                                             }
                                         }
                                     }

                                     catch(e: Exception)
                                     {
                                         Log.d("login",e.message)
                                         var alert = AlertManager("Network Problem!",3000,this)
                                         Log.d("response", "error "+e.printStackTrace().toString())

                                     }

                                     /*val intent = Intent(this, CreatePostActivity::class.java)
                                     startActivity(intent)*/


                                 }

                               // alert(result.getString(JsonKeys.variables.KEY_USER_DISPLAY_NAME)).show()
                                // Toast.makeText(this,result.getString(JsonKeys.variables.KEY_USER_DISPLAY_NAME), Toast.LENGTH_LONG).show()

                             } catch(e: Exception) {
                                 progressDialog?.dismiss()

                                 var alert = AlertManager("Network Problem!",3000,this)


                                 Log.d("JsonResponse",""+e.message)
                                 Log.d("response", "error "+e.printStackTrace().toString())

                             }


                         },
                                 Response.ErrorListener {

                                     error ->

                                     //progressDialog?.dismiss()
                                     try {
                                         progressDialog?.dismiss()
                                     } catch (e: Exception) {
                                     } finally {
                                     }
                                     var alert = AlertManager("Network Problem!",3000,this)

                                     Log.d("response", "error "+error.message)
                                     //Toast.makeText(this, "error " +error.message, Toast.LENGTH_LONG).show()


                                 }
                         ) {}

                         request.setRetryPolicy(DefaultRetryPolicy(
                                 25000,
                                 0,
                                 DefaultRetryPolicy.DEFAULT_BACKOFF_MULT))

                         ApplicationController.instance?.addToRequestQueue(request)

                        // progressDialog?.dismiss()



                     }

                }


                else if(!isInternetConnected)
                {
                    toast("Please Check your Internet connection")
                }
            }
        }
    }
    override fun onNetworkConnectionChanged(isConnected: Boolean) {

        isInternetConnected = ConnectivityReceiver.isConnected
        if (isConnected) {
            //implement here for connected services

        } else if (!isConnected) {
            //implement here for no network services

        }
    }
}
