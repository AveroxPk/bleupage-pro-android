package com.example.sarimahmed.bleupage.Activities

import android.app.ProgressDialog
import android.content.Intent
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.widget.*
import com.android.volley.AuthFailureError
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Response
import com.android.volley.toolbox.JsonObjectRequest
import com.android.volley.toolbox.StringRequest
import com.example.sarimahmed.bleupage.Application.ApplicationController
import com.example.sarimahmed.bleupage.HelperClasses.AlertManager
import com.example.sarimahmed.bleupage.HelperClasses.Constants
import com.example.sarimahmed.bleupage.HelperClasses.JsonKeys

import com.example.sarimahmed.bleupage.R
import org.jetbrains.anko.toast

import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*

class Schedule : AppCompatActivity(),View.OnClickListener , CompoundButton.OnCheckedChangeListener{


    private var homeToolbar: Toolbar? = null
    private var toolbar_title: TextView? = null
    private var button_next: Button? = null
    private var listView_days: ListView? = null
    private var days: Array<String>? = null
    private var autopost_switch: Switch? = null
    private var picker: NumberPicker? = null
    private var AutoPostActivated: Int = 0
    private var days_selected: ArrayList<String>? = null
    private var time: Int? = null
    private var serverTime: String? = null
    private var progressDialog: ProgressDialog? = null
    private var days_map: HashMap<String, Int>? = null
    private var type: String? = null







    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_schedule)


        homeToolbar = findViewById(R.id.my_toolbar) as Toolbar
        setSupportActionBar(homeToolbar)
        //supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_user)


        super.setTitle("")



        toolbar_title = findViewById(R.id.toolbar_title_accounts_list) as TextView?



        supportActionBar?.setDefaultDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)


        button_next = findViewById(R.id.button_next) as Button
        button_next?.setOnClickListener(this)

        listView_days = findViewById(R.id.days_listview) as ListView?
        listView_days?.choiceMode = ListView.CHOICE_MODE_MULTIPLE

        days = resources.getStringArray(R.array.days_array)

        picker = findViewById(R.id.timePicker_generalpost) as NumberPicker
        picker?.maxValue = 23

        val strs = resources.getStringArray(R.array.array_time_24hr)
        picker?.displayedValues = strs
        picker?.wrapSelectorWheel = true

        autopost_switch = findViewById(R.id.autopost_switch) as Switch?
        autopost_switch?.setOnCheckedChangeListener(this)




       // var tzid = dz.getShortName(DateTimeUtils.currentTimeMillis())

       // Log.d("localtimezone",""+a)

//tzid will be 'EST'




//longerTimeZoneName  will be 'Eastern Standard Time'






        //Log.d("localtimezone",""+pst.toString())

        //val localTime = date.format(currentLocalTime)


        //var pst1 = DateTime.now().zone

        //Log.d("localtimezone",""+pst1.toString())

        picker?.isEnabled = true
        days_selected = ArrayList()

        days_map = HashMap()

        days_map?.put("Monday",1)
        days_map?.put("Tuesday",2)
        days_map?.put("Wednesday",3)
        days_map?.put("Thursday",4)
        days_map?.put("Friday",5)
        days_map?.put("Saturday",6)
        days_map?.put("Sunday",7)





        var intent = intent
        var extras = intent.extras

        if(extras.getString("FROM") == "createpost")
        {
            button_next?.text = "Next"
        }
        else
        {
            button_next?.text = "Save"
        }

//        Log.d("cat_array",""+extras.getStringArrayList("CATEGORIES").toString())





        val itemsAdapter = ArrayAdapter<String>(this, R.layout.categories_list_item, days)
        listView_days?.adapter = itemsAdapter


        listView_days?.setOnItemClickListener { parent, view, position, id ->

            if(listView_days?.isItemChecked(position) == true)
            {
                days_selected?.add(listView_days?.getItemAtPosition(position) as String)

            }
            else if(listView_days?.isItemChecked(position) == false)
            {
                if(days_selected?.contains(listView_days?.getItemAtPosition(position) as String) == true)
                {
                    days_selected?.remove(listView_days?.getItemAtPosition(position) as String)

                }

            }

        }

        if(extras.getString("FROM") == "createpost") {

            return
        }
        else
        {


            try {

                if(extras.getInt("STATUS") == 1)
                {
                    autopost_switch?.isChecked = true
                    Log.d("response",""+extras.getInt("STATUS"))
                }
                else
                {
                    autopost_switch?.isChecked = false
                    Log.d("response",""+extras.getInt("STATUS"))
                }

                if(extras.getString("TIME").length == 4)
                {
                    time = extras.getString("TIME").substring(0,1).toInt()
                    Log.d("response","time: "+time)

                    Log.d("response","time: "+"in if")

                }
               else
                {
                     time  = extras.getString("TIME").substring(0,2).toInt()
                    Log.d("response","time: "+time)

                    Log.d("response","time: "+"in else")

                }
                Log.d("response"," "+time)

                picker?.value =  time!!

                var temp_arraylist = ArrayList<String>()

                for (i in 0..extras.getIntegerArrayList("POST_ON_DAYS").size - 1) {

                    for (entry in days_map!!.entries) {


                        if (entry.value == extras.getIntegerArrayList("POST_ON_DAYS").get(i)) {
                            temp_arraylist.add(entry.key)
                        }
                    }

                }

                Log.d("response","temp list: "+temp_arraylist.toString())



                for (i in 0..temp_arraylist.size - 1) {

                    for(j in 0..days!!.size -1)
                    {
                        if(listView_days?.getItemAtPosition(j)?.equals(temp_arraylist.get(i)) == true)
                        {

                        }
                        if(listView_days?.getItemAtPosition(j)?.equals(temp_arraylist?.get(i)) == true)
                        {

                            listView_days?.setItemChecked(j,true)
                            days_selected?.add(temp_arraylist?.get(i)!!)



                            //var a =extras.getStringArrayList("DAYS").get(i)
                            //days_array.put(days_map?.get(a))

                        }
                    }


                }
            } catch (e: Exception) {
                Log.d("response","exception: "+e.message.toString())
                Log.d("response","stack trace: "+e.printStackTrace().toString())


            } finally {
            }


        }
    }


    override fun onBackPressed() {

        super.onBackPressed()
        overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left)

    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.getItemId()

        if (id == android.R.id.home) {
            onBackPressed()
            overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left)
            finish()
            return true
        }

        return super.onOptionsItemSelected(item)
    }


    override fun onClick(v: View?) {
        var id = v?.id

        when(id)
        {
            R.id.button_next ->
            {

                var intent = intent
                var extras = intent.extras
                if(extras.getString("FROM") == "createpost")
                {
                    var platform_intent = Intent(this@Schedule,PlatformSelection::class.java)
                    platform_intent.putExtra("POST_TYPE","autopost")
                    platform_intent.putExtra("AUTOPOST_ACTIVATED",AutoPostActivated)
                    platform_intent.putExtra("TIME",getServerTime(picker?.value!!))
                    platform_intent.putExtra("DAYS",days_selected)
                    platform_intent.putExtra("CATEGORIES",extras.getStringArrayList("CATEGORIES"))
                    startActivity(platform_intent)
                    overridePendingTransition(R.anim.enter_horizontal,R.anim.exit_horizontal)
                }
                else
                {

                    saveAutoPostConfig()
                    //Log.d("localtimezone",""+getServerTime(picker?.value!!))


                }



            }
        }

    }

    override fun onCheckedChanged(buttonView: CompoundButton?, isChecked: Boolean) {
        if(isChecked)
        {
            AutoPostActivated = 1
            Log.d("response",""+AutoPostActivated)
        }
        else
        {
            AutoPostActivated = 0
            Log.d("response",""+AutoPostActivated)

        }
    }

    fun saveAutoPostConfig() {


        var days_array = JSONArray()
        var temp_array = ArrayList<String>()

        var intent = intent
        var extras = intent.extras


        var autopost_time = getServerTime(picker?.value!!)
        var type = extras.getString("FROM")
        var id = extras.getInt("ID")

        if(type.equals("account"))
        {
            type = "fb"
        }
        else if(type.equals("twitter"))
        {
            type = "tw"
        }
        else if(type.equals("linkedin"))
        {
            type = "li"
        }


        else if(type.equals("page"))
        {
            type = "pg"
        }
        else if(type.equals("groups"))
        {
            type = "gp"
        }
        else if(type.equals("li"))
        {
            type = "company"
        }
        else if(type.equals("gpc"))
        {
            type = "gpc"
        }
        else if(type.equals("gpcl"))
        {
            type = "gpcl"
        }

            progressDialog = ProgressDialog.show(this@Schedule, "Saving Configuration....", "Please wait", true)
            progressDialog?.setCancelable(false)

            var request = object : JsonObjectRequest(Method.POST, Constants.URL_AUTO_POST,null, Response.Listener<JSONObject> {

                response ->

                try
                {

                    Log.d("response",""+response.toString())

                    var status = response.getString(JsonKeys.variables.KEY_STATUS)
                    Log.d("response",""+status)

                    if(status.equals("success"))
                    {
                        progressDialog?.dismiss()

                        var alert = AlertManager("Configuration saved successfully",5000,this)
                    }
                    else
                    {
                        progressDialog?.dismiss()

                        var alert = AlertManager("Network problem!",5000,this@Schedule)
                    }

                }
                catch(e: Exception)
                {
                    var alert = AlertManager("Network problem!",5000,this@Schedule)

                }
                Log.d("response","")

            },
                    Response.ErrorListener { error ->

                        Log.d("response",""+error.message.toString())

                        progressDialog?.dismiss()
                        var alert = AlertManager("Network problem!",5000,this@Schedule)


                    }) {

                override fun getBody(): ByteArray {

                    var accountarray = JSONArray()
                    var cat_array = JSONArray()




                    for (i in 0..days_selected!!.size - 1) {
                        if(days_map?.containsKey(days_selected?.get(i)) == true)
                        {
                            var a =days_selected?.get(i)
                            days_array.put(days_map?.get(a))

                        }

                    }

                    for(i in 0..extras.getStringArrayList("CATEGORIES").size -1)
                    {
                        cat_array.put(extras.getStringArrayList("CATEGORIES").get(i))
                    }

                    accountarray.put(type+":"+id)

                    var jsonObject = JSONObject()

                    var body: String? = null
                    Log.d("post", "In get Body")

                    try {

                        jsonObject.put("status",AutoPostActivated)
                        jsonObject.put("time",autopost_time)
                        jsonObject.put("post_on_days",days_array)
                        jsonObject.put("cat_name",cat_array)
                        jsonObject.put("accounts",accountarray)

                        body = jsonObject.toString()

                        Log.d("autopost", "json object string: " + jsonObject.toString())
                        //Log.d("autpost", "json object bytearray: " + jsonObject.toString().toByteArray())

                    }
                    catch (e: JSONException)
                    {

                        Log.d("autpostpost",""+ e.message)
                        var alert = AlertManager("Network problem!",5000,this@Schedule)


                    }



                    return jsonObject.toString().toByteArray()
                }

                @Throws(AuthFailureError::class)
                override fun getHeaders(): Map<String, String> {
                    return Constants.getAuthHeader()
                }


            }

            request.setRetryPolicy(DefaultRetryPolicy(
                    25000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT))


            val addToRequestQueue = ApplicationController.instance?.addToRequestQueue(request)

        }

    fun getServerTime(time: Int): String
    {



        var serverTimeZone = TimeZone.getTimeZone("US/Arizona")

        val TimeZoneOffset = serverTimeZone.getRawOffset() / (60 * 1000)
        var serverhrs = TimeZoneOffset / 60


        if(serverhrs < 0)
        {
            serverhrs =  serverhrs * (-1)
        }

        Log.d("localtimezone",""+serverhrs)


        var localTimeZone = TimeZone.getDefault()

        val TimeZoneOffset1 = localTimeZone.getRawOffset() / (60 * 1000)
        val localhrs1 = TimeZoneOffset1 / 60

        Log.d("localtimezone",""+localhrs1)


        var result = serverhrs + localhrs1

        if(result <0)
        {
            var finalresult = time + result
            if(finalresult < 0)
            {
                finalresult = finalresult * (-1)
            }
            serverTime = ""+finalresult
            return serverTime!!+":00"
        }
        else
        {
            var finalresult = time.minus(result)
            if(finalresult < 0)
            {
                finalresult = finalresult * (-1)
            }
            serverTime = ""+finalresult

            return serverTime!!+":00"

        }
    }


}
