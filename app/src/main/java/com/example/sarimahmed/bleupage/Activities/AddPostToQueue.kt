package com.example.sarimahmed.bleupage.Activities

import android.app.Activity
import android.app.ProgressDialog
import android.content.Context
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.AsyncTask
import android.os.Build
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.util.Base64
import android.util.Log
import android.view.ContextMenu
import android.view.MenuItem
import android.view.View
import android.widget.*
import com.android.volley.AuthFailureError
import com.android.volley.DefaultRetryPolicy
import com.android.volley.Response
import com.android.volley.toolbox.JsonArrayRequest
import com.android.volley.toolbox.JsonObjectRequest
import com.bumptech.glide.Glide
import com.example.sarimahmed.bleupage.Adapter.BlogFeedAdapter
import com.example.sarimahmed.bleupage.Adapter.QueueAdapter
import com.example.sarimahmed.bleupage.Application.ApplicationController
import com.example.sarimahmed.bleupage.HelperClasses.*

import com.example.sarimahmed.bleupage.R
import kotlinx.android.synthetic.main.activity_add_post_to_queue.*
import org.jetbrains.anko.toast
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.io.ByteArrayOutputStream
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.*

class AddPostToQueue : AppCompatActivity(),View.OnClickListener {



    private var spinner_queue: Spinner? = null
    private var input_content_description: EditText? = null
    private var button_add_image: LinearLayout? = null
    private var queue_id: Int? = null
    private lateinit var marshMallowPermission: MarshMallowPermission
    private var RESULT_LOAD_CAMERA = 0
    private var RESULT_LOAD_IMAGE = 1
    private var button_remove_image: ImageButton? = null
    private var imageview_add_image: ImageView? = null
    private var imageFileuri: Uri? = null
    private var bitmap_object: Bitmap? = null
    private var imageFile: File? = null
    private var bitmap: Bitmap? = null
    private var filename: String? = null
    private var base64_string:  String = "#"
    private var link: String? = null
    private var progressDialog: ProgressDialog? = null
    private var button_add:  Button? = null
    private var button_back:  Button? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_add_post_to_queue)

        spinner_queue = findViewById(R.id.spinner_queue) as Spinner?
        input_content_description = findViewById(R.id.input_content_description) as EditText?
        button_add_image = findViewById(R.id.linearlayout_add_image) as LinearLayout?
        button_add_image?.setOnClickListener(this)

        button_remove_image = findViewById(R.id.button_remove_image) as ImageButton?
        button_remove_image?.setOnClickListener(this)
        button_add = findViewById(R.id.button_add) as Button?
        button_add?.setOnClickListener(this)
        button_back = findViewById(R.id.button_back) as Button?
        button_back?.setOnClickListener(this)
        imageview_add_image = findViewById(R.id.imageview_add_image) as ImageView?

        if(imageview_add_image?.drawable == null)
        {
            button_remove_image?.visibility = View.GONE
            button_remove_image?.isClickable = false
        }

        marshMallowPermission = MarshMallowPermission(this)

        spinner_queue?.onItemSelectedListener = object: AdapterView.OnItemSelectedListener
        {
            override fun onNothingSelected(p0: AdapterView<*>?) {
                TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
            }

            override fun onItemSelected(p0: AdapterView<*>?, p1: View?, p2: Int, p3: Long) {

                var v = spinner_queue?.getSelectedView()
                (v as TextView).setTextColor(resources.getColor(R.color.smokeWhite))
                try {
                    queue_id = QueueAdapter.queueList?.get(spinner_queue?.selectedItemPosition?.minus(1) as Int)?.queue_id
                } catch (e: Exception) {
                } finally {
                }            }

        }

        loadQueues()

    }

    override fun onBackPressed() {
        super.onBackPressed()
        overridePendingTransition(R.anim.top_to_bottom, R.anim.bottom_to_top)

    }

    override fun onResume() {

        var intent = intent
        var extras = intent.extras


        try
        {
            var content = extras.getString("BLOG_CONTENT")
            input_content_description?.setText(content)
            link = extras.getString("BLOG_LINK")


        }
        catch (e: Exception)
        {

        }
        finally
        {

        }


        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (!marshMallowPermission.checkPermissionForCamera()) {
                    marshMallowPermission.requestPermissionForCamera()

                } else if (!marshMallowPermission.checkPermissionForExternalStorage()) {
                    marshMallowPermission.requestPermissionForExternalStorage()
                }
            }
        } catch (e: Exception) {
        } finally {
        }

        super.onResume()
    }


    override fun onClick(v: View?) {
    when(v?.id) {
        R.id.linearlayout_add_image -> {

            registerForContextMenu(button_add_image)
            button_add_image?.showContextMenu()

        }
        R.id.button_remove_image -> {
            try {
                imageview_add_image?.setImageBitmap(null)
                imageview_add_image?.setImageDrawable(null)

                try {
                    /*imageview_add_image?.setImageResource(0);
                    imageview_add_image?.destroyDrawingCache()*/
                    Glide.get(applicationContext).clearMemory()


                } catch (e: Exception) {
                    Log.d("img", e.message)
                    Log.d("img", e.printStackTrace().toString())

                } finally {
                }


                if (imageview_add_image?.drawable == null) {
                    button_remove_image?.visibility = View.GONE
                    button_remove_image?.isClickable = false
                    imageview_add_image?.background = resources.getDrawable(R.drawable.add_image)
                    button_add_image?.isClickable = true
                    button_add_image?.isEnabled = true

                    filename = null
                } else {
                    Log.d("drawable", "drawable is not null")
                }


            } catch (e: Exception) {
            } finally {
            }
        }
        R.id.button_add ->
        {
            if(spinner_queue?.selectedItemPosition != 0)
            {
                AddPostToQueue()
            }
            else
            {
                toast("Select a Queue")
            }
        }
        R.id.button_back->
        {
            onBackPressed()
        }
    }

    }

    override fun onCreateContextMenu(menu: ContextMenu?, v: View?, menuInfo: ContextMenu.ContextMenuInfo?) {
        super.onCreateContextMenu(menu, v, menuInfo)

            getMenuInflater().inflate(R.menu.add_image_menu, menu)

    }

    override fun onContextItemSelected(item: MenuItem?): Boolean {

        // val info = item?.getMenuInfo() as AdapterContextMenuInfo

        when (item?.itemId) {
            R.id.context_camera -> {
                openCamera()
                return true
            }

            R.id.context_gallery ->
            {
                openGallery()
                return true
            }
            else -> return super.onContextItemSelected(item)
        }
    }


    fun openCamera() {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!marshMallowPermission.checkPermissionForCamera()) {
                marshMallowPermission.requestPermissionForCamera()

            }
            else if (!marshMallowPermission.checkPermissionForExternalStorage()) {
                marshMallowPermission.requestPermissionForExternalStorage()
            }
            else {
                /* if (!marshMallowPermission.checkPermissionForExternalStorage()) {
                     marshMallowPermission.requestPermissionForExternalStorage()
                 }*/
                val takePictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                val mediaStorageDir = File(
                        Environment.getExternalStorageDirectory().toString()
                                + File.separator
                                + "abc"
                                + File.separator
                                + "abc"
                )

                if (!mediaStorageDir.exists()) {
                    mediaStorageDir.mkdirs()
                }

                val timeStamp = SimpleDateFormat("yyyyMMdd_HHmmss",
                        Locale.getDefault()).format(Date())
                try {
                    imageFile = File.createTempFile(
                            "IMG_" + timeStamp, /* prefix */
                            ".jpg", /* suffix */
                            mediaStorageDir      /* directory */
                    )
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(imageFile))
                    startActivityForResult(takePictureIntent, RESULT_LOAD_CAMERA)
                } catch (e: IOException) {
                    Log.d("camera",e.message)
                    Log.d("camera",e.printStackTrace().toString())
                }
                finally
                {
                    Log.d("camera","final")
                }
            }
        }
        else
        {
            val camera_intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            imageFile = File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "test.jpg")
            imageFileuri = Uri.fromFile(imageFile)
            camera_intent.putExtra(MediaStore.EXTRA_OUTPUT, imageFileuri)
            camera_intent.putExtra(MediaStore.EXTRA_VIDEO_QUALITY, 1)
            try {
                if (camera_intent.resolveActivity(getPackageManager()) != null) {

                    startActivityForResult(camera_intent, RESULT_LOAD_CAMERA)
                } else {
                    Log.d("Imagelog", "package manager null")
                }
            } catch (e: Exception) {
                e.printStackTrace()
                Log.d("Camera Activity", e.message)

            } finally {
                Log.d("Camera Activity", "final")

            }
        }
    }



    fun openGallery() {

        if (!marshMallowPermission.checkPermissionForExternalStorage()) {
            marshMallowPermission.requestPermissionForExternalStorage()
        }
        else {
            val gallery_intent = Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
            startActivityForResult(gallery_intent, RESULT_LOAD_IMAGE)
        }

    }




    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == 0) {
            when (resultCode) {
                Activity.RESULT_OK -> if (imageFile?.exists() == true) {

                    if(bitmap_object != null)
                    {
                        bitmap_object = null

                    }
                    else if(bitmap != null)
                    {
                        bitmap = null
                    }
                    bitmap_object = scaleBitmapDown(BitmapFactory.decodeFile(imageFile?.getAbsolutePath()), 1200)



                    //imageview_add_image?.setImageBitmap(bitmap_object)


                    /*Glide.with(this@GeneralPost)
                            .load(Uri.fromFile(imageFile))
                            .diskCacheStrategy(DiskCacheStrategy.NONE).skipMemoryCache(true)
                            .into(imageview_add_image)*/


                    var load = SetImageFromBitmap(bitmap_object!!, imageview_add_image!!)
                    load.execute()
                    //imageview_add_image?.setImageBitmap(bitmap_object)

                    filename = "bitmap.png"


                    //imageview_add_image?.setImageBitmap(bitmap_object)
                    //picture_string = getBase64(bitmap_object!!)!!
                    var stream = this.openFileOutput(filename, Context.MODE_PRIVATE)
                    bitmap_object?.compress(Bitmap.CompressFormat.PNG, 100, stream)

                    //Cleanup
                    stream.close()
                    // bitmap_object?.recycle();



                    //Log.d("Imagelog", "base 64: "+getBase64(bitmap_object!!))
                    //Toast.makeText(this,"Picture was saved at "+imageFile.getAbsolutePath(),Toast.LENGTH_LONG).show();
                    //Log.d("Imagelog", "Picture was saved at " + imageFile?.getAbsolutePath())
                }
                else
                {
                    Toast.makeText(this, "Error saving Picture", Toast.LENGTH_LONG).show()
                    Log.d("Imagelog", "Error saving Picture")
                }
                Activity.RESULT_CANCELED ->
                    //Toast.makeText(this,"Result canceled",Toast.LENGTH_LONG).show();
                    Log.d("Imagelog", "Result canceled")
            }
        } else if (requestCode == RESULT_LOAD_IMAGE) {
            when (resultCode) {
                Activity.RESULT_OK -> {
                    val selectedImage = data?.data
                    val filePathColumn = arrayOf(MediaStore.Images.Media.DATA)

                    val cursor = contentResolver.query(selectedImage, filePathColumn, null, null, null)
                    cursor!!.moveToFirst()
                    val columnIndex = cursor.getColumnIndex(filePathColumn[0])
                    var picturePath = cursor.getString(columnIndex)
                    cursor.close()


                    // imageByteArray = convertToByteArray(bitmap_object!!)
                    if(bitmap_object != null)
                    {
                        bitmap_object = null

                    }
                    else if(bitmap != null)
                    {
                        bitmap = null
                    }


                    bitmap_object = scaleBitmapDown(BitmapFactory.decodeFile(picturePath),1200)


                    //imageview_add_image?.setImageBitmap(bitmap_object)

                    var load = SetImageFromBitmap(bitmap_object!!, imageview_add_image!!)
                    load.execute()

                    filename = "bitmap.png"



                    //Write file
                    var stream = this.openFileOutput(filename, Context.MODE_PRIVATE);
                    bitmap_object?.compress(Bitmap.CompressFormat.PNG, 100, stream);

                    //Cleanup
                    stream.close();
                    //bitmap_object?.recycle();







                    //picture_string = getBase64(compressBitmap(bitmap_object!!))


                    //Log.d("Imagelog", "base 64 image load: "+getBase64(bitmap_object!!))



                    // Log.d("Imagelog",imageview_add_image?.toString())

                    //imageview_add_image?.setImageBitmap(compressBitmap(bitmap_object!!))
                    //Toast.makeText(getApplicationContext(),picturePath,Toast.LENGTH_LONG).show();
                }
            }


        }
    }

    fun scaleBitmapDown(bitmap: Bitmap, maxDimension: Int): Bitmap {
        val originalWidth = bitmap.width
        val originalHeight = bitmap.height
        var resizedWidth = maxDimension
        var resizedHeight = maxDimension

        if (originalHeight > originalWidth) {
            resizedHeight = maxDimension
            resizedWidth = (resizedHeight * originalWidth.toFloat() / originalHeight.toFloat()).toInt()
        } else if (originalWidth > originalHeight) {
            resizedWidth = maxDimension
            resizedHeight = (resizedWidth * originalHeight.toFloat() / originalWidth.toFloat()).toInt()
        } else if (originalHeight == originalWidth) {
            resizedHeight = maxDimension
            resizedWidth = maxDimension
        }
        val a = "Width: $resizedWidth\nHeight: $resizedHeight"
        Log.d("imagedimen", a)
        return Bitmap.createScaledBitmap(bitmap, resizedWidth, resizedHeight, false)
    }

    fun loadQueues()
    {
        var temp_array = ArrayList<String>()

        temp_array.add("No Queue Selected")

        var spinnerArrayAdapter = ArrayAdapter(this, R.layout.support_simple_spinner_dropdown_item, temp_array)

        spinner_queue?.adapter = spinnerArrayAdapter

        spinner_queue?.setSelection(0, true)

        var v = spinner_queue?.getSelectedView()
        (v as TextView).setTextColor(resources.getColor(R.color.smokeWhite))

        for(i in 0..QueueAdapter?.queueList!!.size - 1) {
            temp_array.add(QueueAdapter.queueList?.get(i)?.queue_name!!)
        }


        spinnerArrayAdapter = ArrayAdapter(this, R.layout.support_simple_spinner_dropdown_item, temp_array)

        spinner_queue?.adapter = spinnerArrayAdapter

        spinner_queue?.setSelection(0, true)


        (v as TextView).setTextColor(resources.getColor(R.color.smokeWhite))

    }



    private inner class SetImageFromBitmap(bitmap: Bitmap,view: ImageView): AsyncTask<Void, Void, Void>()
    {


        private var bitmap: Bitmap? = null
        private var view: ImageView? = null


        init {
            this.bitmap = bitmap
            this.view = view

            try {
                Glide.get(applicationContext).bitmapPool.clearMemory()
            } catch(e: Exception) {
            } finally {
            }

        }
        override fun onPreExecute() {
            try {
                (imageview_add_image?.setImageBitmap(bitmap))
            } catch(e: Exception) {
                Log.d("img",e.message)
                Log.d("img",e.printStackTrace().toString())

            } finally {
            }
            super.onPreExecute()
        }

        override fun doInBackground(vararg params: Void?): Void? {
            try
            {

                Log.d("load","in do in background")

            }
            catch (e: Exception )
            {
                e.printStackTrace();
            }
            return null
        }

        override fun onPostExecute(result: Void?) {
            imageview_add_image?.background = null
            button_remove_image?.visibility = View.VISIBLE
            button_remove_image?.isClickable = true
            button_add_image?.isClickable = false
            button_add_image?.isEnabled = false
            base64_string = getBase64(bitmap!!)


            Log.d("post",""+bitmap_object.toString())

            super.onPostExecute(result)
        }

    }
    fun getBase64(bitmap: Bitmap): String {
        val outputStream = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, outputStream)

        return Base64.encodeToString(outputStream.toByteArray(), Base64.DEFAULT)
    }

    fun AddPostToQueue()
    {

        var decription = input_content_description?.text.toString()
            try {

                progressDialog = ProgressDialog.show(this, "Saving Configuration", "Please wait....", true)
                progressDialog?.setCancelable(false)

                var request = object : JsonObjectRequest(Method.POST, Constants.URL_ADD_POST_TO_QUEUE, null, Response.Listener<JSONObject> {

                    response ->
                    try {
                        Log.d("response", "response:  " + response.toString())

                        var status = response.getString(JsonKeys.variables.KEY_STATUS)
                        //Log.d("response",""+status)

                        if(status.equals("true"))
                        {
                            progressDialog?.dismiss()


                            var alert = AlertManager("Post Added to Queue Successfully",5000,this)



                        }
                        else
                        {
                            progressDialog?.dismiss()

                            var alert = AlertManager("Problem Adding Post to Queue!",5000,this)
                        }


                        //Log.d("responseaccounts",""+queues_arraylist?.get(1).toString())
                        //listview_queues?.adapter = QueuesListAdapter
                        //QueuesListAdapter.notifyDataSetChanged()


                    } catch (e: Exception) {
                        Log.d("response", "" + e.message)
                        Log.d("response", "" + e.printStackTrace().toString())
                        progressDialog?.dismiss()

                    } finally {

                    }


                },
                        Response.ErrorListener {

                            error ->
                            Log.d("response", "error : " + error.message.toString())
                            Log.d("response", "error : " + error.printStackTrace().toString())

                            progressDialog?.dismiss()


                        }) {


                    override fun getBody(): ByteArray {

                        var jsonObject = JSONObject()



                        var body: String? = null
                        Log.d("queue", "In get Body")

                        try {


                            jsonObject.put("description", decription)
                            jsonObject.put("link", link)

                            jsonObject.put("queueid",queue_id)

                                jsonObject.put("image", base64_string)




                            body = jsonObject.toString()

                            Log.d("queue", "json object string: " + jsonObject.toString())
                            //Log.d("autpost", "json object bytearray: " + jsonObject.toString().toByteArray())

                        } catch (e: JSONException) {

                            Log.d("queue", e.message)
                            var alert = AlertManager("Network problem!", 5000,Constants.context!!)


                        }



                        return jsonObject.toString().toByteArray()
                    }

                    @Throws(AuthFailureError::class)
                    override fun getHeaders(): Map<String, String> {
                        return Constants.getAuthHeader()
                    }
                }

                request.setRetryPolicy(DefaultRetryPolicy(
                        30000,
                        0,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT))

                ApplicationController.instance?.addToRequestQueue(request)
            } catch (e: Exception) {

                Log.d("queue",""+e.message.toString())
            } finally {
            }


    }
    }


