package com.example.sarimahmed.bleupage.SessionManager

import android.content.Context
import android.content.SharedPreferences
import com.example.sarimahmed.bleupage.Adapter.AccountsDataAdapter

/**
 * Created by Averox on 7/20/2017.
 */
class UserDataManager {




        companion object {

            public var FBAccountsList: ArrayList<AccountsDataAdapter>? = null
            public var FBListForPost: ArrayList<AccountsDataAdapter>? = null

            public var TWAccountsList: ArrayList<AccountsDataAdapter>? = null
            public var LIListForPost: ArrayList<AccountsDataAdapter>? = null

            public var LIAccountsList: ArrayList<AccountsDataAdapter>? = null
            public var LICompanyList: ArrayList<AccountsDataAdapter>? = null
            public var INAccountsList: ArrayList<AccountsDataAdapter>? = null
            public var PINAccountsList: ArrayList<AccountsDataAdapter>? = null
            public var GPAccountsList: ArrayList<AccountsDataAdapter>? = null
            public var GPListForPost: ArrayList<AccountsDataAdapter>? = null

            public var FBPagesList: ArrayList<AccountsDataAdapter>? = null
            public var FBGroupList: ArrayList<AccountsDataAdapter>? = null
            public var GPCirclesList: ArrayList<AccountsDataAdapter>? = null
            public var GPCollectionsList: ArrayList<AccountsDataAdapter>? = null
            public var automation_status: Boolean? = null
            public var content_fetcher_status: Boolean? = null
            public var app_purchase_status: Boolean? = null
            public var user_status: Boolean? = null

            init {
                FBAccountsList = ArrayList<AccountsDataAdapter>()
                FBListForPost = ArrayList<AccountsDataAdapter>()
                TWAccountsList = ArrayList<AccountsDataAdapter>()
                LIAccountsList = ArrayList<AccountsDataAdapter>()
                LICompanyList = ArrayList<AccountsDataAdapter>()
                LIListForPost = ArrayList<AccountsDataAdapter>()
                INAccountsList = ArrayList<AccountsDataAdapter>()
                PINAccountsList = ArrayList<AccountsDataAdapter>()
                GPAccountsList = ArrayList<AccountsDataAdapter>()
                GPListForPost = ArrayList<AccountsDataAdapter>()
                FBPagesList = ArrayList<AccountsDataAdapter>()
                FBGroupList = ArrayList<AccountsDataAdapter>()
                GPCirclesList = ArrayList<AccountsDataAdapter>()
                GPCollectionsList = ArrayList<AccountsDataAdapter>()
            }

        fun insertFBData(id: Int?, fb_id: Int?, name: String?, picture: String?, access_token: String?, pages_count: Int?)
        {

            (FBAccountsList as ArrayList<AccountsDataAdapter>).add(AccountsDataAdapter(id, fb_id, name, picture, access_token, pages_count))
        }


        fun insertTWData(id: Int?, fb_id: Int?, username: String?, display_name: String?, picture: String?)
        {

            (TWAccountsList as ArrayList<AccountsDataAdapter>).add(AccountsDataAdapter(id, fb_id, username, display_name, picture))
        }


        fun insertLIData(fb_id: Int?, username: String?, id: Int?, token: String?, display_name: String?, picture: String?)
        {

            (LIAccountsList as ArrayList<AccountsDataAdapter>).add(AccountsDataAdapter(fb_id,username,id,token,display_name,picture))
        }

            fun insertLICompany(id: Int?, linkedin_id: Int?,company_id: Int?, username: String?,  picture: String?, website_url: String?,token: String?)
            {
                (LICompanyList as ArrayList<AccountsDataAdapter>).add(AccountsDataAdapter(id, linkedin_id, company_id,username, picture,website_url,token))
            }


        fun insertINData(fb_id: Int?, username: String?, password: String?, id: Int?, display_name: String?, picture: String?)
        {

            (INAccountsList as ArrayList<AccountsDataAdapter>).add(AccountsDataAdapter(fb_id, username, password, id, display_name, picture))
        }


        fun insertPINData(fb_id: Int?, username: String?, id: Int?, token: String?, display_name: String?, picture: String?)
        {

            (PINAccountsList as ArrayList<AccountsDataAdapter>).add(AccountsDataAdapter(fb_id, username, id, token, display_name, picture))
        }


            fun insertGPData(fb_id: Int?, username: String?, id: Int?, token: String?, display_name: String?, picture: String?)
        {

            (GPAccountsList as ArrayList<AccountsDataAdapter>).add(AccountsDataAdapter(fb_id, username, id, token, display_name, picture))
        }
            fun insertFBPagesData(id: Int?, fb_id: Int?, name: String?, picture: String?, access_token: String?, posts_count: Int?)
            {

                (FBPagesList as ArrayList<AccountsDataAdapter>).add(AccountsDataAdapter(id, fb_id, name, picture, access_token, posts_count))
            }
            fun insertFBGroupsData(id: Int?, fb_id: Int?, group_name: String?, picture: String?, access_token: String?, group_privacy: String?)
            {

                (FBGroupList as ArrayList<AccountsDataAdapter>).add(AccountsDataAdapter(id, fb_id, group_name, picture, access_token, group_privacy))
            }
            fun insertGPCirlcesData(id: Int?, gp_circle_id: String?, fb_id: Int?, name: String?)
            {

                (GPCirclesList as ArrayList<AccountsDataAdapter>).add(AccountsDataAdapter(id, gp_circle_id, fb_id, name))
            }

            fun insertGPCollectionsData(id: Int?, gp_collection_id: String?, fb_id: Int?, name: String?,picture: String?)
            {

                (GPCollectionsList as ArrayList<AccountsDataAdapter>).add(AccountsDataAdapter(id, gp_collection_id, fb_id, name,picture))
            }

            fun insertFBDataForPost(id: String?,name: String?)
            {
                (FBListForPost as ArrayList<AccountsDataAdapter>).add(AccountsDataAdapter(id,name))
            }
            fun insertLIDataForPost(id: String?,company_id: Int?,name: String?,token: String?)
            {
                (LIListForPost as ArrayList<AccountsDataAdapter>).add(AccountsDataAdapter(id,company_id,name,token))

            }
            fun insertGPDataForPost(id: String?,name: String?)
            {
                (GPListForPost as ArrayList<AccountsDataAdapter>).add(AccountsDataAdapter(id,name))

            }


        }




}