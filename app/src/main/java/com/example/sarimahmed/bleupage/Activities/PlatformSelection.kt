package com.example.sarimahmed.bleupage.Activities

import android.app.ProgressDialog
import android.content.Context
import android.content.IntentFilter
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.Toolbar
import android.util.Log
import android.view.*
import android.widget.*
import com.android.volley.toolbox.StringRequest
import com.example.sarimahmed.bleupage.Application.ApplicationController
import com.example.sarimahmed.bleupage.HelperClasses.*

import com.example.sarimahmed.bleupage.R
import org.jetbrains.anko.getStackTraceString
import org.jetbrains.anko.toast
import org.json.JSONArray
import org.json.JSONException
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList
import android.graphics.BitmapFactory
import android.graphics.Bitmap
import android.net.ConnectivityManager
import android.util.Base64
import com.android.volley.*
import com.android.volley.toolbox.JsonObjectRequest
import com.example.sarimahmed.bleupage.Adapter.AccountsDataAdapter
import com.example.sarimahmed.bleupage.BroadcastReceivers.ConnectivityReceiver
import java.io.ByteArrayOutputStream
import android.widget.LinearLayout
import com.example.sarimahmed.bleupage.Adapter.FBLIGPAccountItem
import com.example.sarimahmed.bleupage.Adapter.OtherAccountITem
import com.example.sarimahmed.bleupage.SessionManager.LoginManager
import com.example.sarimahmed.bleupage.SessionManager.UserDataManager
import kotlinx.android.synthetic.main.section_header.*


class PlatformSelection : AppCompatActivity(), View.OnClickListener, ConnectivityReceiver.ConnectivityReceiverListener{



    private var homeToolbar: Toolbar? = null
    private var button_post: Button? = null
    private var AccountType: String = "FB"
    private var ic_facebook: ImageButton? = null
    private var ic_twitter: ImageButton? = null
    private var ic_linkedin: ImageButton? = null
    private var ic_instagram: ImageButton? = null
    private var ic_pinterest: ImageButton? = null
    private var ic_googleplus: ImageButton? = null
    private var listview_accounts: ListView? = null
    private var listview_pages_companies_circles: ListView? = null
    private var listview_groups_collections: ListView? = null
    private var accounts_arraylist: ArrayList<AccountsDataAdapter>? = null
    private var pages_companies_circles_arraylist: ArrayList<AccountsDataAdapter>? = null
    private var groups_collections_arraylist: ArrayList<AccountsDataAdapter>? = null
    private var textview_groups_collections: TextView? = null
    private var textview_pages_companies_circles: TextView? = null
    private var textview_accounts: TextView? = null
    private var header_accounts_listview: TextView? = null
    private var header_pages_circles_companies_listview: TextView? = null
    private var header_groups_collections_listview: TextView? = null
    private var post_body: EditText? = null
    private var post_body_url: EditText? = null
    private var picture_string: String= ""
    private var FbAccountsIDs: ArrayList<String>? = null
    private var FbPagesIDs: ArrayList<Int>? = null
    private var FbGroupsIDs: ArrayList<Int>? = null
    private var TWAccountsIDs: ArrayList<Int>? = null
    private var LIAccountsIDs: ArrayList<String>? = null
    private var LIAccountsIDsForAutoPost: ArrayList<Int>? = null
    private var LIAccountsIDsForQueue: ArrayList<Int>? = null

    private var LICompaniesIDs: ArrayList<String>? = null
    private var LICompaniesIDsForQueue: ArrayList<Int>? = null
    private var LICompaniesIDsForAutoPost: ArrayList<Int>? = null

    private var INAccountsIDs: ArrayList<Int>? = null
    private var PINAccountsIDs: ArrayList<Int>? = null
    private var GPAccountsIDs: ArrayList<String>? = null
    private var GPCirclesIDs: ArrayList<Int>? = null
    private var GPCollectionsIDs: ArrayList<Int>? = null
    private var GPlusIDs: ArrayList<String>? = null
    private var image_string: String = ""
    private var link: String = ""
    private lateinit var current_time: String
    private var template_url: String= ""
    private var schedule: Int = 0
    private lateinit var posted_on: String
    private var message: String= ""
    private lateinit var calendar: Calendar
    private val df_for_current_time = SimpleDateFormat("yyyy-MM-dd HH")
    private val df_for_schedule_time = SimpleDateFormat("yyyy-MM-dd")
    private lateinit var a: Array<String>
    private var base64: Boolean = false
    private lateinit var bitmap_object: Bitmap
    internal var isInternetConnected = ConnectivityReceiver.isConnected
    private var ConnectionReceiver: ConnectivityReceiver? = null
    private lateinit var imageByteArray: ByteArray
    private var progressDialog: ProgressDialog? = null
    private var post_type: String = ""
    var days_map: HashMap<String,Int>? = null
    private var layout_accounts: LinearLayout? = null
    private var layout_pages: LinearLayout? = null
    private var layout_groups: LinearLayout? = null
    private var title: String = ""

    private var list: ArrayList<Any>? = null









    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_platform_selection)
        homeToolbar = findViewById(R.id.my_toolbar) as Toolbar
        setSupportActionBar(homeToolbar)
        //supportActionBar!!.setHomeAsUpIndicator(R.drawable.ic_user)


        super.setTitle("")

        supportActionBar?.setDefaultDisplayHomeAsUpEnabled(true)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)


        button_post = findViewById(R.id.button_post) as Button?
        button_post?.setOnClickListener(this)
        ic_facebook = findViewById(R.id.ic_facebook) as ImageButton
        ic_facebook!!.setOnClickListener(this)
        ic_twitter = findViewById(R.id.ic_twitter) as ImageButton
        ic_twitter!!.setOnClickListener(this)
        ic_linkedin = findViewById(R.id.ic_linkedin) as ImageButton
        ic_linkedin!!.setOnClickListener(this)
        ic_instagram = findViewById(R.id.ic_instagram) as ImageButton
        ic_instagram!!.setOnClickListener(this)
        ic_pinterest = findViewById(R.id.ic_pinterest) as ImageButton
        ic_pinterest!!.setOnClickListener(this)
        ic_googleplus = findViewById(R.id.ic_googleplus) as ImageButton
        ic_googleplus!!.setOnClickListener(this)

        listview_accounts = findViewById(R.id.listview_accounts) as ListView?
        try {
            listview_accounts?.choiceMode = ListView.CHOICE_MODE_MULTIPLE
        } catch (e: Exception) {
        } finally {
        }

        /* listview_pages_companies_circles = findViewById(R.id.listview_pages_companies_circles) as ListView?
         listview_groups_collections = findViewById(R.id.listview_groups_collections) as ListView?
         listview_pages_companies_circles?.choiceMode = ListView.CHOICE_MODE_MULTIPLE
         listview_groups_collections?.choiceMode = ListView.CHOICE_MODE_MULTIPLE*/








        FbAccountsIDs = ArrayList()
        FbPagesIDs = ArrayList()
        FbGroupsIDs = ArrayList()
        TWAccountsIDs = ArrayList()
        LIAccountsIDs = ArrayList()
        LIAccountsIDsForAutoPost = ArrayList()
        LIAccountsIDsForQueue = ArrayList()
        LICompaniesIDs = ArrayList()
        LICompaniesIDsForAutoPost = ArrayList()
        LICompaniesIDsForQueue = ArrayList()
        INAccountsIDs = ArrayList()
        PINAccountsIDs = ArrayList()
        GPAccountsIDs = ArrayList()
        GPCirclesIDs = ArrayList()
        GPCollectionsIDs = ArrayList()
        GPlusIDs = ArrayList()
        calendar = Calendar.getInstance()
        list = ArrayList()

        //Log.d("localtimezone", "=" + tz.g)


        var intent = intent
        var extras = intent.extras


        //Log.d("localtimezone",localTime.substring(5,6))

        try {

            title = extras.getString("TITLE")
            if(title.equals("edit_queue"))
            {
                var accounts = extras.getStringArrayList("QUEUE_ACCOUNTS")
                for(i in 0..accounts.size - 1)
                {
                    var temp = accounts.get(i)
                    var item = temp.split(":")

                    Log.d("id",""+item[1].toInt())
                    if(item[0].equals("fb"))
                    {
                        var id = temp
                        FbAccountsIDs?.add(id)


                    }
                    else if(item[0].equals("pg"))
                    {
                        var id = temp
                        FbAccountsIDs?.add(id)

                    }
                    else if(item[0].equals("gp"))
                    {
                        var id = temp
                        FbAccountsIDs?.add(id)

                    }



                    else if(item[0].equals("li"))
                    {
                        var id = temp
                        LIAccountsIDs?.add(id)
                    }
                    else if(item[0].equals("lic"))
                    {
                        var id = temp
                        LIAccountsIDs?.add(id)
                    }
                    else if(item[0].equals("tw"))
                    {
                        var id = item[1].toInt()
                        TWAccountsIDs?.add(id)
                    }
                    else if(item[0].equals("gpu"))
                    {
                        var id = temp
                        GPAccountsIDs?.add(id)
                    }
                    else if(item[0].equals("gpc"))
                    {
                        var id = temp
                        GPAccountsIDs?.add(id)
                    }
                    else if(item[0].equals("gpl"))
                    {
                        var id = temp
                        GPAccountsIDs?.add(id)
                    }

                }
            }
        } catch (e: Exception) {

            Log.d("id",""+e.message)
        } finally {
        }

        Log.d("queue",""+extras.getInt("QUEUE_ID"))



        accounts_arraylist = ArrayList<AccountsDataAdapter>()
        pages_companies_circles_arraylist = ArrayList<AccountsDataAdapter>()
        groups_collections_arraylist = ArrayList<AccountsDataAdapter>()


        days_map = HashMap()

        days_map?.put("Monday",1)
        days_map?.put("Tuesday",2)
        days_map?.put("Wednesday",3)
        days_map?.put("Thursday",4)
        days_map?.put("Friday",5)
        days_map?.put("Saturday",6)
        days_map?.put("Sunday",7)





        try {
            post_type = extras.getString("POST_TYPE")
        } catch (e: Exception) {
        } finally {
        }

        if(post_type == "general")
        {
            button_post?.setText("Post")
        }
        else if(post_type == "autopost")
        {
            button_post?.setText("Save")
        }
        else if(post_type.equals("queue"))
        {
            ic_instagram?.visibility = View.GONE
            ic_pinterest?.visibility = View.GONE
            button_post?.setText("Save")

        }



        if(post_type == "general")

        {
            try {

                posted_on=df_for_schedule_time.format(calendar.time)+" "+extras.getString("POSTED_ON")+":00:00"

                if(extras.getBoolean("SCHEDULE") == true)
                {
                    schedule = 1

                }
                else if(extras.getBoolean("SCHEDULE") == false)
                {
                    schedule = 0

                }


                message=extras.getString("POST_BODY")
                //message = Uri.encode(message, "UTF-8")

                link=extras.getString("POST_BODY_URL")
                //link = Uri.encode(link, "UTF-8")

                //image_string=extras.getString("IMAGE_STRING")
                //image_string = Uri.encode(image_string, "UTF-8")

                base64=extras.getBoolean("BASE_64")

                if(base64 == true)
                {

                    var bmp: Bitmap? = null
                    val filename = getIntent().getStringExtra("IMAGE")
                    try {
                        val `is` = this.openFileInput(filename)
                        bmp = BitmapFactory.decodeStream(`is`)
                        `is`.close()
                    } catch (e: Exception) {
                        e.printStackTrace()
                    }
                    image_string = getBase64(bmp!!)

                    /*   bitmap_object = convertToBitmap(image_string)
                       image_string = getBase64(bitmap_object)*/
                }
                else if(base64 == false)
                {
                    template_url = getIntent().getStringExtra("TEMPLATE_URL")
                }


                Log.d("postintent recieve",message+link+image_string+base64)


            } catch(e: Exception) {
                Log.d("postintent recieve",e.message)

            } finally {
                Log.d("postintent recieve","postintent recieve final block")

            }
        }



        val filter = IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION)
        ConnectionReceiver = ConnectivityReceiver()
        try {
            registerReceiver(ConnectionReceiver, filter)
        } catch(e: Exception) {
            Log.d("reciever",e.message)
        } finally {
        }










        loadFBData()

        ic_facebook?.imageTintList = resources.getColorStateList(R.color.white)
        ic_twitter?.imageTintList = resources.getColorStateList(R.color.darkgrey)
        ic_linkedin?.imageTintList = resources.getColorStateList(R.color.darkgrey)
        ic_instagram?.imageTintList = resources.getColorStateList(R.color.darkgrey)
        ic_pinterest?.imageTintList = resources.getColorStateList(R.color.darkgrey)
        ic_googleplus?.imageTintList = resources.getColorStateList(R.color.darkgrey)


        listview_accounts?.setOnItemClickListener { adapterView, view, position, l ->

            when(AccountType)
            {
                "FB" ->
                {
                    if(list?.get(position) is String)
                    {

                    }
                    else
                    {

                        if(listview_accounts?.isItemChecked(position) == true)
                        {
                            //listview_accounts?.setItemChecked(position,false)

                            FbAccountsIDs?.add((list?.get(position) as FBLIGPAccountItem).id!!)


                        }
                        else if(listview_accounts?.isItemChecked(position) == false)
                        {
                            if(FbAccountsIDs?.contains((list?.get(position) as FBLIGPAccountItem).id!!) == true)
                            {
                                FbAccountsIDs?.remove((list?.get(position) as FBLIGPAccountItem).id!!)


                            }
                        }
                    }
                }
                "TW" ->
                {
                    if(list?.get(position) is String)
                    {

                    }
                    else
                    {

                        if(listview_accounts?.isItemChecked(position) == true)
                        {
                            //listview_accounts?.setItemChecked(position,false)

                            TWAccountsIDs?.add((list?.get(position) as OtherAccountITem).id!!)


                        }
                        else if(listview_accounts?.isItemChecked(position) == false)
                        {
                            if(TWAccountsIDs?.contains((list?.get(position) as OtherAccountITem).id!!) == true)
                            {
                                TWAccountsIDs?.remove((list?.get(position) as OtherAccountITem).id!!)


                            }
                        }
                    }
                }
                "LI" ->
                {

                    if(post_type.equals("general")) {


                        Log.d("response","general post")
                        if (list?.get(position) is String) {

                        } else {

                            var idForAccount: String = ""
                            var idForCompany: String = ""

                            var item = (list?.get(position) as FBLIGPAccountItem).id?.split(":")

                            if (item!![0].equals("li")) {
                                idForAccount = (list?.get(position) as FBLIGPAccountItem).id!! + ":" + (list?.get(position) as FBLIGPAccountItem).token!!
                                Log.d("response",""+idForAccount)

                            } else if (item!![0].equals("lic")) {
                                idForCompany = (list?.get(position) as FBLIGPAccountItem).id!! + ":" + (list?.get(position) as FBLIGPAccountItem).company_id!! + ":" + (list?.get(position) as FBLIGPAccountItem).token!!
                                Log.d("response",""+idForCompany)

                            }
                            if (listview_accounts?.isItemChecked(position) == true) {

                                if (item!![0].equals("li")) {
                                    LIAccountsIDs?.add(idForAccount)

                                } else if (item!![0].equals("lic")) {
                                    LIAccountsIDs?.add(idForCompany)

                                }

                                //listview_accounts?.setItemChecked(position,false)


                            } else if (listview_accounts?.isItemChecked(position) == false) {

                                if (item!![0].equals("li")) {
                                    if (LIAccountsIDs?.contains(idForAccount) == true) {
                                        LIAccountsIDs?.remove(idForAccount)


                                    }
                                } else if (item!![0].equals("lic")) {
                                    if (LIAccountsIDs?.contains(idForCompany) == true) {
                                        LIAccountsIDs?.remove(idForCompany)


                                    }
                                }

                            }
                        }
                    }
                    else if(post_type.equals("autopost"))
                    {
                        if (list?.get(position) is String) {

                        } else {

                            var idForAccount: String = ""
                            var idForCompany: String = ""

                            var item = (list?.get(position) as FBLIGPAccountItem).id?.split(":")

                            if (item!![0].equals("li")) {
                                idForAccount = (list?.get(position) as FBLIGPAccountItem).id!!

                            } else if (item!![0].equals("lic")) {
                                idForCompany = "company:"+(list?.get(position) as FBLIGPAccountItem).company_id!!

                            }
                            if (listview_accounts?.isItemChecked(position) == true) {

                                if (item!![0].equals("li")) {
                                    LIAccountsIDs?.add(idForAccount)

                                } else if (item!![0].equals("lic")) {
                                    LIAccountsIDs?.add(idForCompany)

                                }

                                //listview_accounts?.setItemChecked(position,false)


                            } else if (listview_accounts?.isItemChecked(position) == false) {

                                if (item!![0].equals("li")) {
                                    if (LIAccountsIDs?.contains(idForAccount) == true) {
                                        LIAccountsIDs?.remove(idForAccount)


                                    }
                                } else if (item!![0].equals("lic")) {
                                    if (LIAccountsIDs?.contains(idForCompany) == true) {
                                        LIAccountsIDs?.remove(idForCompany)


                                    }
                                }

                            }
                        }
                    }
                    else if(post_type.equals("queue"))
                    {
                        if (list?.get(position) is String) {

                        } else {

                            var idForAccount: String = ""
                            var idForCompany: String = ""

                            var item = (list?.get(position) as FBLIGPAccountItem).id?.split(":")

                            if (item!![0].equals("li")) {
                                idForAccount = (list?.get(position) as FBLIGPAccountItem).id!!

                            } else if (item!![0].equals("lic")) {
                                idForCompany = "lic:"+(list?.get(position) as FBLIGPAccountItem).company_id!!

                            }
                            if (listview_accounts?.isItemChecked(position) == true) {

                                if (item!![0].equals("li")) {
                                    LIAccountsIDs?.add(idForAccount)

                                } else if (item!![0].equals("lic")) {
                                    LIAccountsIDs?.add(idForCompany)

                                }

                                //listview_accounts?.setItemChecked(position,false)


                            } else if (listview_accounts?.isItemChecked(position) == false) {

                                if (item!![0].equals("li")) {
                                    if (LIAccountsIDs?.contains(idForAccount) == true) {
                                        LIAccountsIDs?.remove(idForAccount)


                                    }
                                } else if (item!![0].equals("lic")) {
                                    if (LIAccountsIDs?.contains(idForCompany) == true) {
                                        LIAccountsIDs?.remove(idForCompany)


                                    }
                                }

                            }
                        }
                    }
                }
                "IN" ->
                {
                    if(list?.get(position) is String)
                    {

                    }
                    else
                    {

                        if(listview_accounts?.isItemChecked(position) == true)
                        {
                            //listview_accounts?.setItemChecked(position,false)

                            INAccountsIDs?.add((list?.get(position) as OtherAccountITem).id!!)


                        }
                        else if(listview_accounts?.isItemChecked(position) == false)
                        {
                            if(INAccountsIDs?.contains((list?.get(position) as OtherAccountITem).id!!) == true)
                            {
                                INAccountsIDs?.remove((list?.get(position) as OtherAccountITem).id!!)


                            }
                        }
                    }                }
                "PIN" ->
                {
                    if(list?.get(position) is String)
                    {

                    }
                    else
                    {

                        if(listview_accounts?.isItemChecked(position) == true)
                        {
                            //listview_accounts?.setItemChecked(position,false)

                            PINAccountsIDs?.add((list?.get(position) as OtherAccountITem).id!!)


                        }
                        else if(listview_accounts?.isItemChecked(position) == false)
                        {
                            if(PINAccountsIDs?.contains((list?.get(position) as OtherAccountITem).id!!) == true)
                            {
                                PINAccountsIDs?.remove((list?.get(position) as OtherAccountITem).id!!)


                            }
                        }
                    }
                }
                "GP" ->
                {
                    if(list?.get(position) is String)
                    {

                    }
                    else
                    {

                        if(listview_accounts?.isItemChecked(position) == true)
                        {
                            //listview_accounts?.setItemChecked(position,false)

                            GPAccountsIDs?.add((list?.get(position) as FBLIGPAccountItem).id!!)


                        }
                        else if(listview_accounts?.isItemChecked(position) == false)
                        {
                            if( GPAccountsIDs?.contains((list?.get(position) as FBLIGPAccountItem).id!!) == true)
                            {
                                GPAccountsIDs?.remove((list?.get(position) as FBLIGPAccountItem).id!!)


                            }
                        }
                    }                }

            }

        }


            }



    override fun onResume() {
        ApplicationController.instance?.setConnectivityListener(this)
        super.onResume()
    }


    override fun onBackPressed() {
        try {
            unregisterReceiver(ConnectionReceiver)
            list?.clear()
            FbAccountsIDs?.clear()
            LIAccountsIDs?.clear()
            GPAccountsIDs?.clear()
        } catch (e: Exception) {
        } finally {
        }

        super.onBackPressed()
        overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left)
        finish()
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        val id = item.itemId

        if (id == android.R.id.home) {
            onBackPressed()
            overridePendingTransition(R.anim.left_to_right, R.anim.right_to_left)
            finish()
            return true
        }

        return super.onOptionsItemSelected(item)
    }

    override fun onClick(v: View?) {
        var id = v?.id

        when (id) {

            R.id.button_post ->
            {
                if(isInternetConnected) {

                    if(post_type == "general")
                    {
                        postData()
                    }
                    else if(post_type == "autopost")
                    {
                        saveAutoPostConfig()
                    }
                    else if(post_type.equals("queue"))
                    {
                        saveQueueConfig()
                    }
                }
                else if(!isInternetConnected)
                {
                    toast("Please Check your Internet connection")

                }
            }

            R.id.ic_facebook -> {

                try {
                    loadFBData()

                  /*  var fragManager = fragmentManager

                    var facebook = Facebook()
                    var transaction = fragManager.beginTransaction()
                    transaction.replace(R.id.layout_fragment,facebook,"")
                    transaction.commit()
*/
                    ic_facebook?.imageTintList = resources.getColorStateList(R.color.white)
                    ic_twitter?.imageTintList = resources.getColorStateList(R.color.darkgrey)
                    ic_linkedin?.imageTintList = resources.getColorStateList(R.color.darkgrey)
                    ic_instagram?.imageTintList = resources.getColorStateList(R.color.darkgrey)
                    ic_pinterest?.imageTintList = resources.getColorStateList(R.color.darkgrey)
                    ic_googleplus?.imageTintList = resources.getColorStateList(R.color.darkgrey)

                } catch(e: Exception) {
                    Log.d("oncreate", e.printStackTrace().toString())
                }
            }
            R.id.ic_twitter -> {
                try {
                    loadTWData()
                    ic_twitter?.imageTintList = resources.getColorStateList(R.color.white)
                    ic_facebook?.imageTintList = resources.getColorStateList(R.color.darkgrey)
                    ic_linkedin?.imageTintList = resources.getColorStateList(R.color.darkgrey)
                    ic_instagram?.imageTintList = resources.getColorStateList(R.color.darkgrey)
                    ic_pinterest?.imageTintList = resources.getColorStateList(R.color.darkgrey)
                    ic_googleplus?.imageTintList = resources.getColorStateList(R.color.darkgrey)
                } catch(e: Exception) {
                    Log.d("oncreate", e.printStackTrace().toString())
                }
            }
            R.id.ic_linkedin -> {
                try {
                    loadLIData()
                    ic_linkedin?.imageTintList = resources.getColorStateList(R.color.white)
                    ic_facebook?.imageTintList = resources.getColorStateList(R.color.darkgrey)
                    ic_twitter?.imageTintList = resources.getColorStateList(R.color.darkgrey)
                    ic_instagram?.imageTintList = resources.getColorStateList(R.color.darkgrey)
                    ic_pinterest?.imageTintList = resources.getColorStateList(R.color.darkgrey)
                    ic_googleplus?.imageTintList = resources.getColorStateList(R.color.darkgrey)

                } catch(e: Exception) {
                    Log.d("oncreate", e.printStackTrace().toString())
                }
            }
            R.id.ic_instagram -> {
                try {
                    loadINData()
                    ic_instagram?.imageTintList = resources.getColorStateList(R.color.white)
                    ic_facebook?.imageTintList = resources.getColorStateList(R.color.darkgrey)
                    ic_twitter?.imageTintList = resources.getColorStateList(R.color.darkgrey)
                    ic_linkedin?.imageTintList = resources.getColorStateList(R.color.darkgrey)
                    ic_pinterest?.imageTintList = resources.getColorStateList(R.color.darkgrey)
                    ic_googleplus?.imageTintList = resources.getColorStateList(R.color.darkgrey)

                } catch(e: Exception) {
                    Log.d("oncreate", e.printStackTrace().toString())
                }

            }
            R.id.ic_pinterest -> {
                try {
                    loadPINData()
                    ic_pinterest?.imageTintList = resources.getColorStateList(R.color.white)
                    ic_facebook?.imageTintList = resources.getColorStateList(R.color.darkgrey)
                    ic_twitter?.imageTintList = resources.getColorStateList(R.color.darkgrey)
                    ic_linkedin?.imageTintList = resources.getColorStateList(R.color.darkgrey)
                    ic_instagram?.imageTintList = resources.getColorStateList(R.color.darkgrey)
                    ic_googleplus?.imageTintList = resources.getColorStateList(R.color.darkgrey)

                } catch(e: Exception) {
                    Log.d("oncreate", e.printStackTrace().toString())
                }
            }
            R.id.ic_googleplus -> {
                try {

                    loadGPData()
                    ic_googleplus?.imageTintList = resources.getColorStateList(R.color.white)
                    ic_facebook?.imageTintList = resources.getColorStateList(R.color.darkgrey)
                    ic_twitter?.imageTintList = resources.getColorStateList(R.color.darkgrey)
                    ic_linkedin?.imageTintList = resources.getColorStateList(R.color.darkgrey)
                    ic_instagram?.imageTintList = resources.getColorStateList(R.color.darkgrey)
                    ic_pinterest?.imageTintList = resources.getColorStateList(R.color.darkgrey)
                } catch(e: Exception) {
                    Log.d("oncreate", e.printStackTrace().toString())
                }


            }


        }
    }




    private inner class AccountsAdapter: ArrayAdapter<AccountsDataAdapter>(this@PlatformSelection, R.layout.accounts_listview_item, accounts_arraylist) {


        override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
            var convertView = convertView

            if (convertView == null) {
                convertView = layoutInflater.inflate(R.layout.platform_listview_item, parent, false)

            }

            val AccountsDataView = accounts_arraylist?.get(position)

            textview_accounts = convertView?.findViewById<CheckedTextView>(R.id.textview_platform)


            // imageview_display_picture.setImageURI(AccountsDataView?.getPictureUrl())
            if (AccountType == "TW" || AccountType == "LI" || AccountType == "IN" || AccountType == "PIN" || AccountType == "GP") {
                textview_accounts?.text = AccountsDataView?.getDisplayName()

            } else {
                textview_accounts?.text = AccountsDataView?.getUsername()

            }

            //textview_pages_count?.text = AccountsDataView?.getPagesCount()


            return convertView!!
        }
    }
    private inner class PagesCompaniesCirclesAdapter : ArrayAdapter<AccountsDataAdapter>(this@PlatformSelection, R.layout.accounts_listview_item, pages_companies_circles_arraylist) {


        override fun getView(position: Int, convertView: View?, parent: ViewGroup): View? {
            var convertView = convertView

            if (convertView == null) {
                convertView = layoutInflater.inflate(R.layout.platform_listview_item, parent, false)

            }

            val AccountsDataView = pages_companies_circles_arraylist?.get(position)


            textview_pages_companies_circles = convertView?.findViewById<CheckedTextView>(R.id.textview_platform)

            //imageview_display_picture.setImageURI(AccountsDataView?.getPictureUrl())


            if(AccountType == "LI")
            {
                textview_pages_companies_circles?.text = AccountsDataView?.getUsername()

            }
            else
            {
                textview_pages_companies_circles?.text = AccountsDataView?.getName()

            }










            //textview_pages_count?.text = AccountsDataView?.getPagesCount()


            return convertView

        }
    }
    private inner class GroupsCollectionsAdapter : ArrayAdapter<AccountsDataAdapter>(this@PlatformSelection, R.layout.accounts_listview_item, groups_collections_arraylist) {


        override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
            var convertView = convertView

            if (convertView == null) {
                convertView = layoutInflater.inflate(R.layout.platform_listview_item, parent, false)

            }

            val AccountsDataView = groups_collections_arraylist?.get(position)

            textview_groups_collections = convertView?.findViewById<CheckedTextView>(R.id.textview_platform)






            // imageview_display_picture.setImageURI(AccountsDataView?.getPictureUrl())

            if(AccountType == "FB")
            {

                textview_groups_collections?.text = AccountsDataView?.getGroupName()

            }
            else if(AccountType == "GP")
            {

                textview_groups_collections?.text = AccountsDataView?.getName()

            }




            return convertView!!
        }
    }

    @Throws(RuntimeException::class)
    fun postData(){

       // GPlusIDs?.clear()


        /*if (INAccountsIDs!!.size > 0 && (template_url == null || image_string == null)) {
            var alert = AlertManager("Image is required for Instagram", 3000, this@PlatformSelection)
        } else if (PINAccountsIDs!!.size > 0 && message == null && (template_url == null || image_string == null)) {
            var alert = AlertManager("Image & Message is required for Pinterest", 3000, this@PlatformSelection)

        } else if (LIAccountsIDs!!.size > 0) {
            if (template_url != null || image_string != null) {
                if (link == null) {
                    var alert = AlertManager("Link is required with image for Linkedin Account", 3000, this@PlatformSelection)
                }
            }
        }
        else if (LICompaniesIDs!!.size > 0) {
            if (template_url != null || image_string != null) {
                if (link == null) {
                    var alert = AlertManager("Link is required with image for Linkedin Company", 3000, this@PlatformSelection)

                }
            }

        }
        else if (TWAccountsIDs!!.size > 0 && message == null) {
            var alert = AlertManager(" Message is required for Twitter", 3000, this@PlatformSelection)

        }*/



        if(FbAccountsIDs!!.size == 0 && TWAccountsIDs!!.size == 0 && LIAccountsIDs!!.size == 0 && INAccountsIDs!!.size == 0 && PINAccountsIDs!!.size == 0 && GPAccountsIDs!!.size == 0 )
        {

            Log.d("cond","in if 1")
            var alert = AlertManager("Select atleast one account",3000,this@PlatformSelection)





        }
        else if (INAccountsIDs!!.size > 0 && template_url.isEmpty() && image_string.isEmpty())
        {

            Log.d("cond","in if 2")

            var alert = AlertManager("Image is required for Instagram", 3000, this@PlatformSelection)


        } else if (PINAccountsIDs!!.size > 0  && message.isEmpty() && (template_url.isEmpty() || image_string.isEmpty())) {

            Log.d("cond","in if 3")

            var alert = AlertManager("Image & Message is required for Pinterest", 3000, this@PlatformSelection)



        } else if (LIAccountsIDs!!.size > 0 && (!template_url.isEmpty() || !image_string.isEmpty()) && link.isEmpty()) {

            Log.d("cond","in if 4")


            var alert = AlertManager("Link is required with image for Linkedin Account", 3000, this@PlatformSelection)


        }
        else if (LICompaniesIDs!!.size > 0 && (!template_url.isEmpty() || !image_string.isEmpty()) && link.isEmpty()) {

            Log.d("cond","in if 5")


            var alert = AlertManager("Link is required with image for Linkedin Company", 3000, this@PlatformSelection)



        }
        else if (TWAccountsIDs!!.size > 0 && message.isEmpty()) {

            Log.d("cond","in if 6")

            var alert = AlertManager(" Message is required for Twitter", 3000, this@PlatformSelection)


        }
        else
        {

            Log.d("cond", "in else block")



            try {

                Log.d("post", "in else->try block")



                progressDialog = ProgressDialog.show(this@PlatformSelection, "Processing....", "Please wait", true)
                progressDialog?.setCancelable(false)


                var request = object : StringRequest(Method.POST, Constants.URL_GENERAL_POST, Response.Listener<String> {

                    response ->


                    //alert("response received "+response.toString()).show()
                    Log.d("post", "response received " + response.toString())


                    progressDialog?.dismiss()

                    var alert = AlertManager("Post Successful!", 5000, this@PlatformSelection)


                },
                        Response.ErrorListener {

                            error ->
                            //alert("error received "+ error.message).show()
                            Log.d("post", "error message:" + error.message.toString())
                            Log.d("post", "stacktrace:" + error.getStackTraceString())

                            progressDialog?.dismiss()
                            var alert = AlertManager("Post Unsuccessful!", 5000, this@PlatformSelection)


                        }
                ) {


                    override fun getBody(): ByteArray? {

                        var fbAccountarray = JSONArray()
                        var fbPagearray = JSONArray()
                        var fbGrouparray = JSONArray()
                        var liAccountarray = JSONArray()
                        var liCompanyarray = JSONArray()
                        var twAccountarray = JSONArray()
                        var inAccountarray = JSONArray()
                        var pinAccountarray = JSONArray()
                        var gplusIdarray = JSONArray()







                        for (i in 0..FbAccountsIDs!!.size - 1) {


                            var item = FbAccountsIDs?.get(i)?.split(":")
                            if(item!![0].equals("fb"))
                            {
                                fbAccountarray.put(item!![1])
                            }

                        }
                        for (i in 0..FbAccountsIDs!!.size - 1) {


                            var item = FbAccountsIDs?.get(i)?.split(":")
                            if(item!![0].equals("pg"))
                            {
                                fbPagearray.put(item!![1])
                            }

                        }
                        for (i in 0..FbAccountsIDs!!.size - 1) {


                            var item = FbAccountsIDs?.get(i)?.split(":")
                            if(item!![0].equals("gp"))
                            {
                                fbGrouparray.put(item!![1])
                            }

                        }
                        for (i in 0..TWAccountsIDs!!.size - 1) {
                            twAccountarray.put(TWAccountsIDs?.get(i))
                        }
                        for (i in 0..LIAccountsIDs!!.size - 1) {


                            var item = LIAccountsIDs?.get(i)?.split(":")
                            if(item!![0].equals("li"))
                            {
                                liAccountarray.put(item!![1]+":"+item!![2])
                            }

                        }
                        for (i in 0..LIAccountsIDs!!.size - 1) {


                            var item = LIAccountsIDs?.get(i)?.split(":")
                            if(item!![0].equals("lic"))
                            {
                                liCompanyarray.put(item!![1]+":"+item!![2]+":"+item!![3])
                            }

                        }
                        for (i in 0..INAccountsIDs!!.size - 1) {
                            inAccountarray.put(INAccountsIDs?.get(i))
                        }
                        for (i in 0..PINAccountsIDs!!.size - 1) {
                            pinAccountarray.put(PINAccountsIDs?.get(i))
                        }
                        for (i in 0..GPAccountsIDs!!.size - 1) {
                            gplusIdarray.put(GPAccountsIDs?.get(i))
                        }




                        var jsonObject = JSONObject()


                        var body: String? = null
                        Log.d("post", "In get Body")

                        try {

                            current_time = df_for_current_time.format(calendar.time) + ":00:00"


                            if (schedule == 1) {

                            } else if (schedule == 0) {
                                //  jsonObject.put("posted_on",current_time)


                            }
                            jsonObject.put("link", link)
                            jsonObject.put("current_time", current_time)
                            if (base64 == true) {
                                jsonObject.put("image", image_string)

                            } else if (base64 == false) {
                                jsonObject.put("image", template_url)

                            }
                            jsonObject.put("schedule", schedule)
                            jsonObject.put("message", message)
                            jsonObject.put("base64", base64)
                            jsonObject.put("user_id", fbAccountarray)


                            jsonObject.put("page_id", fbPagearray)

                            jsonObject.put("group_id", fbGrouparray)

                            jsonObject.put("linkedin", liAccountarray)


                            jsonObject.put("company", liCompanyarray)


                            jsonObject.put("twitter", twAccountarray)



                            jsonObject.put("instagram", inAccountarray)


                            jsonObject.put("pinterest", pinAccountarray)

                            jsonObject.put("gplus", gplusIdarray)


                            body = jsonObject.toString()

                            Log.d("post", "json object string: " + jsonObject.toString())
                            Log.d("post", "json object bytearray: " + jsonObject.toString().toByteArray())

                        } catch (e: JSONException) {
                            Log.d("post", e.message)
                            var alert = AlertManager("Network problem!",5000,this@PlatformSelection)

                        }



                        return jsonObject.toString().toByteArray()
                    }


                    @Throws(AuthFailureError::class)
                    override fun getHeaders(): Map<String, String> {
                        return Constants.getAuthHeader()
                    }
                }

                request.setRetryPolicy(DefaultRetryPolicy(
                        25000,
                        0,
                        DefaultRetryPolicy.DEFAULT_BACKOFF_MULT))


                val addToRequestQueue = ApplicationController.instance?.addToRequestQueue(request)
            } catch (e: Exception) {
                progressDialog?.dismiss()

            } finally {
            }

        }

    }


    //function for autopost queue congifuration
    fun saveAutoPostConfig() {



        var days_array = JSONArray()
        var temp_array = ArrayList<String>()

        var intent = intent
        var extras = intent.extras



        var autopost_activated = extras.getInt("AUTOPOST_ACTIVATED")
        var autopost_time = extras.getString("TIME")

        if(FbAccountsIDs!!.size == 0 && TWAccountsIDs!!.size == 0 && LIAccountsIDs!!.size == 0 && INAccountsIDs!!.size == 0 && PINAccountsIDs!!.size == 0 && GPAccountsIDs!!.size == 0 )
        {

            Log.d("cond","in if 1")
            var alert = AlertManager("Select atleast one account",3000,this@PlatformSelection)

        }

        else
        {


            progressDialog = ProgressDialog.show(this@PlatformSelection, "Saving Configuration....", "Please wait", true)
            progressDialog?.setCancelable(false)

            var request = object : JsonObjectRequest(Method.POST, Constants.URL_AUTO_POST,null, Response.Listener<JSONObject> {

                response ->

                Log.d("response",""+response.toString())

                var status = response.getString(JsonKeys.variables.KEY_STATUS)
                Log.d("response",""+status)

                if(status.equals("success"))
                {
                    progressDialog?.dismiss()

                    var alert = AlertManager("Configuration saved successfully",5000,this@PlatformSelection)
                }
                else
                {
                    progressDialog?.dismiss()

                    var alert = AlertManager("Network problem!",5000,this@PlatformSelection)
                }


            },
                    Response.ErrorListener { error ->

                        Log.d("response",error.message.toString())


                        progressDialog?.dismiss()
                        var alert = AlertManager("Network problem!",5000,this@PlatformSelection)


                    })

            {

                override fun getBody(): ByteArray {

                    var accountarray = JSONArray()
                    var cat_array = JSONArray()




                    for (i in 0..extras.getStringArrayList("DAYS").size - 1) {
                        if(days_map?.containsKey(extras.getStringArrayList("DAYS").get(i)) == true)
                        {
                            var a =extras.getStringArrayList("DAYS").get(i)
                            days_array.put(days_map?.get(a))

                        }

                    }

                    for(i in 0..extras.getStringArrayList("CATEGORIES").size -1)
                    {
                        cat_array.put(extras.getStringArrayList("CATEGORIES").get(i))
                    }




                    for (i in 0..FbAccountsIDs!!.size - 1) {
                        accountarray.put(FbAccountsIDs?.get(i))
                    }

                    for (i in 0..TWAccountsIDs!!.size - 1) {
                        accountarray.put("tw:" + TWAccountsIDs?.get(i))
                    }
                    for (i in 0..LIAccountsIDs!!.size - 1) {


                       accountarray.put(LIAccountsIDs?.get(i))

                    }
                    for (i in 0..INAccountsIDs!!.size - 1) {
                        accountarray.put("ins:" + INAccountsIDs?.get(i))
                    }
                    for (i in 0..PINAccountsIDs!!.size - 1) {
                        accountarray.put("pin:" + PINAccountsIDs?.get(i))
                    }

                    for (i in 0..GPAccountsIDs!!.size - 1) {
                        accountarray.put(GPAccountsIDs?.get(i))
                    }


                    var jsonObject = JSONObject()


                    var body: String? = null
                    Log.d("post", "In get Body")

                    try {

                        jsonObject.put("status",autopost_activated)
                        jsonObject.put("time",autopost_time)
                        jsonObject.put("PostOnDays",days_array)
                        jsonObject.put("cat_name",cat_array)
                        jsonObject.put("accounts",accountarray)


                        body = jsonObject.toString()

                        Log.d("autopost", "json object string: " + jsonObject.toString())
                        //Log.d("autpost", "json object bytearray: " + jsonObject.toString().toByteArray())

                    }
                    catch (e: JSONException)
                    {

                        Log.d("autpostpost", e.message)
                        var alert = AlertManager("Network problem!",5000,this@PlatformSelection)


                    }



                    return jsonObject.toString().toByteArray()
                }

                @Throws(AuthFailureError::class)
                override fun getHeaders(): Map<String, String> {
                    return Constants.getAuthHeader()
                }


            }

            request.setRetryPolicy(DefaultRetryPolicy(
                    25000,
                    0,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT))


            val addToRequestQueue = ApplicationController.instance?.addToRequestQueue(request)

        }
    }


    //function for saving queue congifuration
    fun saveQueueConfig() {

       // GPlusIDs?.clear()

        var days_array = JSONArray()
        var temp_array = ArrayList<String>()

        var intent = intent
        var extras = intent.extras



        var loginManager =  LoginManager(this@PlatformSelection)
        var user_id = loginManager.getUserID()


        var queue_name = extras.getString("QUEUE_NAME")
        var queue_id = extras.getInt("QUEUE_ID")





        var request = extras.getString("REQUEST")


        var days_arraylist = extras.getIntegerArrayList("DAYS")
        var localtime_arraylist = extras.getStringArrayList("LOCALTIME")
        var time_arraylist = extras.getStringArrayList("TIME")


        if(FbAccountsIDs!!.size == 0 && TWAccountsIDs!!.size == 0  && LIAccountsIDs!!.size == 0 && INAccountsIDs!!.size == 0 && PINAccountsIDs!!.size == 0 && GPAccountsIDs!!.size == 0)
        {

            Log.d("cond","in if 1")
            var alert = AlertManager("Select atleast one account",3000,this@PlatformSelection)

        }

        else
        {


            progressDialog = ProgressDialog.show(this@PlatformSelection, "Saving Configuration....", "Please wait", true)
            progressDialog?.setCancelable(false)

            var request = object : JsonObjectRequest(Method.POST, Constants.URL_QUEUE_SAVE,null, Response.Listener<JSONObject> {

                response ->

                try {
                    Log.d("queue",""+response.toString())

                    var status = response.getString(JsonKeys.variables.KEY_STATUS)
                    Log.d("response",""+status)

                    if(status.equals("true"))
                    {
                        progressDialog?.dismiss()

                        if(title.equals("edit_queue"))
                        {
                            var alert = AlertManager("Queue Updated Successfully",5000,this@PlatformSelection)

                        }
                        else
                        {
                            var alert = AlertManager("Queue Created Successfully",5000,this@PlatformSelection)

                        }

                    }
                    else
                    {
                        progressDialog?.dismiss()

                        var alert = AlertManager("Problem creating Queue!",5000,this@PlatformSelection)
                    }
                } catch (e: Exception) {
                    Log.d("queue",""+e.message.toString())
                } finally {
                }

            },
                    Response.ErrorListener { error ->

                        Log.d("queue",error.message.toString())


                        progressDialog?.dismiss()
                        var alert = AlertManager("Network problem!",5000,this@PlatformSelection)


                    })

            {

                override fun getBody(): ByteArray {

                    var accountarray = JSONArray()
                    var days_array = JSONArray()
                    var time_array = JSONArray()
                    var localtime_array = JSONArray()





                    for(i in 0..days_arraylist.size -1)
                    {
                        days_array.put(""+(days_arraylist).get(i))
                    }
                    for(i in 0..time_arraylist.size -1)
                    {
                        time_array.put((time_arraylist).get(i))
                    }
                    for(i in 0..localtime_arraylist.size -1)
                    {
                        localtime_array.put((localtime_arraylist).get(i))
                    }


                    for (i in 0..LIAccountsIDs!!.size - 1) {


                        accountarray.put(LIAccountsIDs?.get(i))

                    }



                    for (i in 0..FbAccountsIDs!!.size - 1) {
                        accountarray.put(FbAccountsIDs?.get(i).toString())
                    }

                    for (i in 0..TWAccountsIDs!!.size - 1) {
                        accountarray.put("tw:" + TWAccountsIDs?.get(i))
                    }


                    for (i in 0..GPAccountsIDs!!.size - 1) {
                        accountarray.put(GPAccountsIDs?.get(i))
                    }




                    var jsonObject = JSONObject()


                    var body: String? = null
                    Log.d("queue", "In get Body")

                    try {

                        if(title.equals("edit_queue"))
                        {
                            jsonObject.put("queueid",queue_id)

                            jsonObject.put("request","update")

                        }
                        else
                        {
                            jsonObject.put("request",request)

                        }

                        jsonObject.put("user_id",user_id)
                        jsonObject.put("name",queue_name)


                        jsonObject.put("days",days_array)
                        jsonObject.put("localtime",localtime_array)
                        jsonObject.put("time",time_array)
                        jsonObject.put("accounts",accountarray)


                        body = jsonObject.toString()

                        Log.d("queue", "json object string: " + jsonObject.toString())
                        //Log.d("autpost", "json object bytearray: " + jsonObject.toString().toByteArray())

                    }
                    catch (e: JSONException)
                    {

                        Log.d("queue", e.message)
                        var alert = AlertManager("Network problem!",5000,this@PlatformSelection)


                    }



                    return jsonObject.toString().toByteArray()
                }

                @Throws(AuthFailureError::class)
                override fun getHeaders(): Map<String, String> {
                    return Constants.getAuthHeader()
                }


            }

            request.setRetryPolicy(DefaultRetryPolicy(
                    25000,
                    0,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT))


            val addToRequestQueue = ApplicationController.instance?.addToRequestQueue(request)

        }
    }
    @Throws(IllegalArgumentException::class)
    fun convertToBitmap(base64Str: String): Bitmap {
        val decodedBytes = Base64.decode(
                base64Str.substring(base64Str.indexOf(",") + 1),
                Base64.DEFAULT
        )

        return BitmapFactory.decodeByteArray(decodedBytes, 0, decodedBytes.size)
    }


    @Throws(IllegalArgumentException::class)
    fun convertToBitmap(byteArray: ByteArray): Bitmap {

        val bitmap = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.size)


        return bitmap
    }


    fun getBase64(bitmap: Bitmap): String {
        val outputStream = ByteArrayOutputStream()
        bitmap.compress(Bitmap.CompressFormat.PNG, 100, outputStream)

        return Base64.encodeToString(outputStream.toByteArray(), Base64.DEFAULT)
    }

    override fun onNetworkConnectionChanged(isConnected: Boolean) {
        isInternetConnected = ConnectivityReceiver.isConnected
        if (isConnected) {
            //implement here for connected services

        } else if (!isConnected) {
            //implement here for no network services

        }
    }








    fun loadFBData()
    {

        AccountType = "FB"
        list?.clear()
        list?.add("Accounts" as String)
        for(i in 0..UserDataManager?.FBListForPost!!.size - 1)
        {
            var item = UserDataManager?.FBListForPost?.get(i)?.getIdForPost()?.split(":")

            if(item!![0].equals("fb"))
            {
                list?.add(FBLIGPAccountItem(UserDataManager?.FBListForPost?.get(i)?.getName(),UserDataManager?.FBListForPost?.get(i)?.getIdForPost(),null,null))
            }
        }
        list?.add("Pages" as String)
        for(i in 0..UserDataManager?.FBListForPost!!.size - 1)
        {
            var item = UserDataManager?.FBListForPost?.get(i)?.getIdForPost()?.split(":")

            if(item!![0].equals("pg"))
            {
                list?.add(FBLIGPAccountItem(UserDataManager?.FBListForPost?.get(i)?.getName(),UserDataManager?.FBListForPost?.get(i)?.getIdForPost(),null,null))
            }
        }
        list?.add("Groups" as String)
        for(i in 0..UserDataManager?.FBListForPost!!.size - 1)
        {
            var item = UserDataManager?.FBListForPost?.get(i)?.getIdForPost()?.split(":")

            if(item!![0].equals("gp"))
            {
                list?.add(FBLIGPAccountItem(UserDataManager?.FBListForPost?.get(i)?.getName(),UserDataManager?.FBListForPost?.get(i)?.getIdForPost(),null,null))
            }
        }


        var listviewAdapter = ListviewAdapter(this,list!!)
        listview_accounts?.adapter =  listviewAdapter
        listviewAdapter?.notifyDataSetChanged()


        if(FbAccountsIDs?.size != 0)
        {
            for(i in 0..list!!.size - 1)
            {
                if(list?.get(i) is String)
                {

                }
                else
                {
                    if(FbAccountsIDs?.contains((list?.get(i) as FBLIGPAccountItem)?.id)!!)
                    {
                        listview_accounts?.setItemChecked(i,true)
                    }
                }
            }
        }



    }
       fun loadGPData()
    {
        AccountType = "GP"


        list?.clear()
        list?.add("Accounts" as String)
        for(i in 0..UserDataManager?.GPListForPost!!.size - 1)
        {
            var item = UserDataManager?.GPListForPost?.get(i)?.getIdForPost()?.split(":")

            if(item!![0].equals("gpu"))
            {
                list?.add(FBLIGPAccountItem(UserDataManager?.GPListForPost?.get(i)?.getName(),UserDataManager?.GPListForPost?.get(i)?.getIdForPost(),null,null))
            }
        }
        list?.add("Circles" as String)
        for(i in 0..UserDataManager?.GPListForPost!!.size - 1)
        {
            var item = UserDataManager?.GPListForPost?.get(i)?.getIdForPost()?.split(":")

            if(item!![0].equals("gpc"))
            {
                list?.add(FBLIGPAccountItem(UserDataManager?.GPListForPost?.get(i)?.getName(),UserDataManager?.GPListForPost?.get(i)?.getIdForPost(),null,null))
            }
        }
        list?.add("Collections" as String)
        for(i in 0..UserDataManager?.GPListForPost!!.size - 1)
        {
            var item = UserDataManager?.GPListForPost?.get(i)?.getIdForPost()?.split(":")

            if(item!![0].equals("gpcl"))
            {
                list?.add(FBLIGPAccountItem(UserDataManager?.GPListForPost?.get(i)?.getName(),UserDataManager?.GPListForPost?.get(i)?.getIdForPost(),null,null))
            }
        }

        var listviewAdapter = ListviewAdapter(this,list!!)
        listview_accounts?.adapter =  listviewAdapter
        listviewAdapter?.notifyDataSetChanged()

        if(GPAccountsIDs?.size != 0)
        {
            for(i in 0..list!!.size - 1)
            {
                if(list?.get(i) is String)
                {

                }
                else
                {
                    if(GPAccountsIDs?.contains((list?.get(i) as FBLIGPAccountItem)?.id)!!)
                    {
                        listview_accounts?.setItemChecked(i,true)
                    }
                }
            }
        }

    }

    fun loadLIData() {
        AccountType = "LI"


        list?.clear()
        Log.d("item", "" + UserDataManager?.LIListForPost?.get(0)?.getIdForPost())
        list?.add("Accounts" as String)
        for (i in 0..UserDataManager?.LIListForPost!!.size - 1) {
            var item = UserDataManager?.LIListForPost?.get(i)?.getIdForPost()?.split(":")



            if (item!![0].equals("li")) {
                list?.add(FBLIGPAccountItem(UserDataManager?.LIListForPost?.get(i)?.getDisplayName(), UserDataManager?.LIListForPost?.get(i)?.getIdForPost(), UserDataManager?.LIListForPost?.get(i)?.getCompanyId(), UserDataManager?.LIListForPost?.get(i)?.getToken()))
            }
        }
        list?.add("Companies" as String)
        for (i in 0..UserDataManager?.LIListForPost!!.size - 1) {
            var item = UserDataManager?.LIListForPost?.get(i)?.getIdForPost()?.split(":")

            if (item!![0].equals("lic")) {
                list?.add(FBLIGPAccountItem(UserDataManager?.LIListForPost?.get(i)?.getDisplayName(), UserDataManager?.LIListForPost?.get(i)?.getIdForPost(), UserDataManager?.LIListForPost?.get(i)?.getCompanyId(), UserDataManager?.LIListForPost?.get(i)?.getToken()))
            }
        }


        var listviewAdapter = ListviewAdapter(this, list!!)
        listview_accounts?.adapter = listviewAdapter
        listviewAdapter?.notifyDataSetChanged()


        if (LIAccountsIDs?.size != 0) {

            for (i in 0..list!!.size - 1) {
                if (list?.get(i) is String) {

                } else {
                    if (post_type.equals("general")) {
                        var idForAccount: String = ""
                        var idForCompany: String = ""

                        var item = (list?.get(i) as FBLIGPAccountItem).id?.split(":")

                        if (item!![0].equals("li")) {
                            idForAccount = (list?.get(i) as FBLIGPAccountItem).id!! + ":" + (list?.get(i) as FBLIGPAccountItem).token!!

                        } else if (item!![0].equals("lic")) {
                            idForCompany = (list?.get(i) as FBLIGPAccountItem).id!! + ":" + (list?.get(i) as FBLIGPAccountItem).company_id!! + ":" + (list?.get(i) as FBLIGPAccountItem).token!!

                        }
                        if (LIAccountsIDs?.contains((list?.get(i) as FBLIGPAccountItem)?.id)!!) {
                            listview_accounts?.setItemChecked(i, true)
                        }
                        if (LIAccountsIDs?.contains(idForAccount)!!) {
                            listview_accounts?.setItemChecked(i, true)

                        } else if (LIAccountsIDs?.contains(idForCompany)!!) {
                            listview_accounts?.setItemChecked(i, true)

                        }
                    } else if (post_type.equals("autopost")) {
                        var idForAccount: String = ""
                        var idForCompany: String = ""

                        var item = (list?.get(i) as FBLIGPAccountItem).id?.split(":")

                        if (item!![0].equals("li")) {
                            idForAccount = (list?.get(i) as FBLIGPAccountItem).id!!

                        } else if (item!![0].equals("lic")) {
                            idForCompany = "company:" + (list?.get(i) as FBLIGPAccountItem).company_id!!

                        }
                        if (LIAccountsIDs?.contains((list?.get(i) as FBLIGPAccountItem)?.id)!!) {
                            listview_accounts?.setItemChecked(i, true)
                        }
                        if (LIAccountsIDs?.contains(idForAccount)!!) {
                            listview_accounts?.setItemChecked(i, true)

                        } else if (LIAccountsIDs?.contains(idForCompany)!!) {
                            listview_accounts?.setItemChecked(i, true)

                        }
                    } else if (post_type.equals("queue")) {
                        var idForAccount: String = ""
                        var idForCompany: String = ""

                        var item = (list?.get(i) as FBLIGPAccountItem).id?.split(":")

                        if (item!![0].equals("li")) {
                            idForAccount = (list?.get(i) as FBLIGPAccountItem).id!!

                        } else if (item!![0].equals("lic")) {
                            idForCompany = "lic:"+(list?.get(i) as FBLIGPAccountItem).company_id!!

                        }
                        if (LIAccountsIDs?.contains((list?.get(i) as FBLIGPAccountItem)?.id)!!) {
                            listview_accounts?.setItemChecked(i, true)
                        }
                        if (LIAccountsIDs?.contains(idForAccount)!!) {
                            listview_accounts?.setItemChecked(i, true)

                        } else if (LIAccountsIDs?.contains(idForCompany)!!) {
                            listview_accounts?.setItemChecked(i, true)

                        }
                    }
                }
            }
        }
    }



    fun loadTWData()
    {
        AccountType = "TW"


        list?.clear()
        list?.add("Accounts" as String)
        for(i in 0..UserDataManager?.TWAccountsList!!.size - 1)
        {
        list?.add(OtherAccountITem(UserDataManager?.TWAccountsList?.get(i)?.getDisplayName(),UserDataManager?.TWAccountsList?.get(i)?.getId()))

        }



        var listviewAdapter = ListviewAdapter(this,list!!)
        listview_accounts?.adapter =  listviewAdapter
        listviewAdapter?.notifyDataSetChanged()


        if(TWAccountsIDs?.size != 0)
        {
            for(i in 0..list!!.size - 1)
            {
                if(list?.get(i) is String)
                {

                }
                else
                {
                    if(TWAccountsIDs?.contains((list?.get(i) as OtherAccountITem)?.id)!!)
                    {
                        listview_accounts?.setItemChecked(i,true)
                    }
                }
            }
        }

    }
    fun loadINData()
    {
        AccountType = "IN"

        list?.clear()
        list?.add("Accounts" as String)
        for(i in 0..UserDataManager?.INAccountsList!!.size - 1)
        {
            list?.add(OtherAccountITem(UserDataManager?.INAccountsList?.get(i)?.getDisplayName(),UserDataManager?.INAccountsList?.get(i)?.getId()))

        }


        var listviewAdapter = ListviewAdapter(this,list!!)
        listview_accounts?.adapter =  listviewAdapter
        listviewAdapter?.notifyDataSetChanged()


        if(INAccountsIDs?.size != 0)
        {
            for(i in 0..list!!.size - 1)
            {
                if(list?.get(i) is String)
                {

                }
                else
                {
                    if(INAccountsIDs?.contains((list?.get(i) as OtherAccountITem)?.id)!!)
                    {
                        listview_accounts?.setItemChecked(i,true)
                    }
                }
            }
        }

    }
    fun loadPINData()
    {
        AccountType = "PIN"


list?.clear()
        list?.add("Accounts" as String)
        for(i in 0..UserDataManager?.PINAccountsList!!.size - 1)
        {
            list?.add(OtherAccountITem(UserDataManager?.PINAccountsList?.get(i)?.getDisplayName(),UserDataManager?.PINAccountsList?.get(i)?.getId()))

        }


        var listviewAdapter = ListviewAdapter(this,list!!)
        listview_accounts?.adapter =  listviewAdapter
        listviewAdapter?.notifyDataSetChanged()


        if(PINAccountsIDs?.size != 0)
        {
            for(i in 0..list!!.size - 1)
            {
                if(list?.get(i) is String)
                {

                }
                else
                {
                    if(PINAccountsIDs?.contains((list?.get(i) as OtherAccountITem)?.id)!!)
                    {
                        listview_accounts?.setItemChecked(i,true)
                    }
                }
            }
        }

    }

    private inner class ListviewAdapter(context: Context,list: ArrayList<Any>): BaseAdapter()
    {

        // View Type for Separators
        var list = ArrayList<Any>()
        var HEADER = 0
        // View Type for Regular rows
         var ITEM = 1
        var context: Context
        lateinit var inflater: LayoutInflater

        init {
            this.list = list
            inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            this.context = context
        }


        override fun getItemViewType(position: Int): Int {
           if(list.get(position) is FBLIGPAccountItem || list.get(position) is OtherAccountITem)
           {
               return ITEM

           }else
           {
               return HEADER
           }
        }

        override fun getItem(i: Int): Any {
            return list.get(i)
        }

        override fun getViewTypeCount(): Int {
            return 2
        }

        override fun getItemId(i: Int): Long {
                return i.toLong()
        }

        override fun getCount(): Int {
            return list.size

        }

        override fun getView(i: Int, view: View?, viewGroup: ViewGroup?): View {

            var view = view


            if(view == null)
            {
                when(getItemViewType(i))
                {
                    ITEM ->
                    {
                        view = inflater.inflate(R.layout.platform_listview_item,null)
                    }
                    HEADER ->
                    {
                        view = inflater.inflate(R.layout.section_header,null)

                    }
                }
            }
            when(getItemViewType(i))
            {
                ITEM ->
                {
                    var textview_name = view?.findViewById<CheckedTextView>(R.id.textview_platform)

                    if(list.get(i) is FBLIGPAccountItem)
                    {
                        textview_name?.setText((list.get(i) as FBLIGPAccountItem).name)
                    }
                    else if(list.get(i) is OtherAccountITem)
                    {
                        textview_name?.setText((list.get(i) as OtherAccountITem).name)

                    }

                }
                HEADER ->
                {
                var textview_section_header = view?.findViewById<TextView>(R.id.textview_section_header)

                    textview_section_header?.setText(list.get(i) as String)

                }
            }


            return view!!
        }


    }
}


